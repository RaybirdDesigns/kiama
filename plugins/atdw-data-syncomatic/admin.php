<?php
if (!is_admin()) {
    die();
}

$current_product_numbers = get_option('atdwds_current_product_numbers');

$custom_post_types = get_post_types(array(
   'public'   => true,
   'exclude_from_search'   => false,
   '_builtin' => false
));

?>

<div class="wrap">
	<h2><?php echo get_bloginfo( 'name' ); ?> ATDW Manager</h2>
	<?php settings_errors(); ?>
	<form class="atdw-syncomatic" method="post" action="options.php">
		<?php settings_fields('atdwds_group'); ?>
		<?php do_settings_sections('atdwds_group'); ?>
		<button type="button" class="settings-btn" data-show-settings>Show Settings</button>

		<div data-settings-container class="settings-container" style="display: none;">
			<table class="form-table">			
		        <tr valign="top">
	                <td colspan="3">
	                	<h3>ATDW API Key</h3>
		            	<input required type="text" id="id_atdwds_key" name="atdwds_key" value="<?php echo esc_attr(get_option('atdwds_key')); ?>" size="38" />
		            </td>
		            <td>
		            </td>
		        </tr>
		        <tr valign="top">
	                <td colspan="3">
	                	<h3>Email Notification</h3>
	                	<p>If a listing has been updated notify this email address.</p>
		            </td>
		        </tr>
		        <tr>
		        	<td colspan="3">
	                	<input required type="email" id="id_atdwds_key" name="atdwds_email" value="<?php echo esc_attr(get_option('atdwds_email')); ?>" size="38" />
		        	</td>
		        </tr>
		        <?php
		        $settings = array(
		        	'image_is_sync' => array(
		        		'desc' => 'Images (Recommended).',
		        		'name' => 'atdwds_keep_image_sync',
		        		'value' => 'image_sync',
		        		'checkbox' => get_option('atdwds_keep_image_sync') === 'image_sync' ? 'checked' : null,
		        	),
		        	'title_is_sync' => array(
		        		'desc' => 'Listing Title.',
		        		'name' => 'atdwds_keep_title_sync',
		        		'value' => 'title_sync',
		        		'checkbox' => get_option('atdwds_keep_title_sync') === 'title_sync' ? 'checked' : null,
		        	),
		        	'date_is_sync' => array(
		        		'desc' => 'Event Dates.',
		        		'name' => 'atdwds_keep_start_end_date_sync',
		        		'value' => 'date_sync',
		        		'checkbox' => get_option('atdwds_keep_start_end_date_sync') === 'date_sync' ? 'checked' : null,
		        	),
		        	'accomm_is_sync' => array(
		        		'desc' => 'Rates and Check In/Out Times.',
		        		'name' => 'atdwds_keep_accomm_sync',
		        		'value' => 'accomm_sync',
		        		'checkbox' => get_option('atdwds_keep_accomm_sync') === 'accomm_sync' ? 'checked' : null,
		        	),
		        	'features_is_sync' => array(
		        		'desc' => 'Features (ie. Barbeque,Cafe,Carpark).',
		        		'name' => 'atdwds_keep_features_sync',
		        		'value' => 'features_sync',
		        		'checkbox' => get_option('atdwds_keep_features_sync') === 'features_sync' ? 'checked' : null,
		        	),
		        	'content_is_sync' => array(
		        		'desc' => 'Description.',
		        		'name' => 'atdwds_keep_content_sync',
		        		'value' => 'content_sync',
		        		'checkbox' => get_option('atdwds_keep_content_sync') === 'content_sync' ? 'checked' : null,
		        	),
		        	'comms_is_sync' => array(
		        		'desc' => 'Communication (ie. Email, Phone, Booking URL, Enquiries URL).',
		        		'name' => 'atdwds_keep_content_sync',
		        		'value' => 'comms_sync',
		        		'checkbox' => get_option('atdwds_keep_comms_sync') === 'comms_sync' ? 'checked' : null,
		        	),
		        	'location_is_sync' => array(
		        		'desc' => 'Address.',
		        		'name' => 'atdwds_keep_location_sync',
		        		'value' => 'location_sync',
		        		'checkbox' => get_option('atdwds_keep_location_sync') === 'location_sync' ? 'checked' : null,
		        	),
		        	'coords_is_sync' => array(
		        		'desc' => 'Map Coordinates.',
		        		'name' => 'atdwds_keep_coords_sync',
		        		'value' => 'coords_sync',
		        		'checkbox' => get_option('atdwds_keep_coords_sync') === 'coords_sync' ? 'checked' : null,
		        	),
		        	'platforms_is_sync' => array(
		        		'desc' => 'Platforms (ie. Bookeasy ID, Trip Advisor ID, Trip Advisor URL, Facebook URL, Twitter URL, Instagram URL).',
		        		'name' => 'atdwds_keep_platforms_sync',
		        		'value' => 'platforms_sync',
		        		'checkbox' => get_option('atdwds_keep_platforms_sync') === 'platforms_sync' ? 'checked' : null,
		        	),
		        	'subcats_is_sync' => array(
		        		'desc' => 'Sub Categories.',
		        		'name' => 'atdwds_keep_platforms_sync',
		        		'value' => 'subcats_sync',
		        		'checkbox' => get_option('atdwds_keep_subcats_sync') === 'subcats_sync' ? 'checked' : null,
		        	),
		        	'access_is_sync' => array(
		        		'desc' => 'Accessibility Statement.',
		        		'name' => 'atdwds_keep_access_sync',
		        		'value' => 'access_sync',
		        		'checkbox' => get_option('atdwds_keep_access_sync') === 'access_sync' ? 'checked' : null,
		        	),
		        	'system_is_sync' => array(
		        		'desc' => 'System Data (ie. Owning Organisation ID, Owning Organisation Name, Owning Organisation Number, Product ID, Product Number, Product Pixel URL).',
		        		'name' => 'atdwds_keep_system_sync',
		        		'value' => 'system_sync',
		        		'checkbox' => get_option('atdwds_keep_system_sync') === 'system_sync' ? 'checked' : null,
		        	),
		        );
				$auto_sync = get_option('atdwds_auto_sync') === 'auto_sync' ? 'checked' : null;
		        ?>
		        <tr>
		        	<td colspan="3">
		        		<h3>Sync Settings</h3>
		        	</td>
		        </tr>
		        <tr>
		        	<td colspan="3">
		        		<input id="auto-sync" type="checkbox" <?php echo $auto_sync; ?> name="atdwds_auto_sync" value="auto_sync"><label for="auto-sync">Enable automatic daily syncing.</label>
		        	</td>
		        </tr>
		        <tr>
		        	<td colspan="3">
		        		<p>Check data that will be synced when either using the automated daily syncing or manual syncing.</p>
		        	</td>
		        </tr>
		        <tr>
		        	<td colspan="3">
		        		<?php foreach($settings as $name => $setting): ?>
							<div style="padding: 10px 0 0;">	
								<input id="<?php echo $name; ?>" type="checkbox" <?php echo $setting['checkbox']; ?> name="<?php echo $setting['name']; ?>" value="<?php echo $setting['value']; ?>"><label for="<?php echo $name; ?>"><?php echo $setting['desc']; ?></label>
							</div>
		        		<?php endforeach; ?>
		        	</td>
		        </tr>
		        <tr>
		        	<td>
		        		<?php submit_button() ?>
		        	</td>
		        </tr>
			</table>
		</div>

		<table class="form-table" style="margin-bottom: 30px;">
	        <tr valign="top">
                <th scope="row">
                	<label for="id_atdwds_add_single_member"><h2>Add New Member</h2><p>Use 24 digit Product ID</p></label>
	            </th>
	            <td>
	            	<?php if (empty(get_option('atdwds_add_single_member')) || get_option('atdwds_product_preview_error') === 'empty'): ?>
	            	<input type="text" id="id_atdwds_add_single_member" placeholder="56b245eb3ed14ca745323c5c" name="atdwds_add_single_member" value="" size="30" />
	            	<?php endif; ?>
	            </td>
	        </tr>
	        <?php
	        $preview_data = json_decode(get_option('atdwds_product_preview'), true)[0];

	        if (!empty(get_option('atdwds_add_single_member')) && empty(get_option('atdwds_product_preview_error'))): ?>
	        <tr class="preview-member">
	        	<td valign="top" class="preview-image"><img src="<?php echo $preview_data['productImage']; ?>" alt="<?php echo $preview_data['productName']; ?>"></td>
	        	<td class="preview-text">
	        		<h3><?php echo $preview_data['productName']; ?></h3>
	        		<p><em><?php echo $preview_data['productCategoryId']; ?>, <?php echo $preview_data['productNumber']; ?></em></p>
	        		<p><?php echo substr($preview_data['productDescription'],0,390); ?>...</p>
	        	</td>
	        	<td class="preview-btn">
	        		<a class="button button-primary" onclick="return confirm('<?php echo $preview_data['productName']; ?> will be added to the site. It will be set to draft.')" data-get-data href="<?php echo esc_url(admin_url('admin-post.php?action=add_single_member')); ?>">Add Member<div>+</div></a>
	        		<img data-loader-image class="atdwds-loader-image" style="display: none;" src="<?php echo plugin_dir_url( __FILE__ ) ?>/images/loader.gif">
	        		<a class="button button-secondary preview-btn-cancel" onclick="return confirm('Are you sure you want to cancel adding <?php echo $preview_data['productName']; ?>?')" href="<?php echo esc_url(admin_url('admin-post.php?action=cancel_single_member')); ?>">Cancel <span>&times;</span></a>
	        	</td>
	        </tr>
	        <?php elseif (get_option('atdwds_product_preview_error') === 'empty'): ?>
        	<tr>
        		<td colspan="3" class="preview-not-found"><strong>Can't find a listing with with member number <?php echo get_option('atdwds_add_single_member'); ?></strong></td>
        	</tr>
	        <?php endif; ?>
	    </table>
	    <?php if (empty(get_option('atdwds_add_single_member')) || get_option('atdwds_product_preview_error') === 'empty'): ?>
		<?php submit_button() ?>
		<?php endif; ?>
		<hr/>
		<?php
			$updated_products = json_decode(get_option( 'atdwds_last_updated_products' ), true);
			$last_checked = get_option( 'atdwds_last_attempted_update_time' );
			$auto_active = get_option( 'atdwds_auto_sync' ) === 'auto_sync' ? true : false;
		?>

		<div style="padding: 20px 0;">
			<?php if($current_product_numbers !== null): ?>
				<a style="margin-bottom: 20px;" class="button button-primary" data-get-data data-check-btn href="<?php echo esc_url(admin_url('admin-post.php?action=call_updated_products')); ?>">Check for Updates to Listings</a>
				<img data-loader-image class="atdwds-loader-image updates-loader" style="display: none;" src="<?php echo plugin_dir_url( __FILE__ ) ?>/images/loader.gif">
			<?php endif; ?>
			<?php if($auto_active): ?>
				<div>
					<h3 class="green-text">Daily Auto Sync On</h3>
				</div>
			<?php endif; ?>
			<?php if(!empty($last_checked)): ?>
				<h3>Last checked for updates: <?php echo date('d M Y @ h:i',strtotime($last_checked)); ?></h3>
			<?php endif; ?>
			<?php if(!empty($updated_products)): ?>
	    	<h3>Last updates found: <?php echo date('d M Y @ h:i',strtotime(get_option('atdwds_last_updated_time'))); ?></h3>
	    	<table class="widefat fixed" cellspacing="0">
			    <thead>
			    <tr>
		            <th id="cb" class="column-cb check-column" scope="col"></th>
		            <th id="columnname" class="column-columnname" scope="col"><strong>Product Name</strong></th>
		            <th id="columnname" class="column-columnname" scope="col"><strong>ATDW Category</strong></th>
		            <th id="columnname" class="column-columnname" scope="col"><strong>Product Number</strong></th>
		            <th id="columnname" class="column-columnname" scope="col"><strong>Product ID</strong></th>
		            <th id="columnname" class="column-columnname" scope="col"><strong>Status</strong></th>
			    </tr>
			    </thead>
			    <tbody>
			    	<?php
			    	$updated_ids = array();
			    	$expired = array();
			    	foreach($updated_products as $product) {
			    		$updated_ids[] = $product['product_ID'];

			    		if ($product['status'] == 'INACTIVE' || $product['status'] == 'EXPIRED') {
			    			$expired[] = $product['product_ID'];
			    		}
			    	}

			    	$updated_posts_args = array(
						'post_type' => $custom_post_types,
						'post_status' => array('draft', 'publish', 'pending', 'trash'),
						'paged' => false,
						'posts_per_page' => -1,
			            'update_post_term_cache' => false,
			            'no_found_rows' => true,
						'meta_query' => array(
						   array(
						       'key' => 'product_id',
						       'value' => $updated_ids,
						       'compare' => 'IN',
						   )
						)
					);
					$updated_query = new WP_Query($updated_posts_args);
					$updated_posts = $updated_query->posts;

			    	$index = -1;
			    	?>
			    	<?php foreach($updated_posts as $post): ?>
			    		<?php setup_postdata( $post ); ?>
				    	<?php
				    	$index++;
				    	$product_id = get_field('product_id');
				    	?>
				    	<?php if (!in_array($product_id, $expired)): ?>
				        <tr class="alternate">
				            <th scope="row"><?php echo $index + 1; ?></th>
					        <td class="column-columnname">
                                <a target="_blank" href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a> -
                                <a href="<?php echo get_site_url(); ?>/wp-admin/post.php?post=<?php echo $post->ID; ?>&action=edit">edit</a></td>
				            <td class="column-columnname"><?php echo strtoupper($post->post_type); ?></td>
				            <td class="column-columnname"><?php the_field('product_number'); ?></td>
				            <td class="column-columnname"><?php echo $product_id; ?></td>
				            <td class="column-columnname"><?php echo $post->post_status; ?></td>
				        </tr>
					    <?php else: ?>
					        <tr class="alternate expired">
					            <th scope="row"><?php echo $index + 1; ?></th>
					            <td class="column-columnname"><?php the_title(); ?> <a href="<?php echo get_site_url(); ?>/wp-admin/post.php?post=<?php echo $post->ID; ?>&action=edit">edit</a></td>
					            <td colspan="2" class="column-columnname"><strong>ATDW listing has been made inactive by operator or has expired.</strong></td>
					            <td class="column-columnname"><?php the_field('product_id'); ?></td>
					            <td class="column-columnname"><?php echo $post->post_status; ?></td>
					        </tr>
					    <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
			    	<?php endforeach; ?>
			    </tbody>
			</table>
			<?php endif ?>
		</div>
		
		<hr/>

		<?php

			$current_numbers_arr = explode(",",$current_product_numbers);
			$paged = isset($_GET['paged']) ? $_GET['paged'] : 1;

			$cpt_args = array(
				'post_type' => $custom_post_types,
				'post_status' => array('draft', 'publish', 'pending', 'trash'),
				'posts_per_page' => 80,
				'orderby' => 'date',
            	'order' => 'DESC',
				'paged' => $paged,
				'update_post_term_cache' => false,
				'meta_query' => array(
				   array(
				       'key' => 'product_id',
				       'value' => $current_numbers_arr,
				       'compare' => 'IN',
				   )
				)
			);
			$cpt_query = new WP_Query($cpt_args);
			$posts = $cpt_query->posts;
		?>
		<?php if ($posts): ?>
		<h3>Current ATDW Listings</h3>
		<div class="pagination">	
		<?php 
            $cpt_query->query_vars['paged'] > 1 ? $current = $cpt_query->query_vars['paged'] : $current = 1;

		    echo paginate_links( array(
		        'base'      => @add_query_arg('paged','%#%'),
		        'format'    => '?paged=%#%',
		        'prev_text' => __('Previous'),
		        'next_text' => __('Next'),
		        'current'   => $current,
		        'total'     => $cpt_query->max_num_pages
		    ));
	    ?>
		</div>
		<table class="widefat fixed existing-listing-table" cellspacing="0">
		    <thead>
		    <tr>
	            <th id="cb" class="column-cb check-column" scope="col"></th>
	            <th class="column-columnname" scope="col" style="width: 65px;"></th>
	            <th class="column-columnname" scope="col" style="width: 25%;"><strong>Product Name</strong></th>
	            <th class="column-columnname" scope="col" style="width: 10%;"><strong>ATDW Category</strong></th>
	            <th class="column-columnname" scope="col" style="width: 10%;"><strong>Product Number</strong></th>
	            <th class="column-columnname" scope="col" style="width: 15%;"><strong>Product ID</strong></th>
	            <th class="column-columnname" scope="col" style="width: 10%;"><strong>WP Status</strong></th>
	            <th class="column-columnname" scope="col" style="width: 6%;"><strong>Remove</strong></th>
		    </tr>
		    </thead>
		    <tbody>
		    	<?php $index = 0; ?>
	    		<?php foreach( $posts as $post ):
	    			setup_postdata( $post );
	    			$status = get_post_status( $post->ID );
	    			$expired = $status === 'draft' ? 'expired' : '';
		    		$index++; ?>
		        <tr class="alternate <?php echo $expired; ?>">
		            <th style="white-space: nowrap; width: 65px;" scope="row"><?php echo $index; ?></th>
		            <td class="column-columnname">
		            	<a data-update class="button button-secondary" href="<?php echo esc_url(admin_url('admin-post.php?action=call_update_product&productID='.get_field('product_id'))); ?>">
		            	<span>Update</span>
		            	<img data-update-image class="atdwds-update-image update-loader" style="display: none;" src="<?php echo plugin_dir_url( __FILE__ ) ?>/images/updating.gif">
		            	</a>
		            </td>
		            <td class="column-columnname"><?php echo get_the_title(); ?> <a href="<?php echo get_site_url(); ?>/wp-admin/post.php?post=<?php echo $post->ID; ?>&action=edit">edit</a></td>
		            <td class="column-columnname"><?php echo get_field('product_category_id'); ?></td>
		            <td class="column-columnname"><?php echo get_field('product_number'); ?></td>
		            <td class="column-columnname"><?php echo get_field('product_id'); ?></td>
		            <td class="column-columnname"><?php echo $status; ?></td>
		            <td><a onclick="return confirm('Are you sure? <?php echo get_the_title(); ?> will be permanently deleted!')" class="remove-listing-link" href="<?php echo esc_url(admin_url('admin-post.php?action=remove_product_from_wp&post_id='.$post->ID)); ?>">&times;</a></td>
		        </tr>
	    		<?php endforeach; ?>
		    </tbody>
		</table>
		<div class="pagination">	
		<?php 
            $cpt_query->query_vars['paged'] > 1 ? $current = $cpt_query->query_vars['paged'] : $current = 1;

		    echo paginate_links( array(
		        'base'      => @add_query_arg('paged','%#%'),
		        'format'    => '?paged=%#%',
		        'prev_text' => __('Previous'),
		        'next_text' => __('Next'),
		        'current'   => $current,
		        'total'     => $cpt_query->max_num_pages
		    ));
        ?>
		</div>
	    <?php wp_reset_postdata(); ?>
		<?php endif; ?>
	</form>

</div>