<?php
/**
 * Template Name: Event Calendar
 */

$current_month = date('m');
$current_year = date('Y');
$next_year = (int)$current_year + 1;

$months = array('01' => 'Jan', '02' => 'Feb', '03' => 'Mar', '04' => 'Apr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', '08' => 'Aug', '09' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');

$all_regions = get_categories(array(
	'taxonomy' => 'category',
	'hide_empty' => true,
));

$regions = array(); // Create array of just regions that have events
foreach($all_regions as $region) {
	$region_args = array(
		'posts_per_page'		 => -1,
		'post_type'				 => 'event',
		'category_name'          => $region->slug,
		'post_status'            => 'publish',
		'update_post_term_cache' => false,
		'paged'					 => false,
	);
	$region_query = new WP_Query($region_args);

	if ($region_query->post_count > 0) {
		$regions[] = $region;
	}
	wp_reset_postdata();
}

$regions_param = isset($_GET['rgn']) ? $_GET['rgn'] : null;
$month_param = isset($_GET['mth']) ? $_GET['mth'] : null;
$year_param = isset($_GET['yr']) ? $_GET['yr'] : null;

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/global-hero-short'); ?>

<section class="collapse-top event-calendar grey-bg">
	<div class="row">
		<div class="small-12 columns breadcrumb--dark">
            <?php get_template_part('components/breadcrumb/breadcrumb'); ?>
        </div>
		<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center bm underline">
			<h1><?php the_title(); ?></h1>
			<?php if (have_posts()) {
				while (have_posts()) : the_post();
					the_content();
				endwhile;
			}?>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns">
			<form class="filter" action="<?php the_permalink(); ?>">
				<ul class="filter__month-list">
					<?php foreach($months as $month_int => $month): ?>
						<?php
						$month_str = (string)$month_int;
						$month_param_str = (string)$month_param;
						$past_month =  (string)$year_param === (string)$next_year ? null :  ((int)$month_int < (int)$current_month ? 'past' : null);
						$month_active = $month_param ? ($month_param_str === $month_str ? 'checked' : null) : ($current_month === $month_str ? 'checked' : null);
						?>
						<li class="filter__month-list--item <?php echo $past_month; ?>"><input <?php echo $month_active; ?> id="<?php echo $month_str ?>" type="radio" name="mth" value="<?php echo $month_str ?>"><label for="<?php echo $month_str ?>"><?php echo $month ?></label></li>
					<?php endforeach; ?>
					<?php $next_year = (string)$next_year; ?>
					<?php $selected_current_year = $year_param ? ($year_param === $current_year ? 'checked' : null) : 'checked'; ?>
					<?php $selected_next_year = $year_param ? ($year_param === $next_year ? 'checked' : null) : null; ?>
					<li class="filter__month-list--item year year--current"><input <?php echo $selected_current_year; ?> id="<?php echo $current_year; ?>" type="radio" name="yr" value="<?php echo $current_year; ?>"><label for="<?php echo $current_year; ?>"><?php echo $current_year; ?></label></li>
					<li class="filter__month-list--item year year--next"><input <?php echo $selected_next_year; ?> id="<?php echo $next_year; ?>" type="radio" name="yr" value="<?php echo $next_year; ?>"><label for="<?php echo $next_year; ?>"><?php echo $next_year; ?></label></li>
				</ul>
				<ul class="filter__region-list">
					<?php $all_regions_current = $regions_param === '' || !$regions_param ? 'checked' : null; ?>
					<li class="filter__region-list--item"><input <?php echo $all_regions_current; ?> id="all-regions" type="radio" name="rgn" value=""><label for="all-regions">See All</label></li>
					<?php foreach($regions as $region): ?>
						<?php $region_current = $regions_param === $region->slug ? 'checked' : null; ?>
						<li class="filter__region-list--item"><input <?php echo $region_current; ?> id="<?php echo $region->slug; ?>" type="radio" name="rgn" value="<?php echo $region->slug; ?>"><label for="<?php echo $region->slug; ?>"><?php echo $region->name; ?></label></li>
					<?php endforeach; ?>
				</ul>
				<!-- <button type="submit" class="btn btn-small btn-tertiary btn-filter">Filter Events</button> -->
			</form>
		</div>
	</div>
	<?php if(get_field('button_page_link')): ?>
	<div class="row">
		<div class="small-12 columns text-center">
			<a class="btn btn-medium btn-primary btn-arrow-black" href="<?php the_field('button_page_link'); ?>"><?php the_field('button_text'); ?></a>
		</div>
	</div>
	<?php endif; ?>
</section>
<?php
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$calendar_start_date = $month_param || $year_param ? date($year_param.'-'.$month_param.'-01') : date($current_year.'-'.$current_month.'-01');

$calendar_args = array(
	'posts_per_page'		 => 12,
	'post_type'				 => 'event',
	'category_name'          => $regions_param ? $regions_param : null,
	'post_status'            => 'publish',
	'update_post_term_cache' => false,
	'paged'                  => $paged,
	'meta_query' => array(
        'date_clause' => array(
    	    'key'     => 'start_time',
            'value' => '',
        	'compare' => '!=',
     	),
     	'start' => array(
     		'key' => 'start_time',
			'value' => $calendar_start_date,
			'type' => 'DATE',
			'compare' => '>='
     	),
    ),
    'orderby' => 'start',
    'order' => 'ASC'
);

$wp_query = new WP_Query($calendar_args);
$count = $wp_query->post_count;

if ($count > 0): ?>
	<section class="event-calendar white-bg">
		<div class="row">
			<?php while ( $wp_query->have_posts()) :
			    $wp_query->the_post();?>
				<div class="events-article small-12 medium-6 large-4 columns">
					<?php get_template_part('components/events-tile-link/events-tile-link'); ?>
				</div>
			<?php endwhile; ?>
		</div>
		<div class="row">
	        <div class="small-12 columns">
	            <?php include(locate_template( 'components/pagination/pagination.php')); ?>
	        </div>
	    </div>
	</section>
<?php wp_reset_postdata(); ?>
<?php else: ?>
	<section class="event-calendar no-results white-bg">
		<div class="row">
			<div class="small-12 columns text-center">
				<p>Sorry, we couldn't find any events for your selected dates.</p>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php
$calendar_args = array(
	'posts_per_page'		 => -1,
	'post_type'				 => 'event',
	'category_name'          => null,
	'post_status'            => 'publish',
	'update_post_term_cache' => false,
	'meta_query' => array(
        'date_clause' => array(
    	    'key'     => 'reoccurring_event',
            'value' => '1',
        	'compare' => '==',
     	),
    ),
    'orderby' => array(
        'date_clause'     => 'DESC',
    ),
);

$wp_query = new WP_Query($calendar_args);
$count = $wp_query->post_count;

if ($count > 0): ?>
<section class="event-calendar grey-bg">
	<div class="row">
		<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center bm underline">
			<h2>Recurring Events</h2>
		</div>
	</div>
	<div class="row">
		<?php while ( $wp_query->have_posts()) :
		    $wp_query->the_post();?>
			<div class="events-article small-12 medium-6 large-4 columns">
				<?php get_template_part('components/events-tile-link/events-tile-link'); ?>
			</div>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	</div>
</section>
<?php endif; ?>

<?php get_footer()?>