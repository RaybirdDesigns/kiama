<?php
$post_type = get_post_type();
$post_type_obj = get_post_type_object($post_type);
$post_type_name = $post_type_obj->label;
$rewrite_slug = $post_type_obj->rewrite['slug'];

$primary_category = get_post_primary_category($post->ID, $term='category', $return_all_categories=false);

$category_name = !empty($primary_category) ? $primary_category['primary_category']->name : get_field('product_city_name');

$terms = get_the_terms( $post->ID, $post_type.'_type');
if (!empty( $terms )) {

	$primary_term = get_post_primary_category($post->ID, $term=$post_type.'_type', $return_all_categories=false);
	$primary_term_id = $primary_term['primary_category']->term_id;
	$primary_term_name = $primary_term['primary_category']->name;
	$primary_term_slug = $primary_term['primary_category']->slug;

	$term_names_arr = array();
	foreach($terms as $term) {
		if ($term->slug !== 'nothing' && $term->term_id !== $primary_term_id) {
			$term_names_arr[$term->slug] = '#'.$term->name;
		}
	}
	// Get first term
	$term = array_shift( $terms );
	$term_slug = $term->slug;
}

$product_lat = get_field('product_lat');
$product_long = get_field('product_long');

$feature_image_id = get_post_thumbnail_id($post->ID) ? get_post_thumbnail_id($post->ID) : null;

$image_0 = get_field('product_image_0');
$image_1 = get_field('product_image_1');
$image_2 = get_field('product_image_2');
$image_3 = get_field('product_image_3');
$image_4 = get_field('product_image_4');
$image_5 = get_field('product_image_5');
$image_6 = get_field('product_image_6');
$image_7 = get_field('product_image_7');
$image_8 = get_field('product_image_8');
$image_9 = get_field('product_image_9');

$youtubeID = null;
$product_video_0 = get_field('product_video_0');
if( strpos( $product_video_0, 'youtube' ) !== false) {
    parse_str( parse_url( $product_video_0, PHP_URL_QUERY ), $vars );
	$youtubeID = $vars['v'];
}

$vimeoID = null;
if( strpos( $product_video_0, 'vimeo.com' ) !== false) {
    parse_str( parse_url( $product_video_0, PHP_URL_PATH ), $vars );
    $keys = array_keys($vars);
    if (count($keys) > 0) {
        $vimeoID = substr($keys[0], 1);
    }
}

$product_video_thumb_0 = get_field('product_video_thumb_0') ? get_field('product_video_thumb_0') : 'http://i3.ytimg.com/vi/'.$youtubeID;

$product_booking_url = get_field('product_booking_url') !== 'http://www.kiama.com.au' ? get_field('product_booking_url') : null;
$product_enquiries_url = get_field('product_enquiries_url') !== 'http://www.kiama.com.au' ? get_field('product_enquiries_url') : null;
// $web_site = $web_site_field !== 'http://www.kiama.com.au' ? $web_site_field : null;

$bookeasy_id = get_field('bookeasy_id');

$product_facebook = get_field('product_facebook');
$product_twitter = get_field('product_twitter');
$product_instagram = get_field('product_instagram');

$product_primary_phone = get_field('product_primary_phone');
$product_secondary_phone = get_field('product_secondary_phone') !== $product_primary_phone ?  get_field('product_secondary_phone') : null;
$product_email = get_field('product_email');

$product_address_1 = get_field('product_address_1');
$product_address_2 = get_field('product_address_2');
$product_country_name = get_field('product_country_name');
$product_state_name = get_field('product_state_name');
$product_post_code = get_field('product_post_code') ? ', '.get_field('product_post_code') : null;
$product_city_name = get_field('product_city_name');

$product_accessibility = get_field('product_accessibility');
$product_attributes = array_filter(explode(',',get_field('product_attributes')));

$cancellation_policy = get_field('cancellation_policy');

$custom_event_dates = get_field('custom_event_dates');
$reoccurring_event = get_field('reoccurring_event');

$start_time = get_field('start_time');
$end_time = get_field('end_time');
$start_time_formatted = $start_time ? date('d M Y',strtotime($start_time)) : null;
$end_time_formatted = $start_time !== $end_time ? ' — '.date('d M Y',strtotime($end_time)) : null;

$product_rate_from = get_field('product_rate_from');
$product_rate_to = get_field('product_rate_to') && $product_rate_from !== get_field('product_rate_to') ? ' to $'.get_field('product_rate_to') : null;

$no_detail = !$start_time && !$product_rate_from ? 'listing__map--tall' : '';

// Custom Gallery Images
$image_gallery = get_field('image_gallery') ? get_field('image_gallery') : array();

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/global-hero-short'); ?>

<section class="listing <?php echo $post_type; ?>">
	<div class="row">
		<div class="small-12 columns breadcrumb--dark">
			<?php get_template_part('components/breadcrumb/breadcrumb'); ?>
			<h1><?php the_title(); ?></h1>
			<?php if ($bookeasy_id && $post_type !== 'destinfo' && ($post_type === 'accomm' || $post_type === 'tour' || $post_type === 'attraction')): ?>
				<a title="Scroll to Book" href="#detail-gadget" data-book-now
						class="btn btn-primary btn-medium book-now-btn-mobile"
						aria-haspopup="true">Book Now</a>
			<?php endif; ?>
		</div>
	</div>
	<?php
	$today = date('Ymd');
	$end_date = $end_time ? $end_time : $start_time;
	$expiredate = date('Ymd',strtotime($end_date));

	if ($post_type === 'event' && $end_date && $expiredate < $today): ?>
	<div class="row expired">
		<div class="small-12 columns text-center">
			<p>This event has ended. <a href="<?php echo site_url() ?>/whats-on/events-calendar">See&nbsp;Our&nbsp;Events&nbsp;Calendar</a></p>
		</div>
	</div>
	<?php endif; ?>

	<div class="row listing-body">
		<!-- Map -->
		<div class="small-12 large-4 columns listing-body--left">
			<?php if ($start_time || $product_rate_from || $custom_event_dates): ?>
			<div class="listing__detail listing__detail--wide">
				<?php if ($custom_event_dates): ?>
				<div class="listing__detail--dates-custom">
					<p><?php echo $custom_event_dates; ?></p>
				</div>
				<?php endif; ?>
				<?php if ($start_time): ?>
				<?php $end_date_rendered = $reoccurring_event ? '' : $end_time_formatted; ?>
				<?php $event_messaging = $reoccurring_event ? 'Event Starts:' : 'Event Dates:'; ?>
				<div class="listing__detail--dates">
					<p><span><?php echo $event_messaging; ?> </span><?php echo $start_time_formatted;  echo $end_date_rendered; ?></p>
				</div>
				<?php endif; ?>
				<?php if ($product_rate_from): ?>

				<div class="listing__detail--rates">
					<p><span>Rates From: </span> $<?php echo $product_rate_from;?><?php echo $product_rate_to; ?></p>
				</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>
			<?php if($product_lat && $product_long): ?>
			<div class="listing__map google-map <?php echo $no_detail; ?>">
				<div class="marker" data-lat="<?php echo $product_lat; ?>" data-lng="<?php echo $product_long; ?>"></div>
			</div>
			<?php endif; ?>
			
			<?php if ($bookeasy_id || $product_booking_url || $product_enquiries_url): ?>
			<!-- Buttons -->
			<div class="listing__detail-btns">
				<?php if ($bookeasy_id && $post_type !== 'destinfo' &&
                    ($post_type === 'accomm' || $post_type === 'tour' || $post_type === 'attraction')): ?>
					<a title="Scroll to Book" href="#detail-gadget" data-book-now
							class="btn btn-primary btn-medium btn-50 book-now-btn-desk"
							aria-haspopup="true">Book Now</a>
				<?php endif; ?>
				<?php if ($product_enquiries_url): ?>
					<a target="_blank" href="<?php echo addhttp($product_enquiries_url); ?>" class="btn btn-tertiary btn-medium btn-50 btn-ext"><span>Website</span></a>
				<?php endif; ?>
				<?php if ($product_booking_url && $post_type !== 'accomm'): ?>
					<a target="_blank" href="<?php echo addhttp($product_booking_url); ?>" class="btn btn-primary btn-medium btn-50 btn-ext"><span>Book Now</span></a>
				<?php endif; ?>
			</div>
			<?php endif; ?>

			<!-- Contact deatils -->
			<div class="listing__detail-contact">
				<ul class="listing__detail-contact--list">
					<?php if ($product_primary_phone || $product_secondary_phone): ?>
					<li>
						<svg class="icon" role="presentation">
						    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#phone-ico"></use>
						</svg>
						<a href="tel:<?php echo $product_primary_phone ?>"><?php echo $product_primary_phone ?></a> <?php echo $or = $product_secondary_phone ? '&nbsp;or&nbsp;' : null; ?> <a href="tel:<?php echo $product_secondary_phone; ?>"><?php echo $product_secondary_phone; ?></a>
					</li>
					<?php endif; ?>
					<?php if ($product_email): ?>
					<li>
						<svg class="icon" role="presentation">
						    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#email-ico"></use>
						</svg>
						<a href="mailto:<?php echo $product_email; ?>"><?php echo $product_email; ?></a>
					</li>
					<?php endif; ?>
					<?php if ($product_address_1 || $product_address_2 || $product_city_name || $product_state_name || $product_country_name): ?>
					<li>
						<svg class="icon" role="presentation">
						    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#home-ico"></use>
						</svg>
						<ul>
							<?php if ($product_address_1): ?>
								<li><?php echo $product_address_1; ?></li>
							<?php endif; ?>
							<?php if ($product_address_2): ?>
								<li><?php echo $product_address_2; ?></li>
							<?php endif; ?>
							<?php if ($product_city_name): ?>
								<li><?php echo $product_city_name; ?></li>
							<?php endif; ?>
							<?php if ($product_state_name): ?>
								<li><?php echo $product_state_name; ?><?php echo $product_post_code; ?></li>
							<?php endif; ?>
							<?php if ($product_country_name): ?>
							<li><?php echo $product_country_name; ?></li>
							<?php endif; ?>
						</ul>
					</li>
					<?php endif; ?>
				</ul>
			</div>
			
			<?php if ($product_facebook || $product_twitter || $product_instagram): ?>
			<div class="listing__social">
				<ul class="listing__social--list">
					<?php if($product_facebook): ?>
					<li><a title="Facebook" target="_blank" href="<?php echo addhttp($product_facebook); ?>"><span class="show-for-sr">Facebook</span>
						<svg class="icon" role="presentation">
						    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/global-sprite.svg#facebook-ico"></use>
						</svg>
					</a></li>
					<?php endif; ?>
					<?php if($product_twitter): ?>
					<li><a title="Twitter" target="_blank" href="<?php echo addhttp($product_twitter); ?>"><span class="show-for-sr">Twitter</span>
						<svg class="icon" role="presentation">
						    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/global-sprite.svg#twitter-ico"></use>
						</svg>
					</a></li>
					<?php endif; ?>
					<?php if($product_instagram): ?>
					<li><a title="Instagram" target="_blank" href="<?php echo addhttp($product_instagram); ?>"><span class="show-for-sr">Instagram</span>
						<svg class="icon" role="presentation">
						    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/global-sprite.svg#instagram-ico"></use>
						</svg>
					</a></li>
					<?php endif; ?>
				</ul>
			</div>
			<?php endif; ?>

			<!-- Videos -->
			<?php if ($youtubeID): ?>
			<div class="listing__video">
				<button class="listing__video__btn" data-youtube="<?php echo $youtubeID; ?>">
			    	<span class="show-for-sr">Play Video</span>
					<img src="<?php echo $product_video_thumb_0; ?>" alt="kiama video">
					<svg class="listing__video__btn--ico" role="presentation">
					    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#play-ico"></use>
					</svg>
			    </button>
			</div>
			<?php elseif ($vimeoID): ?>
			<div class="listing__video">
				<button class="listing__video__btn" data-vimeo="<?php echo $vimeoID; ?>">
			    	<span class="show-for-sr">Play Video</span>
					<img src="<?php echo $product_video_thumb_0; ?>" alt="kiama video">
					<svg class="listing__video__btn--ico" role="presentation">
					    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#play-ico"></use>
					</svg>
			    </button>
			</div>
			<?php endif; ?>
		</div>
		<div class="small-12 large-8 columns listing-body--right">

			<div class="listing__detail">
				<?php if ($category_name): ?>
					<p class="listing__detail--region"><span>Area</span> <?php echo $category_name; ?></p>
				<?php endif; ?>
				<?php if (!empty($term_names_arr) || !empty($primary_term)): ?>
					<p class="listing__detail--subcat">
						<svg class="listing__detail--icon" role="presentation">
				            <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/icons.svg#<?php echo $primary_term_slug; ?>"></use>
				        </svg>
				        <?php if(!empty($primary_term)): ?>
				        	<a href="<?php echo site_url().'/'.$rewrite_slug.'/?tax='.$primary_term_slug; ?>">#<?php echo $primary_term_name; ?></a>
				        <?php endif; ?>
				        <?php foreach($term_names_arr as $slug => $name): ?>
							<a href="<?php echo site_url().'/'.$rewrite_slug.'/?tax='.$slug; ?>"><?php echo $name; ?></a>
				        <?php endforeach; ?>
					</p>
				<?php endif; ?>

				<button data-postid="<?php echo $post->ID; ?>" onclick="window.addFavourite(<?php echo $post->ID; ?>)" class="favourite-btn">
					<svg class="icon" role="presentation">
					    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/global-sprite.svg#fav-ico"></use>
					</svg>
				</button>
			</div>

			<?php
			$listing_images = array($image_0, $image_1, $image_2, $image_3, $image_4, $image_5, $image_6, $image_7, $image_8, $image_9);
			$listing_images_arr = array_filter($listing_images);
			$image_count = count(array_merge($listing_images_arr, $image_gallery));
			$feature_image_id = get_post_thumbnail_id($post->ID);
			?>

			<?php if ($image_count > 0 || $feature_image_id): ?>
				<div data-listing-carousel data-arrows="true" class="listing__carousel">
<?php echo $feature_image_id; ?>
					<?php if ($feature_image_id): ?>
						<div class="listing__carousel--item">
							<img data-object-fit="cover" <?php responsive_image_post('large', false); ?>>
						</div>
					<?php endif; ?>

					<?php if (!empty($image_gallery)): ?>
						<?php foreach($image_gallery as $gallery_image): ?>
						<div class="listing__carousel--item">
							<img data-object-fit="cover" <?php responsive_image($gallery_image['ID'], 'large', false); ?>>
						</div>
						<?php endforeach; ?>
					<?php endif; ?>
					
					<?php
					$image_index = -1;
					foreach($listing_images_arr as $image): ?>
						<?php $image_index++; ?>
						<div class="listing__carousel--item">
							<img data-object-fit="cover" <?php adtw_image(900, $image_index, false); ?>>
						</div>
					<?php endforeach; ?>
				</div>

				<?php if ($image_count > 0): ?>
				<div data-listing-thumbs class="listing__carousel__thumbs">
					<?php if ($feature_image_id): ?>
						<div class="listing__carousel--thumb">
							<img data-object-fit="cover" <?php responsive_image_post('large', false); ?>>
						</div>
					<?php endif; ?>

					<?php if (!empty($image_gallery)): ?>
						<?php foreach($image_gallery as $gallery_image): ?>
						<div class="listing__carousel--thumb">
							<img data-object-fit="cover" <?php responsive_image($gallery_image['ID'], 'large', false); ?>>
						</div>
						<?php endforeach; ?>
					<?php endif; ?>

					<?php 
					$thumbs_index = -1;
					foreach($listing_images_arr as $thumb): ?>
						<?php $thumbs_index++; ?>
						<div class="listing__carousel--thumb">
							<img data-object-fit="cover" <?php adtw_image(900, $thumbs_index, false); ?>>
						</div>
					<?php endforeach; ?>
				</div>
				<?php endif; ?>
			<?php endif; ?>

			<?php if (have_posts()) : ?>
			<div class="listing__content">
				<?php
				while (have_posts()) : the_post();
					the_content();
				endwhile;
				?>
			</div>
			<?php endif; ?>

			<?php if($product_accessibility): ?>
			<!-- Accessible -->
			<hr/>
			<div class="listing__accessible">
				<svg class="listing__accessible--icon" role="presentation">
				    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#accessible-ico"></use>
				</svg>
				<div class="listing__accessible--wrapper">
					<h4>Accessibility</h4>
					<p><?php echo $product_accessibility; ?></p>
				</div>
			</div>
			<?php endif; ?>

			<?php if(!empty($product_attributes)): ?>
			<!-- facilities -->
			<hr/>
			<div class="listing__facilities">
				<svg class="listing__facilities--icon" role="presentation">
				    <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/icons.svg#itinerary"></use>
				</svg>
				<div class="listing__facilities--wrapper">
					<h4>Facilities</h4>
					<ul class="listing__facilities--list">
						<?php foreach($product_attributes as $attr): ?>

						<li><?php echo $attr; ?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
			<?php endif; ?>

			<?php if($cancellation_policy): ?>
			<!-- Cancellations -->
			<hr/>
			<div class="listing__cancellations">
				<p><strong>Cancellations:</strong> <?php echo $cancellation_policy; ?></p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>


<?php if ($bookeasy_id && $post_type !== 'destinfo' &&
    ($post_type === 'accomm' || $post_type === 'tour' || $post_type === 'attraction')): ?>
<script type="text/javascript">
	BEcssOverride = "minimal";
    $w(function() {
        BE.gadget.details('#be-details-widget',{
            vcID: 			129	        
        });

        BE.gadget.cart('#be-cart-widget',{
            vcID: 			129,
            bookingURL: 	'/checkout',
            descriptionHover: false,
            showHoverInline: true,
            overlaySettings:     {
			    useBlockout:     true,
			    overlayColour:   '#000000',
			    overlayOpacity:  0.6,
			    innerBackground: '#FFF',
			    zIndexLowest:    1000000,
			    width:           '100%',
			    height:          '100%'
			},
        });
    });
</script>
<section id="detail-gadget" class="book">
	<div class="row">
		<div class="small-12 columns">
			<div id="be-cart-widget"></div>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns">
			<h2>Make a booking...</h2>
			<div id="be-details-widget"></div>
		</div>
	</div>
</section>
<?php endif; ?>

<?php
// Related Listings

$listing_related_heading = get_field('listing_related_heading', 'options');
$listing_button_link = get_field('listing_button_link', 'options');
$listing_button_text = get_field('listing_button_text', 'options');


$listing_args = array(
	'posts_per_page'		 => 3,
	'post_type'				 => $post_type,
	'post_status'            => 'publish',
	'update_post_term_cache' => false,
	'paged'					 => false,
	'post__not_in'           => array(get_the_ID()),
	'orderby'        		 => 'rand'
);

// For events, only get events that are happening in the future.
if ($post_type === 'event') {
	$current_month = date('m');
	$current_year = date('Y');

	$event_start_args = array(
        'date_clause' => array(
    	    'key'     => 'start_time',
            'value' => '',
        	'compare' => '!=',
     	),
     	'start' => array(
     		'key' => 'start_time',
			'value' => date($current_year.'-'.$current_month.'-01'),
			'type' => 'DATE',
			'compare' => '>='
     	),
    );

	$listing_args['meta_query'] = $event_start_args;
}

$listing_query = new WP_Query($listing_args);
$count = $listing_query->post_count;

if ($count > 0) : ?>
	<section class="listing-related">
		<div class="row">
			<div class="small-12 columns text-center underline">
				<?php if ($listing_related_heading): ?>
					<div class="row">
						<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center underline">
							<?php echo $listing_related_heading; ?>
						</div>
					</div>
				<?php endif; ?>
				<div class="row">
					<div class="small-12 columns text-center">
						<?php if ($post_type === 'event'): ?>
							<a class="btn btn-medium btn-tertiary btn-arrow btn-margin-b" href="<?php echo site_url().'/events-calendar'; ?>">Events Calendar</a>
						<?php else: ?>
							<a class="btn btn-medium btn-tertiary btn-arrow btn-margin-b" href="<?php echo site_url().'/'.$rewrite_slug; ?>">See all <?php echo $post_type_name; ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row align-center">
			<?php while ( $listing_query->have_posts() ) :
	    	$listing_query->the_post();
	    	$post_type = get_post_type();
			?>
			<div class="listing-article small-12 medium-4 columns <?php echo $post_type; ?>">
				<?php if ($post_type === 'event'): ?>
					<?php get_template_part('components/events-tile-link/events-tile-link'); ?>
				<?php else: ?>
					<?php get_template_part('components/tile-link/tile-link'); ?>
				<?php endif; ?>
			</div>
			<?php endwhile; ?>
		</div>
	</section>
<?php wp_reset_postdata(); ?>
<?php endif; ?>

<img class="show-for-sr" src="<?php the_field('product_pixel_url'); ?>">
<script src="https://maps.googleapis.com/maps/api/js?key=<?php the_field('google_maps_api_key', 'options'); ?>"></script>
<?php get_footer()?>