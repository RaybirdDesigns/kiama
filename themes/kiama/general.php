<?php
/**
 * Template Name: General
 */

$general_width = get_sub_field('general_col_text_width') ? get_sub_field('general_col_text_width') : get_field('general_col_text_width');
$general_columns = get_sub_field('general_columns') ? get_sub_field('general_columns') : get_field('general_columns');
$general_bg = get_sub_field('general_bg') ? get_sub_field('general_bg') : get_field('general_bg');

switch ($general_width) {
    case 'narrow':
        $width = 'medium-10 large-8 medium-offset-1 large-offset-2';
        break;
    case 'medium':
        $width = 'medium-8 large-10  medium-offset-2 large-offset-1';
        break;
    case 'wide':
        $width = 'large-12';
        break;
    default:
        $width = 'large-12';
}

switch ($general_columns) {
    case '1':
        $columns = 'text-col-1';
        break;
    case '2':
        $columns = 'text-col-2';
        break;
    case '3':
        $columns = 'text-col-3';
        break;
    default:
        $columns = '';
}

?>

<?php get_header()?>

<style>
	.text-col-1 {
		column-count: 1;
	}

	.text-col-2 {
		column-count: 2;
	}

	.text-col-3 {
		column-count: 3;
	}
</style>

<?php get_template_part('components/hero-banners/global-hero-short'); ?>

<?php if (have_posts()) : ?>
<section class="collapse-top bg--<?php echo $general_bg; ?>">
	<div class="row">
		<div class="small-12 columns breadcrumb--dark breadcrumb-row">
            <?php get_template_part('components/breadcrumb/breadcrumb'); ?>
        </div>
		<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center bm underline">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
	<div class="row">
		<div class="<?php echo $columns; ?> small-12 <?php echo $width; ?> columns">
			<?php
			while (have_posts()) : the_post();
				the_content();
			endwhile;
			?>
		</div>
	</div>
</section>
<?php endif; ?>

<?php get_footer()?>