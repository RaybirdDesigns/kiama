<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php if(is_admin()): ?>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url')?>">
        <?php endif; ?>
        <link rel="icon" type="image/png" href="<?php the_field('favicon', 'option') ?>" sizes="64x64" />
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
        <script>
            document.querySelector('html').classList.remove('no-js');
            document.querySelector('html').classList.add('js');
        </script>
        <?php // Schema.org data
            $current_post_type = get_post_type();
            if ($current_post_type == 'post'){
                include( locate_template('components/schema/blog-schema.php')); 
            } else if ($current_post_type == 'accomm') {
                include( locate_template('components/schema/accomm-schema.php'));
            } else if ($current_post_type == 'event') {
                include( locate_template('components/schema/event-schema.php'));
            } else if (is_single()) {
                include( locate_template('components/schema/listing-schema.php'));
            }

            $url = $_SERVER['SERVER_NAME'];
            $key = strpos($url, 'local') ? '234f91d80de8e89169dfd9de6928ce84' : get_field('bookeasy_key','options');
        ?>

        <?php wp_head()?>
        
        <?php if(get_field('bookeasy_demo_mode','options')): ?>
        <script>
            BEurlsOverride = {
                 cdn:function() { return "//gadgets-pvt.impartmedia.com/"; },
                 sjp:function() { return "//sjp-pvt.impartmedia.com/"; },
                 webapi:function() { return "https://webapi-pvt.bookeasy.com.au/"; },
            };
        </script>
        <?php endif; ?>
        <?php $is_demo = get_field('bookeasy_demo_mode','options') ? '-pvt' : null; ?>
        <script type="text/javascript" src="//gadgets<?php echo $is_demo; ?>.impartmedia.com/gadgets.jsz?key=<?php echo $key; ?>"></script>
        
        <?php the_field('header_scripts','option'); ?>
    </head>
    <body <?php body_class(); ?>>
    <?php the_field('body_scripts','option'); ?>
    <!--[if lt IE 11]>
    <p class="browser-upgrade text-center">You are using an <strong>outdated</strong> browser. Please <a target="_blank" href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <a class="skip-main" href="#content">Skip to main content</a>
    <div class="hide">
        <?php
        $auth = base64_encode("destinationkia:e8ea749e");
        $context = stream_context_create([
            "http" => [
                "header" => "Authorization: Basic $auth"
            ]
        ]);
        ?>
        <?php echo file_get_contents( htmlspecialchars(get_template_directory_uri() . '/images/sprites/global-sprite.svg'), false, $context); ?>
    </div>
    <main role="main">
        <?php $logo = get_field('logo', 'options'); ?>
        <header class="header">
            <button data-mobile-nav-btn
                class="header__nav-btn">w
                <span class="show-for-sr">Open Nav</span>
                <svg class="hamburger-ico icon" role="presentation">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/global-sprite.svg#hamburger-ico"></use>
                </svg>
            </button>
            <a title="Destination Kiama Home" class="header__home-link" href="<?php echo site_url(); ?>">
                <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">
            </a>
            <div class="header__right">
                <nav class="main-menu" role="navigation" aria-label="Main Menu">
                <button data-mobile-nav-btn-close
                    class="main-menu__close-btn">
                        <span class="show-for-sr">Close Nav</span>
                        <svg class="hamburger-ico icon" role="presentation">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/global-sprite.svg#cross-ico"></use>
                        </svg>
                </button>
                  <?php
                    if ( has_nav_menu( 'main-menu' ) ) {
                        wp_nav_menu( array(
                            'theme_location' => 'main-menu',
                            'container'      => false,
                            'menu_class'     => 'main-navigation',
                            'walker'         => new Aria_Walker_Nav_Menu(),
                            'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        ) );
                    }
                  ?>
                </nav>
                <button title="Search" class="header__search-btn">
                    <span class="show-for-sr">Open Search Dialog</span>
                    <svg class="search-ico" role="presentation">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/global-sprite.svg#search-ico"></use>
                    </svg>
                </button>
                <div class="cart-head">
                    <svg class="icon" role="presentation">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#cart-ico"></use>
                    </svg>
                    <div id="cart-widget-head"></div>
                </div>
                <a title="See Favourites" href="/favourites" class="header__fav-btn">
                    <span class="show-for-sr">Go to your Favourites</span>
                    <svg class="fav-ico" role="presentation">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/global-sprite.svg#fav-ico"></use>
                    </svg>
                </a>
            </div>
        </header>

        <script type="text/javascript">
            $w(function() {
                BE.gadget.cart('#cart-widget-head',{
                    vcID:           129,
                    bookingURL:     '/checkout',
                    autoCollapse: true,
                    overlaySettings:     {
                        useBlockout:     true,
                        overlayColour:   '#000000',
                        overlayOpacity:  0.6,
                        innerBackground: '#FFF',
                        zIndexLowest:    1000000,
                        width:           '100%',
                        height:          '100%'
                    },
                });
            });
        </script>

        <div id="content">
        
        