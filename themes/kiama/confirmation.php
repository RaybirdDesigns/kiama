<?php
/**
 * Template Name: Confirmation
 */
?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/global-hero-short'); ?>

<script type="text/javascript">
	$w(function() {
        BE.gadget.confirm('#confirm-gadget', {
        	thankYouText: '<?php the_field('thank_you_text'); ?>',
        	pdfLinkText: '<?php the_field('download_pdf_text'); ?>'
        });
    });
</script>

<section class="collapse-top bg--white">
	<div class="row">
		<div class="small-12 columns breadcrumb--dark breadcrumb-row bm">
            <?php get_template_part('components/breadcrumb/breadcrumb'); ?>
        </div>
		<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center underline">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
	<div class="row">
		<div id="confirm-gadget" class="small-12 columns text-center"></div>
	</div>
	<?php if (get_post()->post_content !== '') : ?>
	<div class="row">
		<div class="small-12 large-10 large-offset-1 columns">
			<?php
			while (have_posts()) : the_post();
				the_content();
			endwhile;
			?>
		</div>
	</div>
	<?php endif; ?>
</section>

<?php get_footer()?>