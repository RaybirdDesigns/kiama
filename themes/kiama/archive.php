<?php

$post_type = get_post_type();
$post_type_obj = get_post_type_object($post_type);
$post_type_name = $post_type_obj->name;
$post_type_label = $post_type_obj->label;

$rewrite_slug = $post_type_obj->rewrite['slug'];

$fallback_image_id = get_field('fallback_image', 'options');

$tax_param = isset($_GET['tax']) ? $_GET['tax'] : null;

$terms = get_terms([
    'taxonomy' => $post_type_name.'_type',
    'hide' => false,
]);

switch ($post_type) {
    case 'accomm':
        $heading = 'Places to Stay';
        break;
    case 'attraction':
        $heading = 'Things to Do';
        break;
    case 'event':
        $heading = 'Events';
        break;
    case 'restaurant':
        $heading = 'Places to Eat & Drink';
        break;
    case 'genservice':
        $heading = 'General Services';
        break;
    case 'hire':
        $heading = 'Hire Services';
        break;
    case 'tour':
        $heading = 'Tours';
        break;
    case 'transport':
        $heading = 'Transport Services';
        break;
    case 'info':
        $heading = 'Information Services';
        break;
    case 'destinfo':
        $heading = 'Destinations';
        break;
    default:
        $heading = 'All Articles';
}



if ($tax_param) {
    $term_obj = get_term_by('slug', $_GET['tax'], $post_type_name.'_type');
    $term_name = $term_obj->name;

    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    $list_args = array(
        'posts_per_page'         => 8,
        'post_type'              => array($post_type, 'post'),
        'category_name'          => null,
        'post_status'            => 'publish',
        'update_post_term_cache' => false,
        'paged'                  => $paged,
        'tax_query'              => array(
            array (
                'taxonomy' => $post_type_name.'_type',
                'field' => 'slug',
                'terms' => $_GET['tax'],
            )
        )
    );

    $wp_query = new WP_Query($list_args);
    // var_dump($wp_query);
}

// if ($post_type === 'event') {
//     $event_args = array(
//         array(
//             'date_clause' => array(
//                 'key'     => 'start_time',
//                 'value' => '',
//                 'compare' => '!=',
//             ),
//         ),
//         'orderby' => array(
//             'date_clause'     => 'ASC',
//         ),
//     );
//     $list_args['meta_query'] = $event_args;
// };

?>

<?php get_header(); ?>

<?php get_template_part('components/hero-banners/global-hero-short'); ?>

<section class="archive standard-list__heading grey-bg collapse-top collapse-bottom">
    <div class="row">
        <div class="small-12 columns breadcrumb--dark">
            <?php get_template_part('components/breadcrumb/breadcrumb'); ?>
        </div>
        <div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center underline">
            <?php if ($tax_param): ?>
                <h1>
                    <svg class="archive__icon" role="presentation">
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/icons.svg#<?php echo $tax_param; ?>"></use>
                    </svg>
                    <?php echo $term_name; ?>
                </h1>
            <?php else: ?>
                <h1><?php echo $heading; ?></h1>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
    <?php if ($post_type !== 'destinfo'): ?>
        <div class="archive__nav <?php echo $post_type; ?>">
            <ul class="archive__nav--list">
                <?php foreach($terms as $term): ?>
                    <?php $current = $term->slug === $tax_param ? 'current' : ''; ?>
                    <li class="archive__nav--list---item <?php echo $current; ?>">
                        <a href="<?php echo site_url().'/'.$rewrite_slug.'/?tax='.$term->slug; ?>"><?php echo $term->name; ?></a>
                    </li>
                <?php endforeach; ?>
                <li class="archive__nav--list---item view-all"><a href="<?php echo site_url().'/'.$rewrite_slug; ?>">View All</a></li>
            </ul>
        </div>
    <?php else:  ?>
        <div class="bm"></div>
    <?php endif; ?>
    </div>
</section>

<?php if (have_posts()) : ?>
<section class="standard-list">
    <ul class="row list-reset">
    <?php if ($tax_param): ?>
        <?php while ( $wp_query->have_posts() ) :
            $wp_query->the_post(); ?>
            <?php include(locate_template( 'components/list-item/list-item.php')) ?>
        <?php endwhile; ?>
    <?php else: ?>
        <?php while (have_posts()) : the_post(); ?>
            <?php include(locate_template( 'components/list-item/list-item.php')) ?>
        <?php endwhile; ?>
    <?php endif; ?>
    </ul>
    <div class="row">
        <div class="small-12 columns">
            <?php include(locate_template( 'components/pagination/pagination.php')) ?>
        </div>
    </div>
</section>
<?php endif; ?>

<?php get_footer(); ?>