<?php
/**
 * Template Name: All Sections
 */
$global_index = -1;
$colour_scheme = get_field('colour_scheme');

$add_google_maps_api = false;

$has_footer_carousel = get_field('footer_carousel');

// Check if full hero banners are used
$has_header = false;
if (is_page_template('all-sections.php') && have_rows('all_sections')) {
    $sections = get_field('all_sections');
    foreach($sections as $section) {
        if (in_array('hero_banner_standard', $section) || in_array('hero_experience_banner', $section)) {
            $has_header = true;
        }
    }
}
?>

<?php get_header()?>

<?php if (!$has_header): ?>
    <?php get_template_part('components/hero-banners/global-hero-short'); ?>
<?php endif; ?>
<div class="all-sections theme-<?php echo $colour_scheme; ?>">
    <?php if( have_rows('all_sections') ): ?>

        <?php while ( have_rows('all_sections') ) : the_row(); ?>

        	<?php if( get_row_layout() == 'hero_banner_standard' ): ?>
        		
            	<?php get_template_part('components/hero-banners/hero-standard'); ?>

            <?php elseif( get_row_layout() == 'hero_experience_banner' ): ?>
                
                <?php get_template_part('components/hero-banners/hero-experience'); ?>

            <?php elseif( get_row_layout() == 'text_single_column' ): ?>
                
                <?php get_template_part('components/text/text-single-col'); ?>

            <?php elseif( get_row_layout() == 'in_page_navigation' ): ?>
                
                <?php get_template_part('components/in-page-nav/in-page-nav'); ?>

            <?php elseif( get_row_layout() == 'tile_links_section' ): ?>
                
                <?php get_template_part('components/tile-links-section/tile-links-section'); ?>

            <?php elseif( get_row_layout() == 'link_list' ): ?>
                
                <?php get_template_part('components/link-list/link-list'); ?>

            <?php elseif( get_row_layout() == 'banner_advert' ): ?>
                
                <?php get_template_part('components/banner-advert/banner-advert'); ?>

            <?php elseif( get_row_layout() == 'accommodation_section' ): ?>

                <?php get_template_part('components/section-accomm/section-accomm'); ?>

            <?php elseif( get_row_layout() == 'food_section' ): ?>

                <?php get_template_part('components/food-section/food-section'); ?>

            <?php elseif( get_row_layout() == 'events_section' ): ?>

                <?php get_template_part('components/events-section/events-section'); ?>

            <?php elseif( get_row_layout() == 'video_section' ): ?>

                <?php get_template_part('components/video-section/video-section'); ?>

            <?php elseif( get_row_layout() == 'social_section' ): ?>

                <?php get_template_part('components/social-section/social-section'); ?>

            <?php elseif( get_row_layout() == 'single_image' ): ?>

                <?php get_template_part('components/single-image/single-image'); ?>

            <?php elseif( get_row_layout() == 'alternating_rows' ): ?>

                <?php get_template_part('components/alternating-rows/alternating-rows'); ?>

            <?php elseif( get_row_layout() == 'section_attractions' ): ?>

                <?php get_template_part('components/section-attractions/section-attractions'); ?>

            <?php elseif( get_row_layout() == 'section_form' ): ?>

                <?php get_template_part('components/section-form/section-form'); ?>

            <?php elseif( get_row_layout() == 'section_newsletter_signup' ): ?>

                <?php get_template_part('components/newsletter-signup/newsletter-signup'); ?>

            <?php elseif( get_row_layout() == 'google_map_section' ): ?>

                <?php get_template_part('components/google-map/google-map'); ?>
                <?php
                $add_google_maps_api = true;
                google_map_section_script();
                ?>

            <?php elseif( get_row_layout() == 'itinerary_section' ): ?>
                
                <?php include('components/itinerary/itinerary.php'); ?>

                <?php
                $add_google_maps_api = true;
                itinerary_script();
                ?>

        	<?php elseif( get_row_layout() == 'map_section' ): ?>
        		
            	<?php get_template_part('components/map/map'); ?>
            	<?php map_script(); ?>

            <?php elseif( get_row_layout() == 'bookeasy_region_gadget' ): ?>

                <?php get_template_part('components/bookeasy/region-gadget'); ?>

        	<?php elseif( get_row_layout() == 'listing_grid' ): ?>

                <?php get_template_part('components/listing-grid/listing-grid'); ?>

        	<?php elseif( get_row_layout() == 'accordion_group_section' ): ?>

                <?php get_template_part('components/accordion-group/accordion-group'); ?>

            <?php endif; ?>

        <?php endwhile; ?>

    <?php endif; ?>
</div>

<?php if ($has_footer_carousel): ?>
<?php get_template_part('components/footer-carousel/footer-carousel'); ?>
<?php endif; ?>

<?php
if ($add_google_maps_api) {
    google_maps_api_script();
}
?>

<?php get_footer()?>
