<?php

global $wp_query;
$total_results = $wp_query->found_posts;
$search_string = isset($_GET['s']) && $_GET['s'] !== '' ? 'for <strong>"'.$_GET['s'].'"</strong>' : null;
$has_pt_query = isset($_GET['pt']);
$search_categories = $has_pt_query ? $_GET['pt'] : null;

if ($has_pt_query) { // Get searched category names and make string
    $searched_post_types = array();
    foreach($search_categories as $post_type) {
        $searched_post_types[] = get_post_type_object($post_type)->label;
    }
    $searched_post_types_str = implode(', ',$searched_post_types);
}

$fallback_image_id = get_field('fallback_image', 'options');
?>

<?php get_header(); ?>

<?php get_template_part('components/hero-banners/global-hero-short'); ?>

<section class="search-header">
    <div class="row search-header__heading">
        <div class="small-12 columns text-center">
            <h1>Search Results</h1>
            <p><?php echo $total_results; ?> results found <?php echo $search_string; ?></p>
            <?php if ($has_pt_query): ?>
            <p>Categories Searched:<strong> <?php echo $searched_post_types_str; ?></strong></p>
            <?php endif; ?>
        </div>
    </div>
    <div class="row search-header__form">
        <div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns">
            <?php get_template_part( '/searchform' ); ?>
        </div>
    </div>
</section>

<?php if (have_posts()) : ?>
<section class="standard-list">
    <ul class="row list-reset">
    <?php while (have_posts()) : the_post(); ?>
        <?php include(locate_template( 'components/list-item/list-item.php')) ?>
    <?php endwhile; ?>
    </ul>
    <div class="row">
        <div class="small-12 columns">
            <?php include(locate_template( 'components/pagination/pagination.php')) ?>
        </div>
    </div>
</section>
<?php else : ?>
<section class="standard-list no-results">
    <div class="row">
        <div class="small-12 columns text-center">
            <p>Sorry we couldn't find any results <?php echo $search_string; ?>.</p>
            <a class="btn btn-medium btn-primary btn-arrow" href="<?php echo site_url(); ?>">Back to home</a>
        </div>
    </div>
</section>
                
<?php endif; ?>


<?php get_footer(); ?>