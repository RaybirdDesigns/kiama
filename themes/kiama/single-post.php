<?php
/*
Single Post Template: Article
Description: Template for an article
*/

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/hero-blog'); ?>

<?php if (have_posts()) : ?>
	<section class="blog collapse-top collapse-bottom">
		<div class="row">
			<div class="blog__container small-12 columns breadcrumb--dark">
				<?php get_template_part('components/breadcrumb/breadcrumb'); ?>
				<article class="blog__article small-12 medium-10 medium-offset-1 large-8 large-offset-2 columns">
					<div class="blog__top">
						<h1><?php the_title(); ?></h1>
						<?php the_excerpt(); ?>
                        <?php
                        /*
                        ?>
                        <div style="clear:both;width:100%;float:left;margin-bottom:16px;">
                        <?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>
                        </div>
                        <?php
                        */
                        ?>
					</div>
                    
					<?php
					while (have_posts()) : the_post();
						the_content();
					endwhile;
					?>
					<?php if (get_the_author_meta('display_name', $post->post_author)): ?>
					<div class="blog__author">
						<div class="blog__author--pic-wrapper">
							<?php if (get_avatar( get_the_author_meta( 'ID' ), 250 )) : ?>
							<div class="blog__author--pic">
								<?php echo get_avatar( get_the_author_meta( 'ID' ), 250 ); ?>
							</div>
							<?php endif; ?>
							<p><?php echo get_the_author_meta('display_name', $post->post_author); ?></p>
						</div>
						<div class="blog__author--text">
							<h3>About the Author</h3>
							<p><?php echo get_the_author_meta('description', $post->post_author); ?></p>
						</div>
					</div>
					<?php endif; ?>
				</article>
			</div>
		</div>
	</section>

	<?php

	$blog_related_heading = get_field('blog_related_heading', 'options');
	$blog_button_link = get_field('blog_button_link', 'options');
	$blog_button_text = get_field('blog_button_text', 'options');

	$dont_use_region = get_field('dont_use_region');
	$just_show_blog_posts = get_field('just_show_blog_posts');
	$page_category = get_the_category( $post->ID );
	$page_category_slug = !empty($page_category) ? $page_category[0]->slug : null;
	$page_category_name = !empty($page_category) ? $page_category[0]->name : null;
	$post_type = $just_show_blog_posts ? 'post' : array('accomm', 'post', 'event', 'attraction', 'restaurant', 'tour', 'hire', 'transport');

	$blog_args = array(
		'posts_per_page'		 => 3,
		'post_type'				 => $post_type,
		'category_name'          => $dont_use_region ? null : $page_category_slug,
		'post_status'            => 'publish',
		'update_post_term_cache' => false,
		'paged'					 => false,
		'post__not_in'           => array(get_the_ID()),
		'orderby'        		 => 'rand'
	);

	if (!$just_show_blog_posts) {

		$accomm_type = get_the_terms($post->ID, 'accomm_type');
		$accomm_type_terms = array();
		if (is_array($accomm_type)) {
			foreach($accomm_type as $term) {
				$accomm_type_terms[] = $term->term_id;
			}
		}

		$event_type = get_the_terms($post->ID, 'event_type');
		$event_type_terms = array();
		if (is_array($event_type)) {
			foreach($event_type as $term) {
				$event_type_terms[] = $term->term_id;
			}
		}

		$attraction_type = get_the_terms($post->ID, 'attraction_type');
		$attraction_type_terms = array();
		if (is_array($attraction_type)) {
			foreach($attraction_type as $term) {
				$attraction_type_terms[] = $term->term_id;
			}
		}

		$restaurant_type = get_the_terms($post->ID, 'restaurant_type');
		$restaurant_type_terms = array();
		if (is_array($restaurant_type)) {
			foreach($restaurant_type as $term) {
				$restaurant_type_terms[] = $term->term_id;
			}
		}

		$tour_type = get_the_terms($post->ID, 'tour_type');
		$tour_type_terms = array();
		if (is_array($tour_type)) {
			foreach($tour_type as $term) {
				$tour_type_terms[] = $term->term_id;
			}
		}

		$hire_type = get_the_terms($post->ID, 'hire_type');
		$hire_type_terms = array();
		if (is_array($hire_type)) {
			foreach($hire_type as $term) {
				$hire_type_terms[] = $term->term_id;
			}
		}

		$transport_type = get_the_terms($post->ID, 'transport_type');
		$transport_type_terms = array();
		if (is_array($transport_type)) {
			foreach($transport_type as $term) {
				$transport_type_terms[] = $term->term_id;
			}
		}

	    $sub_category_args = array(
	        array(
				'relation' => 'OR',
				array(
					'taxonomy' => 'accomm_type',
		            'field' => 'term_id',
		            'terms' => $accomm_type_terms
				),
				array(
					'taxonomy' => 'event_type',
		            'field' => 'term_id',
		            'terms' => $event_type_terms
				),
				array(
					'taxonomy' => 'attraction_type',
		            'field' => 'term_id',
		            'terms' => $attraction_type_terms
				),
				array(
					'taxonomy' => 'restaurant_type',
		            'field' => 'term_id',
		            'terms' => $restaurant_type_terms
				),
				array(
					'taxonomy' => 'tour_type',
		            'field' => 'term_id',
		            'terms' => $tour_type_terms
				),
				array(
					'taxonomy' => 'hire_type',
		            'field' => 'term_id',
		            'terms' => $hire_type_terms
				),
				array(
					'taxonomy' => 'transport_type',
		            'field' => 'term_id',
		            'terms' => $transport_type_terms
				),
			)
	    );

		$blog_args['tax_query'] = $sub_category_args;
	}

	$blog_query = new WP_Query($blog_args);
	$count = $blog_query->post_count;

	if ($count > 0) :
	?>

	<section class="blog-related">
		<div class="row">
			<div class="small-12 columns text-center underline">
				<?php if ($blog_related_heading): ?>
					<div class="row">
						<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center bm underline">
							<?php echo $blog_related_heading; ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if ($blog_button_text): ?>
					<div class="row">
						<div class="small-12 columns text-center">
							<a class="btn btn-medium btn-tertiary btn-arrow btn-margin-b" href="<?php echo $blog_button_link; ?>"><?php echo $blog_button_text; ?></a>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="row align-center">
			<?php
			while ( $blog_query->have_posts() ) :
			$blog_query->the_post();
			?>
			<div class="blog-article small-12 medium-4 columns">
				<?php get_template_part('components/tile-link/tile-link'); ?>
			</div>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</div>
	</section>
	<?php get_template_part('components/social-section/social-section'); ?>
	<?php get_template_part('components/in-page-nav/in-page-nav'); ?>
	<?php endif; ?>
<?php else: ?>
	<section>
		<div class="row">
			<div style="height: 30vh;" class="small-12 medium-10 medium-offset-1 large-8 large-offset-2 columns text-center">
				<h4 class="bm">This post appears to missing</h4>
				<a class="btn btn-medium btn-primary btn-margin btn-arrow" href="<?php site_url(); ?>">Home</a>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php get_footer()?>