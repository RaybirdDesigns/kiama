<?php

$post_type = get_post_type();
$category = get_post_primary_category($post->ID, $term='category', $return_all_categories=false);

//print_r($post);
$city_name = get_field('product_city_name', $post->ID) ? get_field('product_city_name', $post->ID) : get_sub_field('product_city_name');
$category_name = !empty($category) ? '<span class="meta-pill meta-pill--white">'.$category['primary_category']->name.'</span>' : ( $city_name ? '<span class="meta-pill meta-pill--white">'.$city_name.'</span>' : null);

$start_time = get_field('start_time');
$end_time = get_field('end_time');

$product_rate_from = get_field('product_rate_from');
$product_rate_to = get_field('product_rate_to');

$bookeasy_id = get_field('bookeasy_id');

$featured_listing = get_field('tile_links_section_featured_pages');

if($curRow['tile_link']->ID === $post->ID) {
    $custom_image = $curRow['custom_image'] ?: get_sub_field('custom_image', $post->ID);
    $custom_icon = $curRow['custom_icon'] ?: get_sub_field('custom_icon', $post->ID);
    $custom_excerpt = $curRow['custom_excerpt'] ?: get_sub_field('custom_excerpt', $post->ID);
} else {
    $custom_image = get_field('custom_image') ? get_field('custom_image') : get_sub_field('custom_image');
    $custom_icon = get_field('custom_icon') ? get_field('custom_icon') : get_sub_field('custom_icon');
    $custom_excerpt = get_field('custom_excerpt') ? get_field('custom_excerpt') : get_sub_field('custom_excerpt');

}

$start_time_formatted = $start_time ? date('d M y',strtotime($start_time)) : null;
$end_time_formatted = $start_time !== $end_time ? ' - '.date('d M y',strtotime($end_time)) : null;

$fallback_image_id = get_field('fallback_image', 'options');
?>

<a href="<?php the_permalink(); ?>" class="tile-link">
    <?php $has_hero = false; ?>
    <?php if ($custom_image): ?>
        <?php $has_hero = true; ?>
        Custom Image Used
        <img class="lazyload" data-object-fit="cover" <?php responsive_image($custom_image, 'large', true) ?>>
	<?php elseif (get_post_thumbnail_id($post->ID)): ?>
    Another Image Used
        <?php $has_hero = true; ?>
        <img class="lazyload" data-object-fit="cover" <?php responsive_image_post('large', true) ?>>
    <?php elseif (get_field('product_image_0')): ?>
        <?php $has_hero = true; ?>
        <img class="lazyload" data-object-fit="cover" <?php adtw_image(880, 0, true); ?>>
    <?php else: ?>
        <?php if( have_rows('all_sections') ): ?>
            <?php while ( have_rows('all_sections') ) : the_row(); ?>
                <?php $hero_standard_image = get_sub_field('hero_image') ? get_sub_field('hero_image') : get_field('hero_image'); ?>
                <?php if($hero_standard_image): ?>
                <?php $has_hero = true; ?>
                <img class="lazyload" data-object-fit="cover" <?php responsive_image($hero_standard_image, 'large', true) ?>>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>
    <?php endif  ?>
    <?php if($has_hero === false): ?>
        <img data-object-fit="cover" <?php responsive_image($fallback_image_id, 'small') ?>>
    <?php endif; ?>
    <div class="tile-link__text">
        <?php if ($custom_icon): ?>
        <svg class="tile-link__icon" role="presentation">
            <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/icons.svg#<?php echo $custom_icon; ?>"></use>
        </svg>
        <?php endif; ?>
    	<?php if ($start_time): ?>
        <span class="meta"><?php echo $start_time_formatted; ?><?php echo $end_time_formatted; ?></span>
        <?php endif; ?>
        <?php if ($bookeasy_id): ?>
        <span class="meta book-now">Book Now</span>
        <?php elseif($product_rate_from): ?>
        <span class="meta"> From $<?php echo $product_rate_from; ?></span>
        <?php endif; ?>
    	<h4><?php the_title(); ?></h4>
    	<div class="read-more">
            <?php if ($custom_excerpt): ?>
                <?php echo $custom_excerpt; ?>
            <?php else: ?>
            	<?php if (get_the_excerpt()) : ?>
                    <?php the_excerpt(); ?>
                <?php else: ?>
                    <?php if( have_rows('all_sections') ): ?>
                        <?php while ( have_rows('all_sections') ) : the_row(); ?>
                            <?php $single_col_text = get_sub_field('text_single_col_text') ? get_sub_field('text_single_col_text') : get_field('text_single_col_text'); ?>
                            <?php if($single_col_text): ?>
                                <p><?php echo wp_trim_words(strip_tags(strip_header_tags($single_col_text)), 20); ?></p>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                <?php endif ?>
            <?php endif ?>
            <span>Read More</span>
        </div>
    </div>
	<?php echo $category_name; ?>
</a>
<?php if ($post_type !== 'page'): ?>
<button aria-label="Click to Favourite <?php the_title(); ?>" data-postid="<?php echo $post->ID; ?>" onclick="window.addFavourite(<?php echo $post->ID; ?>)" class="favourite-btn">
	<svg class="icon" role="presentation">
	    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/global-sprite.svg#fav-ico"></use>
	</svg>
</button>
<?php endif ?>