<?php
$accomm_section_heading = get_sub_field('accomm_section_heading') ? get_sub_field('accomm_section_heading') : get_field('accomm_section_heading'); 
$accomm_section_filter_by_region = get_sub_field('accomm_section_filter_by_region') ? get_sub_field('accomm_section_filter_by_region') : get_field('accomm_section_filter_by_region'); 
$accomm_section_filter_by_sub_category = get_sub_field('accomm_section_filter_by_sub_category') ? get_sub_field('accomm_section_filter_by_sub_category') : get_field('accomm_section_filter_by_sub_category'); 
$accomm_section_remove_padding = get_sub_field('accomm_section_remove_padding') ? get_sub_field('accomm_section_remove_padding') : get_field('accomm_section_remove_padding');

// $page_category = get_the_category( $post->ID );
// $page_category_slug = !empty($page_category) ? $page_category[0]->slug : null;

// Get all selected page regions abd create slug array for use in wp_query
$page_category = get_the_category( $post->ID );
if (!empty($page_category)) {
	$categories = array();
	foreach ($page_category as $category) {
		$categories[] = $category->slug;
	}
}
$page_category_slugs = implode(',',$categories);

$post_type = 'accomm';
$custom_tax = 'accomm_type';

switch ($accomm_section_remove_padding) {
    case 'top':
        $padding = 'collapse-top';
        break;
    case 'bottom':
        $padding = 'collapse-bottom';
        break;
    case 'both':
        $padding = 'collapse-top collapse-bottom';
        break;
    default:
        $padding = '';
}

$accomm_section_args = array(
	'posts_per_page'		 => 10,
	'post_type'				 => $post_type,
	'category_name'          => $accomm_section_filter_by_region ? $page_category_slugs : null,
	'post_status'            => 'publish',
	'update_post_term_cache' => false,
	'paged'					 => false,
	'orderby'        		 => 'rand'
);

if (!empty($accomm_section_filter_by_sub_category)) {

    $sub_category_args = array(
        array (
            'taxonomy' => $custom_tax,
            'field' => 'term_id',
            'terms' => $accomm_section_filter_by_sub_category,
        )
    );

	$accomm_section_args['tax_query'] = $sub_category_args;
}

$accomm_section_query = new WP_Query($accomm_section_args);
$count = $accomm_section_query->post_count;

if ($count > 0): ?>
	<section class="accomm-section <?php echo $padding; ?>">
		<?php if ($accomm_section_heading): ?>
		<div class="row">
			<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center underline">
				<?php echo $accomm_section_heading; ?>
			</div>
		</div>
		<?php endif; ?>
		<?php include(locate_template('components/section-nav/section-nav.php', false, false )); ?>
		<div class="row">
			<div class="small-12 columns">
				<?php
				$accomm_index = 0;
				while ( $accomm_section_query->have_posts() ) :
		    	$accomm_section_query->the_post();
		    	$accomm_index++;
				?>
					<?php if($accomm_index === 1): ?>
						<div class="accomm-top">
					<?php elseif($accomm_index === 3): ?>
						<div data-accomm-carousel class="accomm-carousel">
					<?php endif; ?>
						<div class="accomm-article">
							<?php get_template_part('components/tile-link/tile-link'); ?>
						</div>
					<?php if($accomm_index === 2 || $accomm_index === $count): ?>
						</div>
					<?php endif; ?>
				<?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
			</div>
		</div>
	</section>
<?php endif; ?>
