<?php

$post_type_obj = get_post_type_object($post_type);
// $post_type_name = $post_type_obj->label;
$rewrite_slug = $post_type_obj->rewrite['slug'];
// $rewrite_slug_sans_sub_cat = substr($rewrite_slug, 0, strpos($rewrite_slug, '/%'));

?>

<?php if( have_rows('page_links_section_nav') ): ?>
<div class="row">
	<div class="section-nav <?php echo $post_type; ?>">
		<ul class="section-nav__list">
				<?php while ( have_rows('page_links_section_nav') ) : the_row(); ?>
					<?php
						$page = get_sub_field('page');
						$custom_link = get_sub_field('custom_link');
						$custom_link_text = get_sub_field('custom_link_text');
						$link = $custom_link ? $custom_link : get_permalink( $page->ID, false );;
						$link_text = $custom_link ? $custom_link_text : $page->post_title;
					?>
				    <li class="section-nav__list--item"><a href="<?php echo $link; ?>"><?php echo $link_text; ?></a></li>
			
				<?php endwhile; ?>
			<li class="section-nav__list--item view-all"><a href="<?php echo site_url().'/'.$rewrite_slug; ?>">View All</a></li>
		</ul>
	</div>
</div>
<?php endif; ?>