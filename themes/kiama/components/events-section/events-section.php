<?php
$events_section_heading = get_sub_field('events_section_heading') ? get_sub_field('events_section_heading') : get_field('events_section_heading'); 
$events_section_filter_by_region = get_sub_field('events_section_filter_by_region') ? get_sub_field('events_section_filter_by_region') : get_field('events_section_filter_by_region'); 
$events_section_filter_by_sub_category = get_sub_field('events_section_filter_by_sub_category') ? get_sub_field('events_section_filter_by_sub_category') : get_field('events_section_filter_by_sub_category'); 
$events_section_remove_padding = get_sub_field('events_section_remove_padding') ? get_sub_field('events_section_remove_padding') : get_field('events_section_remove_padding');
$show_reoccurring_events = get_sub_field('show_reoccurring_events') ? get_sub_field('show_reoccurring_events') : get_field('show_reoccurring_events');
$which_events_to_show = get_sub_field('which_events_to_show') ? get_sub_field('which_events_to_show') : get_field('which_events_to_show');
$number_of_events_to_show = get_sub_field('number_of_events_to_show') ? get_sub_field('number_of_events_to_show') : get_field('number_of_events_to_show');

$events_section_button_text = get_sub_field('events_section_button_text') ? get_sub_field('events_section_button_text') : get_field('events_section_button_text'); 
$events_section_button_link = get_sub_field('events_section_button_link') ? get_sub_field('events_section_button_link') : get_field('events_section_button_link'); 

$events_section_featured_events = get_sub_field('events_section_featured_events') ? get_sub_field('events_section_featured_events') : get_field('events_section_featured_events');
//$events_section_featured_events = [];

// Get all selected page regions abd create slug array for use in wp_query
$page_category = get_the_category( $post->ID );
if (!empty($page_category)) {
	$categories = array();
	foreach ($page_category as $category) {
		$categories[] = $category->slug;
	}
}
$page_category_slugs = implode(',',$categories);

$post_type = 'event';
$custom_tax = 'event_type';

$post_type_obj = get_post_type_object($post_type);
$post_type_name = $post_type_obj->label;
$rewrite_slug = $post_type_obj->rewrite['slug'];

switch ($events_section_remove_padding) {
    case 'top':
        $padding = 'collapse-top';
        break;
    case 'bottom':
        $padding = 'collapse-bottom';
        break;
    case 'both':
        $padding = 'collapse-top collapse-bottom';
        break;
    default:
        $padding = '';
}
// Can handle selected events from acf post selector and events from wp_query
// if both types exist they will be merged.
$events_section_args = array(
	'posts_per_page'		 => (int)$number_of_events_to_show,
	'post_type'				 => 'event',
	'category_name'          => $events_section_filter_by_region ? $page_category_slugs : null,
	'post_status'            => 'publish',
	'update_post_term_cache' => false,
	'paged'					 => false,
    'orderby' => array(
        'date_clause'     => 'ASC',
    ),
);

if (empty($which_events_to_show)) {
// Conditionally pull events only reoccurring events or events in the future
    if ($show_reoccurring_events) {

        $meta_query_args = array(
            array(
                'key' => 'reoccurring_event',
                'value' => '1',
                'compare' => '=',
            ),
        );
        $events_section_args['meta_query'] = $meta_query_args;
    } else {
        $current_month = date('m');
        $current_year = date('Y');

        $meta_query_args = array(
            'date_clause' => array(
                'key' => 'start_time',
                'value' => '',
                'compare' => '!=',
            ),
            'start' => array(
                'key' => 'start_time',
                'value' => date($current_year . '-' . $current_month . '-01'),
                'type' => 'DATE',
                'compare' => '>='
            ),
        );
        $events_section_args['meta_query'] = $meta_query_args;
    }
} else {
    if ($which_events_to_show == 'Show Recurring') {
        $meta_query_args = array(
            array(
                'key' => 'reoccurring_event',
                'value' => '1',
                'compare' => '=',
            ),
        );
        $events_section_args['meta_query'] = $meta_query_args;
    } else if ($which_events_to_show == 'One Off') {
        $current_month = date('m');
        $current_year = date('Y');

        $meta_query_args = array(
            'date_clause' => array(
                'key' => 'start_time',
                'value' => '',
                'compare' => '!=',
            ),
            'start' => array(
                'key' => 'start_time',
                'value' => date($current_year . '-' . $current_month . '-01'),
                'type' => 'DATE',
                'compare' => '>='
            ),
        );
        $events_section_args['meta_query'] = $meta_query_args;
    }
}

// Conditionally add sub category query
if (!empty($events_section_filter_by_sub_category)) {
    $sub_category_args = array(
        array (
            'taxonomy' => $custom_tax,
            'field' => 'term_id',
            'terms' => $events_section_filter_by_sub_category,
        )
    );
	$events_section_args['tax_query'] = $sub_category_args;
}
$events_section_query = new WP_Query($events_section_args);

// Process featured events and merge with dynamic events
if (is_array($events_section_featured_events)) {
	$clean_selected_events_obj = array();
	foreach($events_section_featured_events as $item) {
		if ($item['event']->post_status === 'publish') {
			$clean_selected_events_obj[] = $item['event'];
		}
	}
	// Precaution against expired events being used in featured events
	if (count($clean_selected_events_obj) > 0) {
		$events_posts = array_unique(array_merge($clean_selected_events_obj, $events_section_query->posts), SORT_REGULAR);
	} else {
		$events_posts = $events_section_query->posts;
	}
} else {
	$events_posts = $events_section_query->posts;
}

$events_posts = array_slice($events_posts, 0, 6);

$count = count($events_posts);
$alignment = $count <= 2 ? 'align-center' : '';
if ($count > 0) :?>
	<section class="events-section <?php echo $padding; ?>">
		<?php if ($events_section_heading): ?>
		<div class="row">
			<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center underline">
				<?php echo $events_section_heading; ?>
			</div>
		</div>
		<?php endif; ?>
		<?php if ($events_section_button_link): ?>
		<div class="row">
			<div class="small-12 columns text-center">
				<a class="btn btn-medium btn-primary btn-arrow-black btn-margin-b" href="<?php echo $events_section_button_link ?>"><?php echo $events_section_button_text; ?></a>
			</div>
		</div>
		<?php else: ?>
		<div class="row">
			<div class="small-12 columns text-center">
				<a class="btn btn-medium btn-primary btn-arrow-black btn-margin-b" href="<?php echo site_url().'/'.$rewrite_slug; ?>">See all Events</a>
			</div>
		</div>
		<?php endif; ?>
		<?php include(locate_template('components/section-nav/section-nav.php', false, false )); ?>
		<div class="row <?php echo $alignment; ?> collapse">
			<?php
			foreach( $events_posts as $post ):
			setup_postdata( $post ); ?>
			<div class="events-article small-12 medium-6 large-4 columns">
				<?php get_template_part('components/events-tile-link/events-tile-link'); ?>
			</div>
			<?php endforeach; ?>
		</div>
	</section>
<?php wp_reset_postdata(); ?>
<?php endif; ?>
