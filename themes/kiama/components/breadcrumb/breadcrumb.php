<?php

$post_type = get_post_type();
$post_type_obj = get_post_type_object($post_type);
$post_type_label = $post_type_obj->label;
$post_type_name = $post_type_obj->name;
$rewrite_slug = $post_type_obj->rewrite['slug'];
// $rewrite_slug_sans_sub_cat = substr($rewrite_slug, 0, strpos($rewrite_slug, '/%'));

// Get the parent of a page
$ancestors = get_post_ancestors( $post );
$ancestor_count = count($ancestors);

if (is_archive() && isset($_GET['tax'])) {
	$term_id = get_queried_object()->term_id;
	$term_obj = get_term_by('slug', $_GET['tax'], $post_type_name.'_type');
	$term_name = $term_obj->name;
}

$terms = get_post_primary_category($post->ID, $term=$post_type.'_type', $return_all_categories=false);

if (is_single() && $post_type !== 'post' && !empty($terms)) {
	$term_name = $terms['primary_category']->name;
	$term_slug = $terms['primary_category']->slug;
}

?>
<div class="breadcrumb">

	<ul class="list-reset list-inline breadcrumb__list">
		<li><a href="<?php echo site_url(); ?>">Home</a></li>

		<?php if (is_archive() && !is_tax()): ?>
			<?php // Display only on archive list ?>
			<li><a href="<?php echo site_url().'/listings'; ?>">listings</a></li>
			<li><?php echo $post_type_label; ?></li>
		<?php endif; ?>

		<?php if (is_single() && $post_type !== 'post'): ?>
			<?php // Display only listing posts ?>
			<?php if($post_type === 'accomm'): ?>
				<li><a href="<?php echo site_url().'/stay'; ?>"><?php echo $post_type_label; ?></a></li>
			<?php else: ?>
				<li><a href="<?php echo site_url().'/'.$rewrite_slug; ?>"><?php echo $post_type_label; ?></a></li>
			<?php endif; ?>
			<?php if(!empty($terms)): ?>
				<li><a href="<?php echo site_url().'/'.$rewrite_slug.'?tax='.$term_slug; ?>"><?php echo $term_name; ?></a></li>
			<?php endif; ?>
		<?php endif; ?>
	
		<?php if (is_archive() && isset($_GET['tax'])): ?>
			<?php // Display only on taxonomy list ?>
			<li><a href="<?php echo site_url().'/listings'; ?>">listings</a></li>
			<li><a href="<?php echo site_url().'/'.$rewrite_slug; ?>"><?php echo $post_type_label; ?></a></li>
			<li><?php echo $term_name; ?></li>
		<?php endif; ?>

		<?php if (is_single() && $post_type === 'post'): ?>
			<?php // Display only on blog posts ?>
			<li><a href="<?php echo site_url().'/articles/'; ?>">Articles</a></li>
		<?php endif; ?>

		<?php if ($ancestor_count >= 3) : ?>
			<li><a href="<?php echo get_the_permalink( $ancestors[2], false ); ?>"><?php echo get_the_title( $ancestors[2] ); ?></a></li>
		<?php endif; ?>

		<?php if ($ancestor_count >= 2) : ?>
			<li><a href="<?php echo get_the_permalink( $ancestors[1], false ); ?>"><?php echo get_the_title( $ancestors[1] ); ?></a></li>
		<?php endif; ?>

		<?php if ( $post->post_parent ):  ?>
		<li>
		    <a href="<?php echo get_permalink( $post->post_parent ); ?>" >
		    	<?php echo get_the_title( $post->post_parent ); ?>
		    </a>
	    </li>
		<?php endif; ?>

		<?php if (!is_search() && !is_archive()) : ?>
			<li class="current-page"><?php the_title(); ?></li>
		<?php endif; ?>

		<?php if (is_search()) : ?>
			<li class="current-page">search results</li>
		<?php endif; ?>
	</ul>
</div>