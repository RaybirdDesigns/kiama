<?php
$newsletter_signup_text = get_sub_field('newsletter_signup_text') ? get_sub_field('newsletter_signup_text') : get_field('newsletter_signup_text'); 
$newsletter_signup_heading = get_sub_field('newsletter_signup_heading') ? get_sub_field('newsletter_signup_heading') : get_field('newsletter_signup_heading'); 
$newsletter_signup_form = get_sub_field('newsletter_signup_form') ? get_sub_field('newsletter_signup_form') : get_field('newsletter_signup_form'); 
$newsletter_signup_background_colour = get_sub_field('newsletter_signup_background_colour') ? get_sub_field('newsletter_signup_background_colour') : get_field('newsletter_signup_background_colour'); 

switch ($newsletter_signup_background_colour) {
    case 'blue':
        $bg_colour = 'blue';
        break;
    case 'orange':
        $bg_colour = 'orange';
        break;
    case 'green':
        $bg_colour = 'green';
        break;
    case 'peach':
        $bg_colour = 'peach';
        break;
    default:
        $bg_colour = '';
}
?>

<section class="newsletter-signup <?php echo $bg_colour; ?>">
	<div class="row">
		<div class="small-12 large-6 columns newsletter-signup__text">
			<?php if ($newsletter_signup_heading): ?>
				<h3><?php echo $newsletter_signup_heading; ?></h3>
			<?php endif; ?>
			<?php echo $newsletter_signup_text; ?>
		</div>
		<div class="small-12 large-6 columns newsletter-signup__form">
			<?php
				if( class_exists( 'Ninja_Forms' ) && is_array( $newsletter_signup_form ) ){ // Ninja forms 3.x
				    Ninja_Forms()->display( $newsletter_signup_form['id'] );
				} elseif( class_exists( 'Ninja_Forms' ) && is_object( $newsletter_signup_form ) ){ // Ninja forms 3.x
				    foreach ($newsletter_signup_form as $form) {
				      Ninja_Forms()->display( $form['id'] );
				    }
				}
			?>
		</div>
	</div>
</section>