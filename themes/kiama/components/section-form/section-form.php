<?php

$forms_section_heading_copy = get_sub_field('forms_section_heading_copy') ? get_sub_field('forms_section_heading_copy') : get_field('forms_section_heading_copy'); 
$form_selector = get_sub_field('form_selector') ? get_sub_field('form_selector') : get_field('form_selector'); 
$forms_section_remove_padding = get_sub_field('forms_section_remove_padding') ? get_sub_field('forms_section_remove_padding') : get_field('forms_section_remove_padding'); 
$forms_width = get_sub_field('forms_width') ? get_sub_field('forms_width') : get_field('forms_width');
$forms_bg = get_sub_field('forms_bg') ? get_sub_field('forms_bg') : get_field('forms_bg');

switch ($forms_section_remove_padding) {
    case 'top':
        $padding = 'collapse-top';
        break;
    case 'bottom':
        $padding = 'collapse-bottom';
        break;
    case 'both':
        $padding = 'collapse-top collapse-bottom';
        break;
    default:
        $padding = '';
}

switch ($forms_width) {
	case 'tiny':
        $width = 'medium-10 medium-offset-1 large-6 large-offset-3';
        break;
    case 'narrow':
        $width = 'medium-10 medium-offset-1 large-8 large-offset-2';
        break;
    case 'medium':
        $width = 'medium-8  medium-offset-2 large-10 large-offset-1';
        break;
    case 'wide':
        $width = 'large-12';
        break;
    case 'two':
        $width = 'large-6';
        break;
    default:
        $width = 'large-12';
}

$text_column = $forms_width === 'two' ? 'large-6' : 'medium-10 large-8 medium-offset-1 large-offset-2 bm';

?>

<section class="accomm-section <?php echo $padding; ?> bg--<?php echo $forms_bg; ?>">
	<div class="row">
	<?php if ($forms_section_heading_copy): ?>
		<div class="small-12 <?php echo $text_column; ?> columns">
			<?php echo $forms_section_heading_copy; ?>
		</div>
	<?php endif; ?>
		<div class="small-12 <?php echo $width; ?> columns">
			<?php
				if( class_exists( 'Ninja_Forms' ) && is_array( $form_selector ) ){ // Ninja forms 3.x
				    Ninja_Forms()->display( $form_selector['id'] );
				} elseif( class_exists( 'Ninja_Forms' ) && is_object( $form_selector ) ){ // Ninja forms 3.x
				    foreach ($form_selector as $form) {
				      Ninja_Forms()->display( $form['id'] );
				    }
				}
			?>
		</div>
	</div>
</section>