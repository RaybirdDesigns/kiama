<?php

$social_section_heading = get_sub_field('social_section_heading') ? get_sub_field('social_section_heading') : get_field('social_section_heading'); 
$social_section_crowdriff = get_sub_field('social_section_crowdriff') ? get_sub_field('social_section_crowdriff') : get_field('social_section_crowdriff');
$social_section_button_text = get_sub_field('social_section_button_text') ? get_sub_field('social_section_button_text') : get_field('social_section_button_text'); 
$social_section_button_link = get_sub_field('social_section_button_link') ? get_sub_field('social_section_button_link') : get_field('social_section_button_link'); 
$social_section_remove_padding = get_sub_field('social_section_remove_padding') ? get_sub_field('social_section_remove_padding') : get_field('social_section_remove_padding');
$social_width = get_sub_field('social_width') ? get_sub_field('social_width') : get_field('social_width');
$social_use_in_body = get_sub_field('social_use_in_body') ? get_sub_field('social_use_in_body') : get_field('social_use_in_body');
$social_section_background_colour = get_sub_field('social_section_background_colour') ? get_sub_field('social_section_background_colour') : get_field('social_section_background_colour');


if ($social_section_crowdriff && !$social_use_in_body):

switch ($social_section_background_colour) {
    case 'dark':
        $bg_theme = 'dark';
        break;
    case 'white':
        $bg_theme = 'white';
        break;
    case 'default':
        $bg_theme = '';
        break;
    default:
        $bg_theme = '';
}

switch ($social_width) {
    case 'narrow':
        $width = 'medium-10 large-8 medium-offset-1 large-offset-2';
        break;
    case 'medium':
        $width = 'medium-8 large-10  medium-offset-2 large-offset-1';
        break;
    case 'wide':
        $width = 'large-12';
        break;
    case 'full':
        $width = '';
        break;
    default:
        $width = 'large-12';
}

$expanded = $social_width === 'full' ? 'expanded' : '';


switch ($social_section_remove_padding) {
    case 'top':
        $padding = 'collapse-top';
        break;
    case 'bottom':
        $padding = 'collapse-bottom';
        break;
    case 'both':
        $padding = 'collapse-top collapse-bottom';
        break;
    default:
        $padding = '';
}
?>
<section class="social <?php echo $padding; ?> <?php echo $bg_theme; ?>">
	<?php if ($social_section_heading): ?>
	<div class="row">
		<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center underline">
			<?php echo $social_section_heading; ?>
		</div>
	</div>
	<?php endif; ?>
	<?php if ($social_section_button_link): ?>
	<div class="row">
		<div class="small-12 columns text-center">
			<a class="btn btn-medium btn-primary btn-arrow btn-margin-b" href="<?php echo $social_section_button_link ?>"><?php echo $social_section_button_text ?></a>
		</div>
	</div>
	<?php endif; ?>
	<div class="row <?php echo $expanded; ?>">
		<div class="small-12 <?php echo $width; ?> columns">
			<?php echo $social_section_crowdriff; ?>
		</div>
	</div>
</section>
<?php endif; ?>