<?php
$image_id = get_post_thumbnail_id($post->ID);
$image_src = wp_get_attachment_image_url($image_id, 'large');
$content = get_post_field('post_content', $post->ID);
$word_count = str_word_count(strip_tags($content));
$articleBody = preg_replace( "/\r|\n/", "", strip_tags($content));
?>

<script type="application/ld+json">
{ "@context": "https://schema.org", 
 "@type": "BlogPosting",
 "headline": "<?php the_title(); ?>",
 "image": "<?php echo $image_src; ?>",
 "genre": "",
 "keywords": "", 
 "wordcount": "<?php echo $word_count; ?>",
 "publisher": "Destination Kiama",
 "url": "<?php the_permalink(); ?>",
 "isFamilyFriendly": "true",
 "datePublished": "<?php echo get_the_date(); ?>",
 "description": "<?php echo get_the_excerpt(); ?>",
 "articleBody": "<?php echo $articleBody; ?>",
   "author": {
    "@type": "Person",
    "name": "<?php echo get_the_author_meta('display_name', $post->post_author); ?>"
  }
 }
</script>