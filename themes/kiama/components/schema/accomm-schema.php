<?php
$image_id = get_post_thumbnail_id($post->ID);
$image_src = wp_get_attachment_image_url($image_id, 'large');
$image_0 = get_field('product_image_0');
$image = $image_0 ? $image_0 :  $image_src;
$content = get_post_field('post_content', $post->ID);
$articleBody = preg_replace( "/\r|\n/", "", strip_tags($content));

$product_address_1 = get_field('product_address_1');
$product_address_2 = get_field('product_address_2');
$product_state_name = get_field('product_state_name');
$product_post_code = get_field('product_post_code');

$product_email = get_field('product_email');
$product_primary_phone = get_field('product_primary_phone');
?>

<script type="application/ld+json">
{
 "@context": "http://schema.org",
    "@type": ["Accommodation", "Product"],
    "additionalType": "Product",
    "name": "<?php the_title(); ?>",
    "image": "<?php echo $image; ?>",
    "address" : {
      "@type": "PostalAddress",
      "addressLocality": "<?php echo $product_address_2; ?>", 
      "addressRegion": "<?php echo $product_state_name; ?>", 
      "postalCode": "<?php echo $product_post_code; ?>", 
      "streetAddress": "<?php echo $product_address_1; ?>"
    }, 
    "email":"<?php echo $product_email; ?>",
    "telephone":"<?php echo $product_primary_phone; ?>",
    "description": "<?php echo $articleBody; ?>",
}
</script>