<?php
$image_id = get_post_thumbnail_id($post->ID);
$image_src = wp_get_attachment_image_url($image_id, 'large');
$image_0 = get_field('product_image_0');
$image = $image_0 ? $image_0 :  $image_src;
$content = get_post_field('post_content', $post->ID);
$articleBody = preg_replace( "/\r|\n/", "", strip_tags($content));

$product_address_1 = get_field('product_address_1');
$product_address_2 = get_field('product_address_2');
$product_country_name = get_field('product_country_name');
$product_state_name = get_field('product_state_name');
$product_post_code = get_field('product_post_code');
?>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Product",
  "name": "<?php the_title(); ?>",
  "location": {
    "@type": "Place",
    "address": {
      "@type": "PostalAddress",
      "streetAddress": "<?php echo $product_address_1; ?>",
      "addressLocality": "<?php echo $product_address_2; ?>",
      "postalCode": "<?php echo $product_post_code; ?>",
      "addressRegion": "<?php echo $product_state_name; ?>",
      "addressCountry": "<?php echo $product_country_name; ?>"
    }
  },
  "image": '<?php echo $image; ?>',
  "description": "<?php echo $articleBody; ?>",
}
</script>