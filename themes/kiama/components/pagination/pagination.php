<div class="pagination">
    <?php
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;

    echo paginate_links( array(
        'base'      => @add_query_arg('paged','%#%'),
        'format'    => '?paged=%#%',
        'prev_text' => __('Previous'),
        'next_text' => __('Next'),
        'current'   => $current,
        'total'     => $wp_query->max_num_pages
    ) );

    ?>
</div>