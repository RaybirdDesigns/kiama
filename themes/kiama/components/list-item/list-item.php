<?php
$post_type = get_post_type();
$start_time = get_field('start_time');
$end_time = get_field('end_time');
$start_time_formatted = $start_time ? date('d M Y',strtotime($start_time)) : null;
$end_time_formatted = $start_time !== $end_time ? ' - '.date('d M Y',strtotime($end_time)) : null;

// $category = get_the_category( $post->ID );
$category = get_post_primary_category($post->ID, $term='category', $return_all_categories=false);
$category_name = !empty($category) ? '<span class="category">'.$category['primary_category']->name.'</span>' : null;

$custom_event_dates = get_field('custom_event_dates');
$reoccurring_event = get_field('reoccurring_event');

$post_type_obj = get_post_type_object(get_post_type( $post->ID ));
$post_type_label = $post_type_obj->label;
$post_type_slug = $post_type_obj->name;
$sub_cat = get_post_primary_category($post->ID, $term=$post_type.'_type', $return_all_categories=false);

add_filter('excerpt_length', function($length) {
	return 40;
});
?>

<li class="small-12 columns">
    <article class="standard-list__listing">
        <div class="standard-list__image">
            <a href="<?php the_permalink() ?>">
            <?php $has_hero = false; ?>
            <?php if (get_post_thumbnail_id($post->ID)): ?>
                <?php $has_hero = true; ?>
                <img data-object-fit="cover" <?php responsive_image_post('medium') ?>>
            <?php elseif (get_field('product_image_0')): ?>
                <?php $has_hero = true; ?>
                <img data-object-fit="cover" <?php adtw_image(480, 0); ?>>
            <?php else: ?>
                <?php if( have_rows('all_sections') ): ?>
                    <?php while ( have_rows('all_sections') ) : the_row(); ?>
                        <?php $hero_standard_image = get_sub_field('hero_image') ? get_sub_field('hero_image') : get_field('hero_image'); ?>
                        <?php if($hero_standard_image): ?>
                            <?php $has_hero = true; ?>
                            <img data-object-fit="cover" <?php responsive_image($hero_standard_image, 'small') ?>>                                    
                        <?php endif; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            <?php endif  ?>
            <?php if($has_hero === false): ?>
                <img data-object-fit="cover" <?php responsive_image($fallback_image_id, 'small') ?>>
            <?php endif; ?>
            </a>
        </div>
        <div class="standard-list__body">
            <?php if ($custom_event_dates): ?>
                <div class="standard-list__body--dates-custom">
                    <p><?php echo $custom_event_dates; ?></p>
                </div>
                <?php endif; ?>
            <?php if ($start_time): ?>
            <?php $end_date_rendered = $reoccurring_event ? '' : $end_time_formatted; ?>
            <span class="dates"><?php echo $start_time_formatted; ?><?php echo $end_date_rendered; ?></span>
            <?php endif; ?>
            <h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
            <?php if (get_the_excerpt()) : ?>
                <?php the_excerpt(); ?>
            <?php else: ?>
                <?php if( have_rows('all_sections') ): ?>
                    <?php $found_text = true; ?>

                    <?php while ( have_rows('all_sections') ) :
                        // Just render the first text_single_text
                            the_row();
                         if (get_row(true)['acf_fc_layout'] === 'text_single_column' || get_row(true)['acf_fc_layout'] ===  'hero_experience_banner' && $found_text === true) :
                            $single_col_text = get_sub_field('text_single_col_text') ? get_sub_field('text_single_col_text') : get_field('text_single_col_text');
                            $hero_experience_sub_heading_text = get_sub_field('hero_experience_sub_heading_text') ? get_sub_field('hero_experience_sub_heading_text') : get_field('hero_experience_sub_heading_text');
                            $content = $single_col_text ? $single_col_text : $hero_experience_sub_heading_text;
                            ?>
                            <?php if ($found_text): ?>
                                <p><?php echo wp_trim_words(strip_header_tags(strip_tags($content)), 50); ?></p>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php $found_text = false; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            <?php endif ?>
            <?php echo $category_name; ?>
            <?php if ($post_type_label) : ?>
                <span class="post-type <?php echo $post_type_slug; ?>"><?php echo $post_type_label; ?></span>
            <?php endif; ?>
            <?php if (!empty($sub_cat) && $sub_cat['primary_category']->slug !== 'nothing') : ?>
                <span class="sub-cat"><?php echo $sub_cat['primary_category']->name; ?></span>
            <?php endif; ?>
        </div>
    </article>
</li>