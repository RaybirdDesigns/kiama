<?php

$links_link_list = get_sub_field('links_link_list') ? get_sub_field('links_link_list') : get_field('links_link_list');
$links_link_list_bg = get_sub_field('link_list_grey_bg') ? get_sub_field('link_list_grey_bg') : get_field('link_list_grey_bg');
?>

<section class="link-list link-list--<?php echo $links_link_list_bg; ?>">
	<div class="row">
		<div class="small-12 columns">
			<ul class="link-list__list">
				<?php if( have_rows('links_link_list') ): ?>
					<?php while ( have_rows('links_link_list') ) : the_row(); ?>
						<?php $image_id = get_sub_field('image_id'); ?>
					    <li>
					    	<?php if ($image_id): ?>
					    	<div class="link-list__img">
					    		<img class="lazyload" data-object-fit="cover" <?php responsive_image($image_id, 'medium', true) ?>>
					    	</div>
					    	<?php endif; ?>
					    	<a href="<?php the_sub_field('page_link') ?>"><?php the_sub_field('link_text'); ?></a>
					    </li>
					<?php endwhile; ?>
				<?php endif; ?>
			</ul>
		</div>
	</div>
</section>