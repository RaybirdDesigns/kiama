<?php 

$hero_experience_image = get_sub_field('hero_image') ? get_sub_field('hero_image') : get_field('hero_image');
$hero_experience_icon = get_sub_field('hero_experience_icon') ? get_sub_field('hero_experience_icon') : get_field('hero_experience_icon');

$hero_experience_heading_text = get_sub_field('hero_experience_heading_text') ? get_sub_field('hero_experience_heading_text') : get_field('hero_experience_heading_text');
$hero_experience_sub_heading_text = get_sub_field('hero_experience_sub_heading_text') ? get_sub_field('hero_experience_sub_heading_text') : get_field('hero_experience_sub_heading_text');

$hero_experience_video_id = get_sub_field('hero_experience_video_id') ? get_sub_field('hero_experience_video_id') : get_field('hero_experience_video_id');
$hero_experience_video_button_text = get_sub_field('hero_experience_video_button_text') ? get_sub_field('hero_experience_video_button_text') : get_field('hero_experience_video_button_text');

$hero_experience_link_text = get_sub_field('hero_experience_link_text') ? get_sub_field('hero_experience_link_text') : get_field('hero_experience_link_text');
$hero_experience_link_url_internal = get_sub_field('hero_experience_link_url_internal') ? get_sub_field('hero_experience_link_url_internal') : get_field('hero_experience_link_url_internal');
$hero_experience_link_url_external = get_sub_field('hero_experience_link_url_external') ? get_sub_field('hero_experience_link_url_external') : get_field('hero_experience_link_url_external');

$hero_experience_secondary_link_text = get_sub_field('hero_experience_secondary_link_text') ? get_sub_field('hero_experience_secondary_link_text') : get_field('hero_experience_secondary_link_text');
$hero_experience_link_url = get_sub_field('hero_experience_link_url') ? get_sub_field('hero_experience_link_url') : get_field('hero_experience_link_url');

$hero_experience_parallax = get_sub_field('hero_experience_parallax') ? get_sub_field('hero_experience_parallax') : get_field('hero_experience_parallax');
$hero_experience_overlay = get_sub_field('hero_experience_overlay') ? get_sub_field('hero_experience_overlay') : get_field('hero_experience_overlay');
$hero_experience_breadcrumb = get_sub_field('hide_breadcrumb') ? get_sub_field('hide_breadcrumb') : get_field('hide_breadcrumb');
$hero_experience_size = get_sub_field('hero_experience_size') ? get_sub_field('hero_experience_size') : get_field('hero_experience_size');
$hero_experience_down_show = get_sub_field('hero_experience_down_show') ? get_sub_field('hero_experience_down_show') : get_field('hero_experience_down_show');
$hero_experience_scroll_to_section = get_sub_field('hero_experience_scroll_to_section') ? get_sub_field('hero_experience_scroll_to_section') : get_field('hero_experience_scroll_to_section');

$parallax = $hero_experience_parallax ? 'data-parallax' : '';
$hero_size = $hero_experience_size ? $hero_experience_size : 'large';
$has_sub_heading_text = $hero_experience_sub_heading_text ? 'has-subheading' : '';
?>

<div <?php echo $parallax; ?> class="hero-experience <?php echo $has_sub_heading_text; ?>">
	<div class="hero-experience__wrapper hero-experience__wrapper--<?php echo $hero_size; ?>">
		<img data-object-fit="cover" <?php echo responsive_image($hero_experience_image, 'full'); ?>>
		<div class="hero-experience__content">
			<?php if ($hero_experience_icon): ?>
				<svg class="hero-experience__icon" role="presentation">
		            <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/icons.svg#<?php echo $hero_experience_icon; ?>"></use>
		        </svg>
			<?php endif; ?>
			<?php if (!$hero_experience_breadcrumb): ?>
				<?php get_template_part('components/breadcrumb/breadcrumb'); ?>
			<?php endif ?>
			<?php if ($hero_experience_heading_text): ?>
				<h1><?php echo $hero_experience_heading_text; ?></h1>
			<?php else: ?>
				<h1><?php the_title(); ?></h1>
			<?php endif ?>
			<?php if ($hero_experience_video_id && !$hero_experience_link_url_external && !$hero_experience_link_url_internal): ?>
				<button data-youtube="<?php echo $hero_experience_video_id; ?>" class="btn btn-large btn-primary btn-video"><?php echo $hero_experience_video_button_text; ?></button>
			<?php endif ?>
			<?php if ($hero_experience_link_url_internal && !$hero_experience_link_url_external): ?>
				<a class="btn btn-medium btn-primary btn-arrow-black" target="_blank" href="<?php echo $hero_experience_link_url_internal; ?>"><?php echo $hero_experience_link_text; ?></a>
			<?php endif ?>
			<?php if ($hero_experience_link_url_external): ?>
				<a class="btn btn-medium btn-primary btn-arrow-black" target="_blank" href="<?php echo $hero_experience_link_url_external; ?>"><?php echo $hero_experience_link_text; ?></a>
			<?php endif ?>
		</div>
		<?php if ($hero_experience_overlay): ?>
			<div class="hero-experience__overlay"></div>
		<?php endif ?>
		<?php if ($hero_experience_down_show && !$hero_experience_sub_heading_text): ?>
			<?php get_template_part('components/hero-banners/scroll-down-btn'); ?>
		<?php endif ?>
	</div>
	<?php if ($hero_experience_sub_heading_text): ?>
		<div class="hero-experience__secondary">
			<?php echo $hero_experience_sub_heading_text; ?>
			<?php if ($hero_experience_link_url): ?>
				<a class="btn btn-medium btn-primary btn-arrow-black" href="<?php echo $hero_experience_link_url; ?>"><?php echo $hero_experience_secondary_link_text; ?></a>
			<?php elseif($hero_experience_scroll_to_section): ?>
				<a data-scroll-id class="btn btn-medium btn-primary" href="<?php echo $hero_experience_scroll_to_section; ?>"><?php echo $hero_experience_secondary_link_text; ?></a>
			<?php endif ?>
		</div>
	<?php endif ?>
</div>