<?php

$image_id = get_field('global_header_image', 'options');

?>

<div class="global-hero-short-wrapper breadcrumb--dark">
	<div class="global-hero-short">
		<img <?php responsive_image($image_id, 'full', false) ?>>
	</div>
	<?php if (is_page_template('all-sections.php')): ?>
	<div class="row breadcrumb-row">
	    <div class="small-12 columns">
	        <?php get_template_part('components/breadcrumb/breadcrumb'); ?>
	    </div>
	</div>
	<?php endif; ?>
</div>