<?php 

$hero_standard_image = get_sub_field('hero_image') ? get_sub_field('hero_image') : get_field('hero_image');
$hero_standard_heading_text = get_sub_field('hero_standard_heading_text') ? get_sub_field('hero_standard_heading_text') : get_field('hero_standard_heading_text');
$hero_standard_sub_heading_text = get_sub_field('hero_standard_sub_heading_text') ? get_sub_field('hero_standard_sub_heading_text') : get_field('hero_standard_sub_heading_text');
$hero_standard_video_id = get_sub_field('hero_standard_video_id') ? get_sub_field('hero_standard_video_id') : get_field('hero_standard_video_id');
$hero_standard_video_button_text = get_sub_field('hero_standard_video_button_text') ? get_sub_field('hero_standard_video_button_text') : get_field('hero_standard_video_button_text');
$hero_standard_link_text = get_sub_field('hero_standard_link_text') ? get_sub_field('hero_standard_link_text') : get_field('hero_standard_link_text');
$hero_standard_link_url = get_sub_field('hero_standard_link_url') ? get_sub_field('hero_standard_link_url') : get_field('hero_standard_link_url');
$hero_standard_parallax = get_sub_field('hero_standard_parallax') ? get_sub_field('hero_standard_parallax') : get_field('hero_standard_parallax');
$hero_standard_down_hide = get_sub_field('hero_standard_down_hide') ? get_sub_field('hero_standard_down_hide') : get_field('hero_standard_down_hide');
$hero_standard_overlay = get_sub_field('hero_standard_overlay') ? get_sub_field('hero_standard_overlay') : get_field('hero_standard_overlay');

$parallax = $hero_standard_parallax ? 'data-parallax' : '';
?>

<div <?php echo $parallax; ?> class="hero-standard">
	<img data-object-fit="cover" <?php echo responsive_image($hero_standard_image, 'full'); ?>>
	<div class="hero-standard__content">
		<?php if ($hero_standard_heading_text): ?>
		<h1><?php echo $hero_standard_heading_text; ?></h1>
		<?php endif ?>
		<?php if ($hero_standard_sub_heading_text): ?>
		<p><?php echo $hero_standard_sub_heading_text; ?></p>
		<?php endif ?>
		<?php if ($hero_standard_video_id && !$hero_standard_link_url): ?>
		<button title="Play Video" data-youtube="<?php echo $hero_standard_video_id; ?>" class="btn btn-large btn-primary btn-video"><?php echo $hero_standard_video_button_text; ?></button>
		<?php endif ?>
		<?php if ($hero_standard_link_url): ?>
		<a class="btn btn-medium btn-primary btn-arrow btn-arrow-black" href="<?php echo $hero_standard_link_url; ?>"><?php echo $hero_standard_link_text; ?></a>
		<?php endif ?>
	</div>
	<?php if (!$hero_standard_down_hide): ?>
		<?php get_template_part('components/hero-banners/scroll-down-btn'); ?>
	<?php endif ?>
	<?php if ($hero_standard_overlay): ?>
	<div class="hero-standard__overlay"></div>
	<?php endif ?>
</div>