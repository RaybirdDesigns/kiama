<button title="Scroll to first section" data-scroll-to class="btn-scroll-down">
	<span class="show-for-sr">Scroll to first section</span>
	<svg class="icon" role="presentation">
	    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#down-ico"></use>
	</svg>
</button>