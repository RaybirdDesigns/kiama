<div data-parallax class="hero-blog">
	<div class="hero-blog__wrapper">
		<img data-object-fit="cover" <?php echo responsive_image_post('large-banner', false); ?>>
		<div class="hero-blog__overlay"></div>
	</div>
</div>