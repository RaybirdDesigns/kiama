<?php

$text_single_col_text_width = get_sub_field('text_single_col_text_width') ? get_sub_field('text_single_col_text_width') : get_field('text_single_col_text_width');
$text_single_col_remove_padding = get_sub_field('text_single_col_remove_padding') ? get_sub_field('text_single_col_remove_padding') : get_field('text_single_col_remove_padding');
$text_single_col_bg = get_sub_field('text_single_col_bg') ? get_sub_field('text_single_col_bg') : get_field('text_single_col_bg');

switch ($text_single_col_text_width) {
    case 'narrow':
        $width = 'medium-10 large-8 medium-offset-1 large-offset-2';
        break;
    case 'medium':
        $width = 'medium-8 large-10  medium-offset-2 large-offset-1';
        break;
    case 'wide':
        $width = 'large-12';
        break;
    default:
        $width = 'large-12';
}

switch ($text_single_col_remove_padding) {
    case 'top':
        $padding = 'collapse-top';
        break;
    case 'bottom':
        $padding = 'collapse-bottom';
        break;
    case 'both':
        $padding = 'collapse-top collapse-bottom';
        break;
    default:
        $padding = '';
}

?>

<section id="text-single-colomn" class="text-single-col <?php echo $padding; ?> bg--<?php echo $text_single_col_bg; ?>">
	<div class="row">
		<div class="small-12 <?php echo $width; ?> columns">
			<?php the_sub_field('text_single_col_text'); ?>
		</div>
	</div>
</section>