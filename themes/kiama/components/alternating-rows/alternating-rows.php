<section class="alternating-rows collapse-top collapse-bottom">
	<div class="row expanded alternating-rows__wrapper">
		<?php if ( have_rows('alternating_rows_rows') ): ?>
			<?php while ( have_rows('alternating_rows_rows') ) : the_row(); ?>
				<?php
					$heading = get_sub_field('heading');
					$image_id = get_sub_field('image');
					$body_copy = get_sub_field('body_copy');
					$page_link = get_sub_field('page_link');
				?>
				<div class="alternating-rows__row">
					<div class="alternating-rows__row--image">
						<img data-object-fit="cover" class="lazyload" <?php responsive_image($image_id, 'large-banner', true); ?>>
					</div>
					<div class="alternating-rows__row--text">
						<h3><?php echo $heading; ?></h3>
						<?php echo $body_copy; ?>
						<?php if ($page_link): ?>
							<a class="read-more" href="<?php echo $page_link; ?>">Read More</a>
						<?php endif; ?>
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</section>