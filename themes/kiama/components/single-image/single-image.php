<?php

$single_image_id = get_sub_field('single_image_id') ? get_sub_field('single_image_id') : get_field('single_image_id');
$single_image_external_link_url = get_sub_field('single_image_external_link_url') ? get_sub_field('single_image_external_link_url') : get_field('single_image_external_link_url');
$single_image_internal_link_url = get_sub_field('single_image_internal_link_url') ? get_sub_field('single_image_internal_link_url') : get_field('single_image_internal_link_url');
$single_image_meta = wp_get_attachment($single_image_id);
$caption = $single_image_meta['caption'];
$description = $single_image_meta['description'];

$single_image_bg = get_sub_field('single_image_grey_bg') ? get_sub_field('single_image_grey_bg') : get_field('single_image_grey_bg');
$single_image_width = get_sub_field('single_image_width') ? get_sub_field('single_image_width') : get_field('single_image_width');

$single_image_remove_padding = get_sub_field('single_image_remove_padding') ? get_sub_field('single_image_remove_padding') : get_field('single_image_remove_padding');

switch ($single_image_remove_padding) {
    case 'top':
        $padding = 'collapse-top';
        break;
    case 'bottom':
        $padding = 'collapse-bottom';
        break;
    case 'both':
        $padding = 'collapse-top collapse-bottom';
        break;
    default:
        $padding = '';
}

switch ($single_image_width) {
    case 'narrow':
        $width = 'medium-8 medium-offset-2  large-6 large-offset-3';
        break;
    case 'normal':
        $width = 'medium-10 medium-offset-1 large-8 large-offset-2';
        break;
    case 'wide':
        $width = '';
        break;
    default:
        $width = '';
}

?>

<section class="single-image <?php echo $padding; ?> single-image--<?php echo $single_image_bg; ?>">
	<div class="row">
		<div class="small-12 <?php echo $width; ?> columns">
			<div class="single-image__wrapper">
                <?php if ($description): ?>
    				<span class="single-image__desc"><?php echo $description; ?></span>
                <?php endif; ?>
                <?php if ($single_image_external_link_url): ?>
                    <a target="_blank" href="<?php echo $single_image_external_link_url; ?>">
                <?php elseif ($single_image_internal_link_url): ?>
                    <a href="<?php echo $single_image_internal_link_url; ?>">
                <?php endif; ?>  
				<img class="lazyload" <?php responsive_image($single_image_id, 'large', true); ?>>
                <?php if ($single_image_external_link_url || $single_image_internal_link_url): ?>
                    </a>
                <?php endif; ?>  
			</div>
            <?php if ($caption): ?>
			     <figure><?php echo $caption; ?></figure>
            <?php endif; ?>
		</div>
	</div>
</section>