<?php
$global_index++;

$itinerary_bg = get_sub_field('itinerary_bg') ? get_sub_field('itinerary_bg') : get_field('itinerary_bg');
$itinerary_grade = get_sub_field('itinerary_grade') ? '<li>Grade: <strong>'.get_sub_field('itinerary_grade').'</strong></li>' : null;
$itinerary_distance = get_sub_field('itinerary_distance') ? '<li>Distance: <strong>'.get_sub_field('itinerary_distance').'</strong></li>' : null;
$itinerary_time = get_sub_field('itinerary_time') ? '<li>Time: <strong>'.get_sub_field('itinerary_time').'</strong></li>' : null;
$itinerary_remove_padding = get_sub_field('itinerary_remove_padding') ? get_sub_field('itinerary_remove_padding') : get_field('itinerary_remove_padding');
$itinerary_mode_of_transport = get_sub_field('itinerary_mode_of_transport') ? get_sub_field('itinerary_mode_of_transport') : get_field('itinerary_mode_of_transport');
$itinerary_show_path = get_sub_field('itinerary_show_path') ? get_sub_field('itinerary_show_path') : get_field('itinerary_show_path');
$path = $itinerary_show_path ? 'true' : 'false';

if (have_rows('itinerary_waypoint')) {
	$waypoint_index = 0;
	$waypoints_locations = array();
	$waypoints = array();
	while(have_rows('itinerary_waypoint')) {
		the_row();
		$waypoint_index++;
		$waypoint = get_sub_field('waypoint_location');
		$waypoints[] = '{location: "'.$waypoint['address'].'", stopover: true}';
		$waypoints_locations[] = $waypoint['address'];
	}
}

$waypoints_locations_count = count($waypoints_locations) - 1;
$waypointsStops = array_slice($waypoints_locations, 1, -1);
$waypoints_locations_str = urlencode(implode('|', $waypointsStops));

switch ($itinerary_remove_padding) {
    case 'top':
        $padding = 'collapse-top';
        break;
    case 'bottom':
        $padding = 'collapse-bottom';
        break;
    case 'both':
        $padding = 'collapse-top collapse-bottom';
        break;
    default:
        $padding = '';
}

$waypoint_row_index = 0;
?>

<script>
	var waypoints<?php echo $global_index; ?> = [<?php echo implode(',', $waypoints) ?>];
</script>

<section data-mode="<?php echo $itinerary_mode_of_transport; ?>" data-path="<?php echo $path; ?>" id="itinerary" class="itinerary <?php echo $padding; ?> bg--<?php echo $itinerary_bg; ?>">
	<?php if ($itinerary_grade || $itinerary_distance || $itinerary_time): ?>
	<div class="row bm">
		<div class="small-12 columns text-center">
			<ul class="itinerary__details">
				<?php echo $itinerary_grade; ?>
				<?php echo $itinerary_distance; ?>
				<?php echo $itinerary_time; ?>
			</ul>
		</div>
	</div>
	<?php endif; ?>
	<div class="row bm">
		<div class="small-12 columns text-center">
			<a class="itinerary__map-link" target="_blank" href="https://www.google.com/maps/dir/?api=1&travelmode=<?php echo $itinerary_mode_of_transport; ?>&origin=<?php echo $waypoints_locations[0]; ?>&destination=<?php echo $waypoints_locations[$waypoints_locations_count]; ?>&waypoints=<?php echo $waypoints_locations_str; ?>">See in Google Maps</a>
		</div>
	</div>
	<div class="waypoint__wrapper">
		<div class="waypoint__line">
        	<svg id="dashed-line">
				<line stroke-dasharray="22, 8" x1="0" y1="0" x2="0" y2="100%"></line>
			</svg>
        </div>
		<?php if ( have_rows('itinerary_waypoint') ): ?>
			<?php while ( have_rows('itinerary_waypoint') ) : the_row(); ?>
			<?php
				$image_id = get_sub_field('image');
				$body_copy = get_sub_field('text');
				$page_link = get_sub_field('page_link');
				$waypoint_row_index++;
				$expanded = $waypoint_row_index === 1 || $waypoint_row_index - 1 === $waypoints_locations_count ? 'expanded' : null;
			?>
			<div class="row <?php echo $expanded; ?> collapse itinerary__waypoint">
				<?php if ($waypoint_row_index - 1 !== $waypoints_locations_count): ?>
				<div class="small-12 medium-6 columns">
					<?php if ($waypoint_row_index === 1): ?>
					<div class="waypoint__map"></div>
					<?php else: ?>
					<div class="waypoint__image">
						<img data-object-fit="cover" class="lazyload" <?php responsive_image($image_id, 'large', true); ?>>
					</div>
					<?php endif ?>
				</div>
				<div class="small-12 medium-6 columns itinerary__text-container">
					<div class="waypoint__text">
						<?php echo $body_copy; ?>
						<?php if ($page_link): ?>
							<a class="read-more" href="<?php echo $page_link; ?>">See More</a>
						<?php endif; ?>
					</div>
				</div>
				<?php elseif($body_copy): ?>
				<?php $has_image = $image_id ? 'has-image' : null; ?>
				<div class="row itinerary__end <?php echo $has_image; ?>">
					<?php if ($image_id): ?>
						<img data-object-fit="cover" class="lazyload" <?php responsive_image($image_id, 'large-banner', true); ?>>
						<div class="end__line">
				        	<svg id="dashed-line">
								<line stroke-dasharray="22, 8" x1="0" y1="0" x2="0" y2="100%"></line>
							</svg>
				        </div>
					<?php endif; ?>
					<div class="itinerary__end--content">
						<?php echo $body_copy; ?>
						<?php if ($page_link): ?>
							<a class="read-more" href="<?php echo $page_link; ?>">See More</a>
						<?php endif; ?>
					</div>
				</div>
				<?php endif; ?>
			</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</section>