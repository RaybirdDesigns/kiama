<?php if( have_rows('footer_carousel_items', 'options') ): ?>
<div data-footer-carousel class="footer-carousel">
	<?php while ( have_rows('footer_carousel_items', 'options') ) : the_row(); ?>
		<div class="footer-carousel__item">
			<img class="lazyload" data-object-fit="cover" <?php responsive_image(get_sub_field('carousel_image'), 'large-banner', true); ?>>
			<div class="row">
				<div class="footer-carousel__text small-12 medium-6 columns">
					<?php the_sub_field('carousel_text'); ?>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
</div>
<?php endif; ?>