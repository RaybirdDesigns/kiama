<?php
$tile_links_section_heading = get_sub_field('tile_links_section_heading') ? get_sub_field('tile_links_section_heading') : get_field('tile_links_section_heading'); 
$tile_links_section_remove_padding = get_sub_field('tile_links_section_remove_padding') ? get_sub_field('tile_links_section_remove_padding') : get_field('tile_links_section_remove_padding');
$tile_links_section_tiles_shown_on_desktop = get_sub_field('tile_links_section_tiles_shown_on_desktop') ? get_sub_field('tile_links_section_tiles_shown_on_desktop') : get_field('tile_links_section_tiles_shown_on_desktop');
$tiles_to_show = $tile_links_section_tiles_shown_on_desktop ? $tile_links_section_tiles_shown_on_desktop : 4;
$tile_links_section_width = get_sub_field('tile_links_section_width') ? get_sub_field('tile_links_section_width') : get_field('tile_links_section_width');

$tile_links_grey_bg = get_sub_field('tile_links_grey_bg') ? get_sub_field('tile_links_grey_bg') : get_field('tile_links_grey_bg');

$tile_links_section_filter_by_category = get_sub_field('tile_links_section_filter_by_category') ? get_sub_field('tile_links_section_filter_by_category') : get_field('tile_links_section_filter_by_category');
$tile_links_section_filter_by_region = get_sub_field('tile_links_section_filter_by_region') ? get_sub_field('tile_links_section_filter_by_region') : get_field('tile_links_section_filter_by_region');
$tile_links_section_show_blog_posts = get_sub_field('tile_links_section_show_blog_posts') ? get_sub_field('tile_links_section_show_blog_posts') : get_field('tile_links_section_show_blog_posts');
$tile_links_section_results_to_show = get_sub_field('tile_links_section_results_to_show') ? get_sub_field('tile_links_section_results_to_show') : get_field('tile_links_section_results_to_show');
$tile_links_section_alternate_layout = get_sub_field('tile_links_section_alternate_layout') ? get_sub_field('tile_links_section_alternate_layout') : get_field('tile_links_section_alternate_layout');

$alternate_layout = $tile_links_section_alternate_layout ? 'alternate-layout' : '';

// Get all selected page regions abd create slug array for use in wp_query
$page_category = get_the_category( $post->ID );
if (!empty($page_category)) {
	$categories = array();
	foreach ($page_category as $category) {
		$categories[] = $category->slug;
	}
	$page_category_slugs = implode(',',$categories);
} else {
	$page_category_slugs = '';
}

// Get custom post types
$args = array(
   'public'   => true,
   'exclude_from_search'   => false,
   '_builtin' => false
);

$post_types = get_post_types( $args, 'names', 'and' );

$post_type_names = array();
foreach($post_types as $name => $slug) {
	$post_type_names[] = $slug;
}

$query_post_type = $tile_links_section_show_blog_posts ? 'post' : ($tile_links_section_filter_by_category ? $tile_links_section_filter_by_category : $post_type_names);

switch ($tile_links_section_remove_padding) {
    case 'top':
        $padding = 'collapse-top';
        break;
    case 'bottom':
        $padding = 'collapse-bottom';
        break;
    case 'both':
        $padding = 'collapse-top collapse-bottom';
        break;
    default:
        $padding = '';
}

?>
<section data-shown="<?php echo $tiles_to_show; ?>" class="tile-links-section <?php echo $padding; ?> <?php echo $alternate_layout; ?> tile-links-section--<?php echo $tile_links_grey_bg; ?>">
	<?php if ($tile_links_section_heading): ?>
	<div class="row">
		<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center bm">
			<?php echo $tile_links_section_heading; ?>
		</div>
	</div>
	<?php endif; ?>

    <?php
    ?>

	<?php

    $rows = get_sub_field('tile_links_section_featured_pages');
    $rnd_rows = shuffle($rows);

    if(have_rows('tile_links_section_featured_pages')) : ?>
	<div data-articles-carousel class="articles">
        <?php
        foreach($rows as $row) {

            $post_object = $row['tile_link'];
            if ( $post_object ) {
                $post = $post_object;
                setup_postdata( $post_object ); ?>
                <div class="article">
                    <?php set_query_var( 'curRow', $row ); ?>
                    <?php get_template_part('components/tile-link/tile-link'); ?>
                </div>
                <?php wp_reset_postdata();
            }?>
            <?php
        }
        ?>
	</div>
	<?php else: ?>
		<div data-articles-carousel class="articles">
		<?php
		$tile_links_args = array(
			'posts_per_page'		 => $tile_links_section_results_to_show ? $tile_links_section_results_to_show : 10,
			'post_type'				 => $query_post_type,
			'category_name'          => $tile_links_section_filter_by_region ? $page_category_slugs : null,
			'post_status'            => 'publish',
			'update_post_term_cache' => false,
			'paged'					 => false,
			'orderby'        		 => 'rand'
		);

		// if ($tile_links_section_filter_by_sub_category) {

		// 	$terms = get_the_terms( $post->ID, $tile_links_section_filter_by_category.'_type' );
		// 	if ( !empty( $terms ) ){
		// 	    // get the first term
		// 	    $term = array_shift( $terms );
		// 	    $term_slug = $term->slug;

		// 	    $sub_category_args = array(
		// 	        array (
		// 	            'taxonomy' => $tile_links_section_filter_by_category.'_type',
		// 	            'field' => 'slug',
		// 	            'terms' => $term_slug,
		// 	        )
		// 	    );

		//     	$tile_links_args['tax_query'] = $sub_category_args;
		// 	}
	 //    }

		$tile_links_query = new WP_Query($tile_links_args);

		while ( $tile_links_query->have_posts() ) :
    	$tile_links_query->the_post();
		?>
		<div class="article">
			<?php get_template_part('components/tile-link/tile-link'); ?>
		</div>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
		</div>
	<?php endif; ?>
</section>
