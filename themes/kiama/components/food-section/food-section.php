<?php
$food_section_heading = get_sub_field('food_section_heading') ? get_sub_field('food_section_heading') : get_field('food_section_heading'); 
$food_section_post = get_sub_field('food_section_banner_link') ? get_sub_field('food_section_banner_link') : get_field('food_section_banner_link'); 
$food_section_custom_image = get_field('food_section_custom_image') ? get_field('food_section_custom_image') : get_sub_field('food_section_custom_image');
$food_section_remove_padding = get_sub_field('food_section_remove_padding') ? get_sub_field('food_section_remove_padding') : get_field('food_section_remove_padding');

$food_section_hide_post_title_and_region = get_sub_field('food_section_hide_post_title_and_region') ? false : true;

$post_type = 'restaurant';

switch ($food_section_remove_padding) {
    case 'top':
        $padding = 'collapse-top';
        break;
    case 'bottom':
        $padding = 'collapse-bottom';
        break;
    case 'both':
        $padding = 'collapse-top collapse-bottom';
        break;
    default:
        $padding = '';
}
?>

<section class="food-section <?php echo $padding; ?>">
	<?php if ($food_section_heading): ?>
	<div class="row">
		<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center underline">
			<?php echo $food_section_heading; ?>
		</div>
	</div>
	<?php endif; ?>
	<?php include(locate_template('components/section-nav/section-nav.php', false, false )); ?>
	
	<?php if($food_section_post):
		$post = $food_section_post;
		setup_postdata( $post );
		$city_name = get_field('product_city_name') ? get_field('product_city_name') : get_sub_field('product_city_name');
		$primary_category = get_post_primary_category($post->ID, $term='category', $return_all_categories=false);
		$category_name = !empty($primary_category) ? '<span class="meta-pill meta-pill--white">'.$primary_category['primary_category']->name.'</span>' : ( $city_name ? '<span class="meta-pill meta-pill--white">'.$city_name.'</span>' : null);
	?>
	<div class="row expanded">
		<a href="<?php the_permalink(); ?>" class="food-section__banner">
			<?php if ($food_section_custom_image): ?>
		        <img class="lazyload" data-object-fit="cover" <?php responsive_image($food_section_custom_image, 'large-banner', true) ?>>
			<?php elseif (get_post_thumbnail_id($post->ID)): ?>
		        <img class="lazyload" data-object-fit="cover" <?php responsive_image_post('large-banner', true) ?>>
		    <?php elseif (get_field('product_image_0')): ?>
		        <img class="lazyload" data-object-fit="cover" <?php adtw_image(2400, 0, true); ?>>
		    <?php else: ?>
		        <?php if( have_rows('all_sections') ): ?>
		            <?php while ( have_rows('all_sections') ) : the_row(); ?>
		                <?php $hero_standard_image = get_sub_field('hero_image') ? get_sub_field('hero_image') : get_field('hero_image'); ?>
		                <?php if($hero_standard_image): ?>
		                <img class="lazyload" data-object-fit="cover" <?php responsive_image($hero_standard_image, 'large-banner', true) ?>>
		                <?php endif; ?>
		            <?php endwhile; ?>
		        <?php endif; ?>
		    <?php endif  ?>
		    <?php if ($food_section_hide_post_title_and_region): ?>
		    	<?php echo $category_name; ?>
		    	<span class="meta-pill meta-pill--black"><?php the_title(); ?></span>
			<?php endif; ?>
		</a>
	</div>
	<?php wp_reset_postdata(); ?>
	<?php endif; ?>
</section>