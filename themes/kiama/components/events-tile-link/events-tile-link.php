<?php

$post_type = get_post_type();
$category = get_post_primary_category($post->ID, $term='category', $return_all_categories=false);
$city_name = get_field('product_city_name') ? get_field('product_city_name') : get_sub_field('product_city_name');
$category_name = !empty($category) ? '<span class="meta-pill meta-pill--white">'.$category['primary_category']->name.'</span>' : ( $city_name ? '<span class="meta-pill meta-pill--white">'.$city_name.'</span>' : null);

$terms = get_the_terms( $post->ID, $post_type.'_type');
$term_names_arr = array();
foreach($terms as $term) {
    if ($term->slug !== 'nothing') {
        $term_names_arr[] = '#'.$term->name;
    }
}
$term_names = implode(', ',$term_names_arr);
// Get first term
$term = array_shift( $terms );
$term_slug = $term->slug;

$product_rate_from = get_field('product_rate_from');
$product_rate_to = get_field('product_rate_to');

$custom_image = get_field('custom_image') ? get_field('custom_image') : get_sub_field('custom_image');
$custom_icon = get_field('custom_icon') ? get_field('custom_icon') : get_sub_field('custom_icon');

$start_time = get_field('start_time');
$end_time = get_field('end_time');

$custom_event_dates = get_field('custom_event_dates');

$start_time_formatted = $start_time ? date('d M y',strtotime($start_time)) : null;
$end_time_formatted = $start_time !== $end_time ? ' - '.date('d M y',strtotime($end_time)) : null;

$start_hour_raw = get_field('start_hour');
$start_hour = date('h:i a', strtotime($start_hour_raw));
$end_hour = get_field('end_hour') ? ' - '.date('h:i a', strtotime(get_field('end_hour'))) : null;

$fallback_image_id = get_field('fallback_image', 'options');

?>

<a href="<?php the_permalink(); ?>" class="event-tile-link">
    <div class="event-tile-link__head">
        <?php if ($custom_event_dates): ?>
            <span class="date"><?php echo $custom_event_dates; ?></span>
        <?php elseif ($start_time): ?>
            <span class="date"><?php echo $start_time_formatted; ?><?php echo $end_time_formatted; ?></span>
        <?php endif; ?>
        <?php if ($term_slug): ?>
        <svg class="event-tile-link__head--icon" role="presentation">
            <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/icons.svg#<?php echo $term_slug; ?>"></use>
        </svg>
        <?php endif; ?>
        <?php if ($start_hour_raw): ?>
            <span class="time"><?php echo $start_hour; ?><?php echo $end_hour; ?></span>
        <?php endif; ?>
    </div>  
    <div class="event-tile-link__image">
        <?php if ($custom_image): ?>
            <img class="lazyload" data-object-fit="cover" <?php responsive_image($custom_image, 'large', true) ?>>
    	<?php elseif (get_post_thumbnail_id($post->ID)): ?>
            <img class="lazyload" data-object-fit="cover" <?php responsive_image_post('large', true) ?>>
        <?php elseif (get_field('product_image_0')): ?>
            <img class="lazyload" data-object-fit="cover" <?php adtw_image(880, 0, true); ?>>
        <?php else:  ?>
            <img data-object-fit="cover" <?php responsive_image($fallback_image_id, 'small') ?>>
        <?php endif; ?>
        <?php echo $category_name; ?>
    </div>
    <div class="event-tile-link__text">
        <div>
        	<h4><?php the_title(); ?></h4>
            <?php if ($product_rate_from): ?>
            <span class="meta"> ⁠— from $<?php echo $product_rate_from; ?></span>
            <?php endif; ?>
        </div>
    	<?php if (get_the_excerpt()) : ?>
            <?php the_excerpt(); ?>
        <?php endif ?>
        <span class="read-more">Read More</span>
    </div>
</a>
<button aria-label="Click to Favourite <?php the_title(); ?>" data-postid="<?php echo $post->ID; ?>" onclick="window.addFavourite(<?php echo $post->ID; ?>)" class="favourite-btn">
    <svg class="icon" role="presentation">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/global-sprite.svg#fav-ico"></use>
    </svg>
</button>
