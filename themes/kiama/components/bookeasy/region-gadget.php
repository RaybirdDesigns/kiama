<?php

$bookeasy_region_listings_type = get_sub_field('bookeasy_region_listings_type') ? get_sub_field('bookeasy_region_listings_type') : get_field('bookeasy_region_listings_type');
$bookeasy_region_remove_padding = get_sub_field('bookeasy_region_remove_padding') ? get_sub_field('bookeasy_region_remove_padding') : get_field('bookeasy_region_remove_padding');
$bookeasy_region_heading = get_sub_field('bookeasy_region_heading') ? get_sub_field('bookeasy_region_heading') : get_field('bookeasy_region_heading');

switch ($bookeasy_region_remove_padding) {
    case 'top':
        $padding = 'collapse-top';
        break;
    case 'bottom':
        $padding = 'collapse-bottom';
        break;
    case 'both':
        $padding = 'collapse-top collapse-bottom';
        break;
    default:
        $padding = '';
}

switch($bookeasy_region_listings_type) {
	case 'accomm':
        $disabledTypes = '"tours","events","carhire","packages"';
        break;
    case 'tour':
        $disabledTypes = '"accom","events","carhire","packages"';
        break;
    case 'event':
        $disabledTypes = '"tours","accom","carhire","packages"';
        break;
    case 'hire':
        $disabledTypes = '"tours","events","accom","packages"';
        break;
    default:
    	$disabledTypes = '"tours","events","carhire","packages"';
}

// Get all accomm posts with 
$args = array(
	'posts_per_page'		 => -1,
	'post_type'				 => $bookeasy_region_listings_type,
	'category_name'          => null,
	'post_status'            => 'publish',
	'update_post_term_cache' => false,
	'paged'					 => false,
	'meta_query'			 =>	array(
        array (
            'key'		=> 'bookeasy_id',
            'value'		=> '',
        	'compare'	=> '!=',
        )
    )
);

$bookeasy_query = new WP_Query($args);
$bookeasy_posts = $bookeasy_query->posts;

$operator_ids = array();
?>

<script type="text/javascript">
    BEcssOverride = "minimal";
	var BE_gadgetURLOverrides = {
	    <?php foreach( $bookeasy_posts as $post ): setup_postdata( $post ); ?>
            <?php $operator_ids[] = (int)get_field('bookeasy_id'); ?>
	    	<?php echo (int)get_field('bookeasy_id'); ?>: '<?php echo get_permalink(); ?>',
	    <?php endforeach; ?>
	};

    var showMap;
    if (window.innerWidth > 768) {
        showMap = true;
    } else {
        showMap = false;
    }

    $w(function() {
        BE.gadget.region('#region-conatiner',{
            vcID: 129,
            googleMapsKey: '<?php the_field('google_maps_api_key','options'); ?>',
            period: '2',
			adults: '2',
			accomOnlyMode: <?php echo $bookeasy_region_listings_type === 'accomm' ? 'true' : 'false' ?>,
			collapseRefineTools:true,
			collapseToursMode:true,
			crossDomainSearch:false,
			includedOperators: [<?php echo implode(',',$operator_ids); ?>],
			disabledTypes:[<?php echo $disabledTypes; ?>],
			ignoreSearchCookie:true,
			showRefineTools: true,
			showLegend:false,
            showList: false,
            defaultSort: 'instant',
            showMap: showMap,
            customMapIcons: {
                'accom':{
                    icon:'<?php echo get_template_directory_uri().'/images/bookeasy/accom-icon.png'; ?>',
                    pinpoint: [15,40],
                    size: [29,40]
                },
                'events':{
                    icon:'<?php echo get_template_directory_uri().'/images/bookeasy/event-icon.png'; ?>',
                    pinpoint: [15,40],
                    size: [29,40]
                },
            }
        });
    });

</script>

<section id="region" class="<?php echo $padding; ?>">
	<div class="row">
		<div class="small-12 columns">
			<div id="region-conatiner"></div>
		</div>
	</div>
</section>
<?php wp_reset_postdata(); ?>