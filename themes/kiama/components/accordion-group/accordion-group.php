<section class="accordion-group collapse-top collapse-bottom">
    <div class="row">
        <div class="small-12 columns">
    <?php
        if(have_rows('dropdown_groups')) {
            while( have_rows('dropdown_groups')) {
                the_row();

                $title = get_sub_field('title');
                echo $title ? "<h2 class='accordion-group-header'>$title</h2>" : null;
                echo '<div class="question-wrapper">';
                if(have_rows('questionsanswers')) {
                    while(have_rows('questionsanswers')){
                        the_row();
                        $question = get_sub_field('question');
                        $answer = get_sub_field('answer');

                        echo "<div class='question-wrapper'>
                                <h3 class='question'>
                                    $question
                                    <svg class=\"icon\" role=\"presentation\">
									    <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#down-ico\"></use>
									</svg>
                                </h3>
                                <div class='answer'>
                                    $answer
                                </div>
                            </div>";
                    }
                }
                echo '</div>';
            }
        }
    ?>
        </div>
    </div>
</section>