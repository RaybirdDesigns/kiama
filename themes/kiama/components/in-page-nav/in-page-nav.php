<?php

$in_page_nav_background_colour = get_sub_field('in_page_nav_background_colour') ? get_sub_field('in_page_nav_background_colour') : get_field('in_page_nav_background_colour');
$in_page_nav_heading_text = get_sub_field('in_page_nav_heading_text') ? get_sub_field('in_page_nav_heading_text') : get_field('in_page_nav_heading_text');
$in_page_nav_link_text = get_sub_field('in_page_nav_link_text') ? get_sub_field('in_page_nav_link_text') : get_field('in_page_nav_link_text');
$in_page_nav_link_url = get_sub_field('in_page_nav_link_url') ? get_sub_field('in_page_nav_link_url') : get_field('in_page_nav_link_url');

$text_color = $in_page_nav_background_colour === '#f58b6d' ? '#ffffff' : '#363636';
$is_dark = $in_page_nav_background_colour === '#f58b6d' ? '' : ' dark';

?>

<?php if( have_rows('in_page_nav_icon_links') ): ?>
<style>

    .in-page-nav__link:hover .in-page-nav__icon-wrapper {
    	background-color: <?php echo $text_color; ?>;
    }

    @media only screen and (min-width: 480px) {
	  .in-page-nav__icon-wrapper {
	    	border: solid 2px <?php echo $text_color; ?> !important;
	    }
	}

	.in-page-nav__icon {
		fill: <?php echo $text_color; ?>;
	}

	.in-page-nav {
		background-color: <?php echo $in_page_nav_background_colour; ?>!important;
	}
	.in-page-nav__link:hover .in-page-nav__icon {
		fill: <?php echo $in_page_nav_background_colour; ?>;
	}
</style>

<section id="in-page-nav" class="in-page-nav <?php echo $is_dark; ?>">
	<div class="row">
		<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center">
			<?php if ($in_page_nav_heading_text): ?>
			<?php echo $in_page_nav_heading_text; ?>
			<?php endif; ?>
			<?php if($in_page_nav_link_url): ?>
			<a class="btn btn-secondary btn-medium btn-arrow-black" href="">Contact Us</a>
			<?php endif; ?>
		</div>
	</div>
	<div class="row ">
		<div class="small-12 large-10 large-offset-1 in-page-nav__links">
			<?php while ( have_rows('in_page_nav_icon_links') ) : the_row(); ?>
			    <a class="in-page-nav__link" href="<?php the_sub_field('page_link'); ?>">
			    <div class="in-page-nav__icon-wrapper">
					<svg class="in-page-nav__icon" role="presentation">
	                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/icons.svg#<?php the_sub_field('icon'); ?>"></use>
	                </svg>
			    </div>
                <?php if(get_sub_field('link_text')): ?>
                	<?php
                	$svg = '<svg class="angle-ico icon" role="presentation">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#angle-ico"></use>
                    </svg></span>';
                	$text_field = get_sub_field('link_text');
                	$text_field_arr = explode(' ', $text_field);
                	$last_word = end($text_field_arr);
                	$last_word_spaned = '<span>'.$last_word.'&nbsp;'.$svg.'</span>';
                	$text = str_replace($last_word,$last_word_spaned,$text_field);
                	?>
                	<span class="<?php echo $is_dark; ?>"><?php echo $text; ?></span>
                <?php endif; ?>
				</a>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>