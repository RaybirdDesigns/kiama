<button class="main-menu-btn"><span class="hamburger"></span></button>
<nav class="main-nav clearfix">
    <div class="menu-header">
        <button class="menu-header-close-btn">
            <svg class="menu-header-close-ico" role="presentation">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#cross"></use>
            </svg>
        </button>
    </div>
    <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
</nav>