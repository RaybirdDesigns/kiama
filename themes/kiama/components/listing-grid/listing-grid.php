<?php

$listing_grid_remove_padding = get_sub_field('listing_grid_remove_padding');
$listing_grid_region = get_sub_field('listing_grid_region');
$post_type = get_sub_field('listing_grid_category');
$post_type_value = $post_type ? $post_type['value'] : null;
$all_post_types = array('accomm','attraction','event','restaurant','genservice','hire','tour','transport','destinfo','info');
$regions_param = isset($_GET['rgn']) ? $_GET['rgn'] : null;

$region_cat = isset($_GET['rgn']) ? $_GET['rgn'] : $listing_grid_region;

$sub_categories = array(
	'accomm' => get_sub_field('listing_grid_accomm_sub_categories'),
	'attraction' => get_sub_field('listing_grid_attraction_sub_categories'),
	'event' => get_sub_field('listing_grid_event_sub_categories'),
	'restaurant' => get_sub_field('listing_grid_restaurant_sub_categories'),
	'genservice' => get_sub_field('listing_grid_genservice_sub_categories'),
	'hire' => get_sub_field('listing_grid_hire_sub_categories'),
	'tour' => get_sub_field('listing_grid_tour_sub_categories'),
	'transport' => get_sub_field('listing_grid_transport_sub_categories'),
	'info' => get_sub_field('listing_grid_info_sub_categories'),
);

switch ($listing_grid_remove_padding) {
    case 'top':
        $padding = 'collapse-top';
        break;
    case 'bottom':
        $padding = 'collapse-bottom';
        break;
    case 'both':
        $padding = 'collapse-top collapse-bottom';
        break;
    default:
        $padding = '';
}

$args = array(
    'post_type'      => $post_type_value ? $post_type_value : $all_post_types,
    'posts_per_page' => -1,
    'post_status'    => 'publish'
);

$the_query = new WP_Query( $args );
$region_categories = array();
if ( $the_query->have_posts() ) {
    while ( $the_query->have_posts() ) {
        $the_query->the_post();

        $terms = get_the_terms( get_the_ID(), 'category' );
        if ( $terms && ! is_wp_error( $terms ) ) : 
            foreach ( $terms as $term ) {
            if(!in_array($term->term_id, $region_categories))
                $region_categories[] = $term->term_id;
            }   
        endif;  
    }
    wp_reset_postdata();

    $region_count = count($region_categories);
}

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$listing_grid_args = array(
	'posts_per_page'		 => 16,
	'post_type'				 => $post_type_value ? $post_type_value : $all_post_types,
	'cat'          			 => $region_cat ? $region_cat : null,
	'post_status'            => 'publish',
	'update_post_term_cache' => false,
	'paged'					 => $paged,
    'orderby' 				 => 'title',
    'order'   				 => 'ASC',
);

if (!empty($sub_categories[$post_type_value])) {

	$terms_ids = array();
	foreach ($sub_categories[$post_type_value] as $term_id) {
		$terms_ids[] = $term_id->term_id;
	}

    $sub_category_args = array(
        array (
            'taxonomy' => $post_type_value.'_type',
            'field' => 'term_id',
            'terms' => $terms_ids,
        )
    );

	$listing_grid_args['tax_query'] = $sub_category_args;
}

$wp_query_grid = new WP_Query($listing_grid_args);
$count = $wp_query_grid->post_count;
$posts = $wp_query_grid->posts;

?>

<?php if ($count > 0): ?>
<section id="listing-grid" class="listing-grid <?php echo $padding; ?>">
	<?php if(empty($listing_grid_region) && $region_count > 0): ?>
	<div class="row">
		<div class="small-12 columns">
			<form class="filter" action="<?php the_permalink(); ?>#listing-grid">
				<ul class="filter__region-list">
					<?php $all_regions_current = $regions_param === '' || !$regions_param ? 'checked' : null; ?>
					<li class="filter__region-list--item"><input <?php echo $all_regions_current; ?> id="all-regions" type="radio" name="rgn" value=""><label for="all-regions">SEE All</label></li>


					<?php if(sizeof($region_categories)) : ?>
					    <?php foreach ($region_categories as $term_id) : ?>
					        <?php $region = get_term_by('id', $term_id, 'category'); ?>
					        <?php $region_current = (int)$regions_param === $term_id ? 'checked' : null; ?>
					            <li class="filter__region-list--item"><input <?php echo $region_current; ?> id="term-<?php echo $term_id; ?>" type="radio" name="rgn" value="<?php echo $term_id; ?>"><label for="term-<?php echo $term_id; ?>"><?php echo $region->name; ?></label></li>
					    <?php endforeach; ?>
					<?php endif; ?>
				</ul>
			</form>
		</div>
	</div>
	<?php endif; ?>
	<div class="row">
		<?php foreach ( $posts as $post ) : ?>
    	<?php setup_postdata( $post ); ?>
			<div class="small-12 medium-6 large-4 xlarge-3 columns">
		    	<div class="listing-grid__article">
					<?php get_template_part('components/tile-link/tile-link'); ?>
		    	</div>
			</div>
		<?php endforeach; ?>
	</div>
	<div class="row">
        <div class="small-12 columns pagination">
            <?php 
            $wp_query_grid->query_vars['paged'] > 1 ? $current = $wp_query_grid->query_vars['paged'] : $current = 1;

		    echo paginate_links( array(
		        'base'      => @add_query_arg('paged','%#%'),
		        'format'    => '?paged=%#%',
		        'prev_text' => __('Previous'),
		        'next_text' => __('Next'),
		        'current'   => $current,
		        'total'     => $wp_query_grid->max_num_pages
		    ) );
             ?>
        </div>
    </div>
</section>
<?php wp_reset_postdata(); ?>
<?php else: ?>
<section id="listing-grid" class="listing-grid no-listings <?php echo $padding; ?>">
	<div class="row align-center">
		<div class="small-12 columns text-center">
			<p>Sorry, we don't have any listings in that category yet...</p>
			<a class="btn btn-medium btn-primary btn-arrow-black" href="<?php echo get_permalink(); ?>#listing-grid">Go Back to All Categories</a>
		</div>
    </div>
</section>
<?php endif; ?>