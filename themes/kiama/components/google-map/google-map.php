<?php 

$location = get_sub_field('google_map') ? get_sub_field('google_map') : get_field('google_map'); 
$google_map_remove_padding = get_sub_field('google_map_remove_padding') ? get_sub_field('google_map_remove_padding') : get_field('google_map_remove_padding'); 
$google_map_width = get_sub_field('google_map_width') ? get_sub_field('google_map_width') : get_field('google_map_width'); 
$google_map_zoom_level = get_sub_field('google_map_zoom_level') ? get_sub_field('google_map_zoom_level') : get_field('google_map_zoom_level'); 
$google_map_style = get_sub_field('google_default_map_style') ? get_sub_field('google_default_map_style') : get_field('google_default_map_style');

switch ($google_map_style) {
    case 'default':
        $map_style = 'data-default';
        break;
    case 'bottom':
        $map_style = 'data-green';
        break;
    default:
        $map_style = 'data-green';
}

switch ($google_map_remove_padding) {
    case 'top':
        $padding = 'collapse-top';
        break;
    case 'bottom':
        $padding = 'collapse-bottom';
        break;
    case 'both':
        $padding = 'collapse-top collapse-bottom';
        break;
    default:
        $padding = '';
}

switch ($google_map_width) {
	case 'tiny':
        $width = 'medium-10 medium-offset-1 large-6 large-offset-3 columns';
        break;
    case 'narrow':
        $width = 'medium-10 medium-offset-1 large-8 large-offset-2 columns';
        break;
    case 'medium':
        $width = 'medium-8  medium-offset-2 large-10 large-offset-1 columns';
        break;
    case 'wide':
        $width = 'large-12 columns';
        break;
    case 'full':
        $width = '';
        break;
    default:
        $width = 'large-12';
}

$expanded = $google_map_width === 'full' ? 'expanded' : '';

if( !empty($location) ):
?>
<section <?php echo $map_style; ?> data-zoom="<?php echo $google_map_zoom_level; ?>" class="google-map-section <?php echo $padding; ?>">
	<div class="row <?php echo $expanded; ?>">
		<div class="small-12 <?php echo $width; ?>">	
			<div class="acf-map google-map">
				<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>