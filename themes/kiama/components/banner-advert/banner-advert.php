<?php

$post_object = get_sub_field('banner_advert_section');

if( $post_object ):

	$post = $post_object;
	setup_postdata( $post ); 
	$id_banner_advert = get_sub_field('id_banner_advert') ? get_sub_field('id_banner_advert') : get_field('id_banner_advert');
	$desktop_banner_advert = get_sub_field('desktop_banner_advert') ? get_sub_field('desktop_banner_advert') : get_field('desktop_banner_advert');
	$mobile_banner_advert = get_sub_field('mobile_banner_advert') ? get_sub_field('mobile_banner_advert') : get_field('mobile_banner_advert');
	$banner_advert_external_link = get_sub_field('banner_advert_external_link') ? get_sub_field('banner_advert_external_link') : get_field('banner_advert_external_link');
	$banner_advert_internal_link = get_sub_field('banner_advert_internal_link') ? get_sub_field('banner_advert_internal_link') : get_field('banner_advert_internal_link');
	$link = $banner_advert_internal_link ? $banner_advert_internal_link : $banner_advert_external_link;
	$target = $banner_advert_external_link ? 'target="_blank"' : '';
	$id_banner_advert_id = $id_banner_advert ? 'id="'.$id_banner_advert.'"' : '';

	?>
    <a href="<?php echo $link; ?>" <?php echo $target; ?> <?php echo $id_banner_advert_id ?> class="banner-advert">
		<img class="banner-advert__desktop lazyload" <?php responsive_image($desktop_banner_advert, 'full', true); ?>>
		<img class="banner-advert__mobile lazyload" <?php responsive_image($mobile_banner_advert, 'full', true); ?>>
	</a>
    <?php wp_reset_postdata(); ?>
<?php endif; ?>

