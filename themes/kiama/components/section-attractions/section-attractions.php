<?php
$attractions_section_heading = get_sub_field('attractions_section_heading') ? get_sub_field('attractions_section_heading') : get_field('attractions_section_heading'); 
$attractions_section_filter_by_region = get_sub_field('attractions_section_filter_by_region') ? get_sub_field('attractions_section_filter_by_region') : get_field('attractions_section_filter_by_region'); 
$attractions_section_filter_sub_category = get_sub_field('attractions_section_filter_sub_category') ? get_sub_field('attractions_section_filter_sub_category') : get_field('attractions_section_filter_sub_category'); 
$attractions_section_remove_padding = get_sub_field('attractions_section_remove_padding') ? get_sub_field('attractions_section_remove_padding') : get_field('attractions_section_remove_padding');

$attractions_section_button_text = get_sub_field('attractions_section_button_text') ? get_sub_field('attractions_section_button_text') : get_field('attractions_section_button_text'); 
$attractions_section_button_link = get_sub_field('attractions_section_button_link') ? get_sub_field('attractions_section_button_link') : get_field('attractions_section_button_link'); 

// Get all selected page regions abd create slug array for use in wp_query
$page_category = get_the_category( $post->ID );
if (!empty($page_category)) {
	$categories = array();
	foreach ($page_category as $category) {
		$categories[] = $category->slug;
	}
}
$page_category_slugs = implode(',',$categories);

$post_type = 'attraction';
$custom_tax = 'attraction_type';

switch ($attractions_section_remove_padding) {
    case 'top':
        $padding = 'collapse-top';
        break;
    case 'bottom':
        $padding = 'collapse-bottom';
        break;
    case 'both':
        $padding = 'collapse-top collapse-bottom';
        break;
    default:
        $padding = '';
}

$attractions_section_args = array(
	'posts_per_page'		 => 6,
	'post_type'				 => $post_type,
	'category_name'          => $attractions_section_filter_by_region ? $page_category_slugs : null,
	'post_status'            => 'publish',
	'update_post_term_cache' => false,
	'paged'					 => false,
	'orderby'        		 => 'rand'
);

if (!empty($attractions_section_filter_sub_category)) {

    $sub_category_args = array(
        array (
            'taxonomy' => $custom_tax,
            'field' => 'term_id',
            'terms' => $attractions_section_filter_sub_category,
        )
    );

	$attractions_section_args['tax_query'] = $sub_category_args;
}

$attractions_section_query = new WP_Query($attractions_section_args);
$attractions_count = $attractions_section_query->post_count;

if ($attractions_count > 0): ?>
	<section class="section-attractions <?php echo $padding; ?>">
		<?php if ($attractions_section_heading): ?>
		<div class="row">
			<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center underline">
				<?php echo $attractions_section_heading; ?>
			</div>
		</div>
		<?php endif; ?>
		<?php if ($attractions_section_button_link): ?>
		<div class="row">
			<div class="small-12 columns text-center">
				<a class="btn btn-medium btn-primary btn-arrow-black btn-margin-b" href="<?php echo $attractions_section_button_link ?>"><?php echo $attractions_section_button_text ?></a>
			</div>
		</div>
		<?php endif; ?>
		<?php include(locate_template('components/section-nav/section-nav.php', false, false )); ?>

		<div class="row align-center collapse">
			<?php
			$attraction_index = 0;
			while ( $attractions_section_query->have_posts() ) :
	    	$attractions_section_query->the_post();
			$attraction_index++;

			switch($attractions_count) {
				case 1:
			        $layout = 'small-12 large-6 columns';
			        break;
			    case 2:
			        $layout = 'small-12 large-12 columns';
			        break;
			    case 3:
			        $layout = 'small-12 large-4 columns';
			        break;
			    case 4:
			        $layout = 'small-12 large-4 columns';
			        break;
			    case 5:
			        $layout = 'small-12 large-4 columns';
			        break;
			    case 6:
			        $layout = 'small-12 large-4 columns';
			        break;
			    default:
			        $layout = 'small-12 large-4 columns';
			}
	    	?>
			<?php if ($attraction_index === 1 || $attraction_index === 3 || $attraction_index === 5):  ?>
				<div class="attractions-column attractions-count-<?php echo $attractions_count; ?> <?php echo $layout; ?>">
			<?php endif; ?>
			<div class="attractions-article">
				<?php get_template_part('components/tile-link/tile-link'); ?>
			</div>
			<?php if ($attraction_index === 2 || $attraction_index === 4 || $attraction_index === 6):  ?>
				</div>
			<?php endif; ?>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; ?>
<?php wp_reset_postdata(); ?>
