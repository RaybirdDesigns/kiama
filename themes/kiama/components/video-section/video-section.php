<?php
$video_section_heading = get_sub_field('video_section_heading') ? get_sub_field('video_section_heading') : get_field('video_section_heading'); 
$video_section_button_text = get_sub_field('video_section_button_text') ? get_sub_field('video_section_button_text') : get_field('video_section_button_text'); 
$video_section_button_link = get_sub_field('video_section_button_link') ? get_sub_field('video_section_button_link') : get_field('video_section_button_link'); 
$video_section_remove_padding = get_sub_field('video_section_remove_padding') ? get_sub_field('video_section_remove_padding') : get_field('video_section_remove_padding');
$video_section_filter_by_region = get_sub_field('video_section_filter_by_region') ? get_sub_field('video_section_filter_by_region') : get_field('video_section_filter_by_region'); 
$video_section_select_videos = get_sub_field('video_section_select_videos') ? get_sub_field('video_section_select_videos') : get_field('video_section_select_videos');

$page_category = get_the_category( $post->ID );
$page_category_slug = !empty($page_category) ? $page_category[0]->slug : null;

switch ($video_section_remove_padding) {
    case 'top':
        $padding = 'collapse-top';
        break;
    case 'bottom':
        $padding = 'collapse-bottom';
        break;
    case 'both':
        $padding = 'collapse-top collapse-bottom';
        break;
    default:
        $padding = '';
}

?>
<section class="video-section <?php echo $padding; ?>">
	<?php if ($video_section_heading): ?>
	<div class="row">
		<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center underline">
			<?php echo $video_section_heading; ?>
		</div>
	</div>
	<?php endif; ?>
	<?php if($video_section_button_link): ?>
	<div class="row">
		<div class="small-12 columns text-center">
			<a class="btn btn-medium btn-primary btn-arrow btn-margin-b" href="<?php echo $video_section_button_link; ?>"><?php echo $video_section_button_text; ?></a>
		</div>
	</div>
	<?php endif; ?>
	<div class="row align-center">

<?php

$video_section_args = array(
	'posts_per_page'		 => 3,
	'post_type'				 => 'video',
	'category_name'          => $video_section_filter_by_region ? $page_category_slug : null,
	'post_status'            => 'publish',
	'update_post_term_cache' => false,
	'paged'					 => false,
    'orderby'        		 => 'rand'
);

$video_section_query = new WP_Query($video_section_args);

// Process featured videos and merge with dynamic videos
if (is_array($video_section_select_videos)) {
	$clean_selected_videos_obj = array();
	foreach($video_section_select_videos as $item) {
		if ($item['select_a_video']->post_status === 'publish') {
			$clean_selected_videos_obj[] = $item['select_a_video'];
		}
	}
	// Precaution against expired videos being used in featured videos
	if (count($clean_selected_videos_obj) > 0) {
		$video_posts = array_unique(array_merge($clean_selected_videos_obj, $video_section_query->posts), SORT_REGULAR);
	} else {
		$video_posts = $video_section_query->posts;
	}
} else {
	$video_posts = $video_section_query->posts;
}

$video_posts = array_slice($video_posts, 0, 3);

$count = count($video_posts);

if ($count > 0) :?>
			<?php
			foreach( $video_posts as $post ):
			setup_postdata( $post );
			$layout = $count === 1 ? 'small-12 medium-8' : ($count === 2 ? 'small-12 medium-6' : 'small-12 medium-4');
			?>
				<div class="<?php echo $layout; ?> columns">
				    <?php get_template_part('components/video-section/video-tile'); ?>
				</div>	
			<?php endforeach; ?>
			<?php wp_reset_postdata(); ?>
		<?php endif; ?>
	</div>
</section>
