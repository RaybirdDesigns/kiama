<?php $youtube_share_id = get_field('youtube_share_id'); ?>

<button title="Play Video" class="video-section__btn" data-youtube="<?php echo $youtube_share_id; ?>">
	<span class="show-for-sr">Play Video</span>
	<img class="lazyload" data-src="http://i3.ytimg.com/vi/<?php echo $youtube_share_id; ?>/maxresdefault.jpg" alt="kiama video">
	<span class="video-section__btn--title"><?php the_title(); ?></span>
	<svg class="video-section__btn--ico" role="presentation">
	    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#play-ico"></use>
	</svg>
</button>