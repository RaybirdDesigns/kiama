<?php

// ===============================================================================================
// This is used for getting post titles for the favourites notification
// ===============================================================================================
// @example: /wp-json/custom/v1/listing_title?id=54
add_action( 'rest_api_init', 'custom_api_listing_title' );

function custom_api_listing_title() {
    register_rest_route( 'custom/v1', '/listing_title', array(
        'methods' => 'GET',
        'callback' => 'custom_api_listing_title_callback'
    ));
}
// Used in the quick search.
function custom_api_listing_title_callback( $request ) {
    // Initialize the array that will receive the posts' data. 
    $posts_data = array();
    $post_id = $request->get_param( 'id' ) ? $request->get_param( 'id' ) : null;

    $listing_title_args = array(
    	'paged' => false,
    	'p' => $post_id,
        'post_type' => 'any',
        'post_status' => 'publish',
        'update_post_term_cache' => false,
        'posts_per_page' => 1,            
    );

    $listing_title_query = new WP_Query($listing_title_args);

    $posts = $listing_title_query->posts;

    // Loop through the posts and push the desired data to the array we've initialized earlier in the form of an object
    foreach( $posts as $post ) {

        $posts_data[] = (object) array(
            'title' => $post->post_title,
        );
    }

    $response = new WP_REST_Response( $posts_data );

    return $response;                   
}

// ===============================================================================================
// This is used for getting favourited listings
// ===============================================================================================
// @example: /wp-json/custom/v1/favourites?ids=54,1165,878
add_action( 'rest_api_init', 'custom_api_favourites' );   

function custom_api_favourites() {
    register_rest_route( 'custom/v1', '/favourites', array(
        'methods' => 'GET',
        'callback' => 'custom_api_favourites_callback'
    ));
}
// Used in the quick search.
function custom_api_favourites_callback( $request ) {

    $post_ids = $request->get_param( 'ids' ) ? $request->get_param( 'ids' ) : null;

    $listing_args = array(
    	'paged' => false,
        'post_type' => 'any',
        'post_status' => 'publish',
        'post__in' => explode(',',$post_ids),
        'update_post_term_cache' => false,
        'posts_per_page' => -1,            
    );

    $listing_query = new WP_Query($listing_args);

    $posts = $listing_query->posts;
    $global_fallback_image = get_field('fallback_image', 'options');
    // Initialize the array that will receive the posts' data. 
    $posts_data = array();

    // Loop through the posts and push the desired data to the array we've initialized earlier in the form of an object
    foreach( $posts as $post ) {
        $id = $post->ID;
        $has_post_thumbnail = has_post_thumbnail( $id );
        $post_thumbnail = $has_post_thumbnail ?
        		wp_get_attachment_image_url(get_post_thumbnail_id( $id ), 'medium') :
        		(get_field('product_image_0', $id) ? get_field('product_image_0', $id) : get_field('product_image_1', $id));
        $featured_image = $has_post_thumbnail ? array('media_details' => array('sizes' => array('large' => array('source_url' => $post_thumbnail))))['media_details']['sizes']['large']['source_url'] : null;
        $post_link = append_bookeasy_string(get_permalink( $id, false ), $id);
        $post_excerpt = wp_trim_words($post->post_content, 32);
        $category_name = !is_wp_error(get_the_terms($id, $post->post_type.'_type')) ? get_the_terms($id, $post->post_type.'_type')[0]->name : '';
        $category_slug = !is_wp_error(get_the_terms($id, $post->post_type.'_type')) ? get_the_terms($id, $post->post_type.'_type')[0]->slug : '';
        $longitude = get_field('product_long', $id);
        $latitude = get_field('product_lat', $id);
        $start_date = get_field('start_time', $id);
        $end_date = get_field('end_time', $id);

        $posts_data[] = (object) array(
            'id' => $id,
            'title' => $post->post_title,
            'sub_category' => $category_name,
            'sub_category_label' => $category_name,
            'category' => $post->post_type,
            'title' => array('rendered' => $post->post_title)['rendered'],
            'link' => $post_link,
            'image' => $featured_image ? $featured_image : ($post_thumbnail ? $post_thumbnail.'?w=480' : wp_get_attachment_image_url($global_fallback_image, 'large')),
            'excerpt' => array('rendered' => '<p>'.$post_excerpt.'</p>')['rendered'],
            'longitude' => $longitude ? $longitude : null,
            'latitude' => $latitude ? $latitude : null,
            'start_date' => $start_date ? $start_date : null,
            'end_date' => $end_date ? $end_date : null
        );
    }

    $response = new WP_REST_Response( $posts_data );

    $response->header( 'X-WP-Total', (int) $listing_query->found_posts );
    $response->header( 'X-WP-TotalPages', (int) $listing_query->max_num_pages );

    return $response;                   
}