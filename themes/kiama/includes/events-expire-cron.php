<?php

$delayed_expire = get_field('delayed_expire','options') ? get_field('delayed_expire','options') : 0;

if (get_field('expire_events','options')) {

	// expire offer posts on date field.
	if (!wp_next_scheduled('expire_events_posts')){
	  wp_schedule_event(time(), 'daily', 'expire_events_posts'); // this can be hourly, twicedaily, or daily
	}

	add_action('expire_events_posts', 'expire_posts_function');

	function expire_posts_function() {
		$today = date('Ymd');
		$args = array(
			'post_type' => array('event'), // post types you want to check
			'posts_per_page' => -1 
		);
		$posts = get_posts($args);
		foreach($posts as $p){
			$end_time_field = get_field('end_time', $p->ID, false, false);
			$reoccurring_event_field = get_field('reoccurring_event', $p->ID, false, false);
			$expiredate = date('Ymd',strtotime($end_time_field)); // get the raw date from the db

			if ($end_time_field && !$reoccurring_event_field) {
				if($expiredate + $delayed_expire < $today) {
					$postdata = array(
						'ID' => $p->ID,
						'post_status' => 'draft'
					);
					wp_update_post($postdata);
				}
			}
		}
	}
} else {
	wp_clear_scheduled_hook('expire_events_posts');
}