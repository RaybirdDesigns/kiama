<?php

/**
 * Filter permalinks, add bookeasy params.
 * 
 * Used in map-end-points.php & favourites-end-points.php
 *
 * @param string    permalink url.
 * @param int      post id (optional).
 */
function append_bookeasy_string($url, $post_id = false) {
	global $post;
	$post_id = $post_id ? $post_id : $post->ID;
	$post_type = get_post_type($post_id);
	$bookeasy_id = get_field('bookeasy_id', $post_id) ? get_field('bookeasy_id', $post_id) : null;
	if ($bookeasy_id && $post_type !== 'destinfo') {
		$post_type = get_post_type($post_id);

		switch($post_type) {
			case 'accomm':
		        $url_type = $url.'#/accom/'.$bookeasy_id;
		        break;
		    case 'tour':
		        $url_type = $url.'#/tours/'.$bookeasy_id;
		        break;
		    case 'event':
		        $url_type = $url.'#/events/'.$bookeasy_id;
		        break;
		    case 'hire':
		        $url_type = $url.'#/carhire/'.$bookeasy_id;
		        break;
		    case 'attraction':
		        $url_type = $url.'#/packages/'.$bookeasy_id;
		        break;
		    default:
		    	$url_type = $url;
		}

    	return $url_type;
	} else {
		return $url;
	}
}
add_filter('the_permalink', 'append_bookeasy_string');