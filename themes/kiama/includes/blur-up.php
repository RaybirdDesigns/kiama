<?php
/**
 * Render the markup required for a "blurred up" image
 *
 * fnc base_64_convert: As the name suggests
 * fnc blur_up_feature: use for post feature images
 * fnc blur_up_acf: use for adhoc acf images
 *
 * @param string $container_class is arbitrary class name for the main image container
 * @param string $img_field_name is the image ID of the advanced custom field
 */


// convert image to base64
function base_64_convert($path) {
   $type = pathinfo($path, PATHINFO_EXTENSION);
   $data = file_get_contents($path);
   return 'data:image/' . $type . ';base64,' . base64_encode($data);
}

// for post feature images
function blur_up_feature($container_class, $size = 'large') {
	global $post;
	if (!get_post_thumbnail_id($post->ID)) {
		return;
	} else {
		$image_id = get_post_thumbnail_id($post->ID) ? get_post_thumbnail_id($post->ID) : get_field('fallback_image', 'options');
		$image_alt = get_post_meta($image_id , '_wp_attachment_image_alt', true);
		$thumb_nail_url = wp_get_attachment_image_url($image_id, 'tiny' );
		echo '<div class="blur-up '.$container_class.'">';
		echo	'<img class="blur-up-full"'.responsive_image_return($image_id, $size, true).'>';
		echo 	'<img class="blur-up-thumb" src="'.base_64_convert($thumb_nail_url).'">';
		echo '</div>';
	}
}

// for ad hoc acf images
function blur_up_acf($container_class, $img_field_name, $size = 'large') {
	if (empty($img_field_name)) {
		return;
	} else {
		$image_id = get_field($img_field_name) ? get_field($img_field_name): (get_sub_field($img_field_name) ? get_sub_field($img_field_name) : get_field('fallback_image', 'options'));
		$image_alt = get_post_meta($image_id , '_wp_attachment_image_alt', true);
		$thumb_nail_url = wp_get_attachment_image_url($image_id, 'tiny' );
		echo '<div class="blur-up '.$container_class.'">';
		echo	'<img data-blur-lazy class="blur-up-full"'.responsive_image_return($image_id, $size, true).'alt="'.$image_alt.'">';
		echo 	'<img class="blur-up-thumb" src="'.base_64_convert($thumb_nail_url).'">';
		echo '</div>';
	}
}