<?php

// ===============================================================================================
// This is used for getting listings
// ===============================================================================================
// @example: /wp-json/custom/v1/listings?post_type=accomm&sub_category=cabcottage,resort
add_action( 'rest_api_init', 'custom_api_listings' );   

function custom_api_listings() {
    register_rest_route( 'custom/v1', '/listings', array(
        'methods' => 'GET',
        'callback' => 'custom_api_listings_callback'
    ));
}
// Used in the quick search.
function custom_api_listings_callback( $request ) {
    // Initialize the array that will receive the posts' data. 
    $posts_data = array();
    // Receive and set the page parameter from the $request for pagination purposes
    // Note: using "primary_category" for main category
    // Note: using "sub_category" for custom taxonomy
    // $paged = $request->get_param( 'paged' ) ? $request->get_param( 'paged' ) : null;
    $post_ids = $request->get_param( 'ids' ) ? $request->get_param( 'ids' ) : null;
    // $posts_per_page = $request->get_param( 'posts_per_page' ) ? $request->get_param( 'posts_per_page' ) : null;
    $primary_category = $request->get_param( 'primary_category' ) ? $request->get_param( 'primary_category' ) : null;
    $sub_category = $request->get_param( 'sub_category' ) ? $request->get_param( 'sub_category' ) : null;
    $post_type = $request->get_param( 'post_type' ) ? $request->get_param( 'post_type' ) : array('accomm','attraction','event','restaurant','genservice','hire','tour','transport','destinfo','info');
    // $paged = ( isset( $paged ) || ! ( empty( $paged ) ) ) ? $paged : -1;

    $listing_args = array(
    	'paged' => false,
        'category_name' => $primary_category,
        'post_type' => $post_type,
        'post_status' => 'publish',
        'post__in' => $post_ids ? explode(',',$post_ids) : null,
        'update_post_term_cache' => false,
        'posts_per_page' => -1,            
        'orderby' => 'date',
        'order'   => 'DESC',
    );

    if ($sub_category) {

	    $tax_args = array(
	        array (
	            'taxonomy' => $post_type.'_type',
	            'field' => 'slug',
	            'terms' => explode(',',$sub_category),
	        )
	    );

    	$listing_args['tax_query'] = $tax_args;
    }

    $listing_query = new WP_Query($listing_args);
    $global_fallback_image = get_field('fallback_image', 'options');
    $posts = $listing_query->posts;

    // Loop through the posts and push the desired data to the array we've initialized earlier in the form of an object
    foreach( $posts as $post ) {
        $id = $post->ID;
        $has_post_thumbnail = has_post_thumbnail( $id );
        $post_thumbnail = $has_post_thumbnail ?
        		wp_get_attachment_image_url(get_post_thumbnail_id( $id ), 'medium') :
        		(get_field('product_image_0', $id) ? get_field('product_image_0', $id) : get_field('product_image_1', $id));
        $featured_image = $has_post_thumbnail ? array('media_details' => array('sizes' => array('large' => array('source_url' => $post_thumbnail))))['media_details']['sizes']['large']['source_url'] : null;
        $post_link = append_bookeasy_string(get_permalink( $id, false ), $id);
        $post_excerpt = wp_trim_words($post->post_content, 32);
        $category_name = !is_wp_error(get_the_terms($id, $post->post_type.'_type')) ? get_the_terms($id, $post->post_type.'_type')[0]->name : '';
        $longitude = get_field('product_long', $id);
        $latitude = get_field('product_lat', $id);
        $bookeasyId = get_field('bookeasy_id', $id);

        $posts_data[] = (object) array(
            'id' => $id,
            'title' => $post->post_title,
            'sub_category' => $category_name,
            'category' => $post->post_type,
            'title' => array('rendered' => $post->post_title)['rendered'],
            'link' => $post_link,
            'image' => $featured_image ? $featured_image : ($post_thumbnail ? $post_thumbnail.'?w=480' : wp_get_attachment_image_url($global_fallback_image, 'large')),
            'excerpt' => array('rendered' => '<p>'.$post_excerpt.'</p>')['rendered'],
            'longitude' => $longitude,
            'latitude' => $latitude,
            'bookeasy' => $bookeasyId,
        );
    }

    $response = new WP_REST_Response( $posts_data );

    $response->header( 'X-WP-Total', (int) $listing_query->found_posts );
    $response->header( 'X-WP-TotalPages', (int) $listing_query->max_num_pages );

    return $response;                   
}