<?php

/*
	=======================================
	Standardise urls. Add http if needed.
	=======================================
*/
function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}

/*
	=======================================
	Remove heading tags from content
	=======================================
*/
function strip_header_tags( $content ) {
    // this is just an example, there is probably a better regex
    $regex = '#(<h([1-6])[^>]*>)\s?(.*)?\s?(<\/h\2>)#';
    return preg_replace(
        $regex,
        '',
        $content
    );
}