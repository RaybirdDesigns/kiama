<?php

function searchfilter($query) {
    if ($query->is_search && !is_admin() ) {

    	if (isset($_GET['pt'])) {
    		$params =  $_GET['pt'];
			$query->set('post_type',$params);
    	}

		$query->set('posts_per_page',8);
		$query->set('paged', ( get_query_var('paged') ) ? get_query_var('paged') : 1 );
    }

	return $query;
}
add_filter('pre_get_posts','searchfilter');