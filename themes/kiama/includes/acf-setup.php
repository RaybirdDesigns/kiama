<?php 

/*
	=======================================
	Options page
	=======================================
*/
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Global Content and Settings',
		'menu_title'	=> 'Global Content',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

/*
	=======================================
	Setup for ACF google maps
	=======================================
*/
function my_acf_init() {
	acf_update_setting('google_api_key', get_field('google_maps_api_key', 'options'));
}
add_action('acf/init', 'my_acf_init');

// function my_acf_google_map_api( $api ){
	
// 	$api['key'] = get_field('google_maps_api_key', 'options');
	
// 	return $api;
	
// }
// add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');


/*
	=======================================
	Crowdriff blog shortcode setup
	=======================================
*/
function get_crowdriff_snippet() {
	$acf_field = get_field('social_section_crowdriff');
	return '<div class="crowdriff-container">'.$acf_field.'</div>';
}
add_shortcode( 'crowdriff', 'get_crowdriff_snippet' );

/*
	=======================================
	Collapse flexible content fields by default
	=======================================
*/
function rdsn_acf_repeater_collapse() {
?>
<style id="acf-flexible-content-collapse">.acf-flexible-content .acf-fields {display:none;}</style>
<script type="text/javascript">
	jQuery(function($) {
		$('.acf-flexible-content .layout').addClass('-collapsed');
		$('#acf-flexible-content-collapse').detach();
	});
</script>
<?php
}
add_action('acf/input/admin_head', 'rdsn_acf_repeater_collapse');

/*
	=======================================
	Setup for ACF icons
	=======================================
*/
add_filter( 'acf_icon_path_suffix', 'acf_icon_path_suffix' );

function acf_icon_path_suffix( $path_suffix ) {
    return 'images/icons/';
}