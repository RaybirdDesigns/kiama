<?php


// flush_rewrite_rules();
function get_post_primary_category($post_id, $term='category', $return_all_categories=false){
    $return = array();

    if (class_exists('WPSEO_Primary_Term')){
        // Show Primary category by Yoast if it is enabled & set
        $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
        $primary_term = get_term($wpseo_primary_term->get_primary_term());

        if (!is_wp_error($primary_term)){
            $return['primary_category'] = $primary_term;
        }
    }

    if (empty($return['primary_category']) || $return_all_categories){
        $categories_list = get_the_terms($post_id, $term);

        if (empty($return['primary_category']) && !empty($categories_list) && !is_wp_error($categories_list)){
            $return['primary_category'] = $categories_list[0];  //get the first category
        }
        if ($return_all_categories){
            $return['all_categories'] = array();

            if (!empty($categories_list)){
                foreach($categories_list as &$category){
                    $return['all_categories'][] = $category->term_id;
                }
            }
        }
    }

    return $return;
}

/*
	=======================================
	Add Theme Support
	=======================================
*/
add_theme_support( 'title-tag' );

/*
	=======================================
	Insert Custom Login Logo
	=======================================
*/
function custom_login_logo()
{
	echo '
		<style>
			.login h1 a { background-image: url(' . get_template_directory_uri() . '/images/logos/logo-login.png) !important; background-size: 320px 240px; width:320px; height:240px; display:block; }
		</style>
	';
}
add_action( 'login_head', 'custom_login_logo' );


/*
	=======================================
	Set base url
	=======================================
	In development on local host the base url should be set
	to localhost:8080 so the webpack dev server can 
*/
function base_url()
{
	$url = $_SERVER['SERVER_NAME'];

	$base_url = strpos($url, 'local') ? 'http://localhost:8080' : get_template_directory_uri();

	return $base_url;
}

/*
	=======================================
	Add scripts and css
	=======================================
*/
function add_theme_scripts()
{
	$current_post_type = get_post_type();

	wp_enqueue_script( 'main', base_url() . get_hashed_asset('main.js'), false, 1.1, true);

	if (is_single() && $current_post_type !=='post') {
		wp_enqueue_script( 'listing', base_url() . get_hashed_asset('listing.js'), false, 1.1, true);
	}
	
	function map_script() {
		wp_enqueue_script( 'map', base_url() . get_hashed_asset('map.js'), false, 1.1, true);
	}

	function google_maps_api_script() {
		wp_enqueue_script( 'googlemapscript', 'https://maps.googleapis.com/maps/api/js?key='.get_field('google_maps_api_key', 'options'), false, 1.1, true);
	}

	function itinerary_script() {
		wp_enqueue_script( 'itinerary_script', base_url() . get_hashed_asset('itinerary.js'), array('googlemapscript'), 1.1, true);
	}

	function google_map_section_script() {
		wp_enqueue_script( 'googlemap', base_url() . get_hashed_asset('googlemap.js'), array('googlemapscript'), 1.1, true);
	}

	if (base_url() != 'http://localhost:8080') {
		wp_enqueue_style( 'main', get_template_directory_uri() . get_hashed_asset('main.css') );
	}
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );



/*
	=======================================
	Limit excerpt length
	=======================================
*/
add_filter('excerpt_length', function($length) {
	    return 14;
});

function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

function custom_trim_excerpt($text)
{ // Fakes an excerpt if needed
	global $post;
	if ( '' == $text ) {
		$text = get_the_content('');
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]>', $text);
		$text = strip_tags($text);
		$excerpt_length = 1;
		$words = explode(' ', $text, $excerpt_length + 1);
		if (count($words) > $excerpt_length) {
			array_pop($words);
			array_push($words, '...');
			$text = implode(' ', $words);
		}
	}
	return $text;
}
add_filter('get_the_excerpt', 'custom_trim_excerpt');

/*
	=======================================
	Ensure all tags are included in queries
	=======================================
*/
function tags_support_query($wp_query) {
	if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
}

/*
	=======================================
	Add categories to pages
	=======================================
*/
function categories_for_pages(){
    register_taxonomy_for_object_type( 'category', 'page' );
}
add_action( 'init', 'categories_for_pages' );


/*
	=======================================
	Assign CPT template to custom post types
	=======================================
*/
add_filter( 'single_template', function( $template ) {

    $cpt_types = array( 'accomm', 'attraction', 'event', 'restaurant', 'genservice', 'hire', 'tour', 'transport', 'info', 'destinfo' );
    $post_type = get_post_type();

    if ( ! in_array( $post_type, $cpt_types ) )
        return $template;

    return get_stylesheet_directory() . '/single-listing.php'; 
});

/*
	=======================================
	Includes
	=======================================
*/
require get_template_directory() . '/includes/menus.php';
require get_template_directory() . '/includes/image-helpers.php';
require get_template_directory() . '/includes/helpers.php';
require get_template_directory() . '/includes/acf-setup.php';
require get_template_directory() . '/includes/remove-stuff.php';
require get_template_directory() . '/includes/asset-management.php';
require get_template_directory() . '/includes/bookeasy.php';
require get_template_directory() . '/includes/map-end-points.php';
require get_template_directory() . '/includes/favourites-end-points.php';
require get_template_directory() . '/includes/search.php';
require get_template_directory() . '/includes/events-expire-cron.php';

