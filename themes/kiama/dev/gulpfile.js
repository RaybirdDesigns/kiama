const gulp = require('gulp');
const svgstore = require('gulp-svgstore');
const svgmin = require('gulp-svgmin');
const path = require('path');

/* ---------------------------------------------------------------
    SVG spriting
------------------------------------------------------------------*/

gulp.task('svg-global', () => {
    return gulp
        .src('./img/svg-icons/global-sprite/*.svg')
        .pipe(
            svgmin(file => {
                var prefix = path.basename(file.relative, path.extname(file.relative));
                console.log(prefix);
                return {
                    plugins: [
                        {
                            cleanupIDs: {
                                prefix: prefix + '-',
                                minify: true,
                            },
                        },
                    ],
                };
            }),
        )
        .pipe(svgstore({ inlineSvg: true }))
        .pipe(gulp.dest('../images/sprites'));
});

gulp.task('svg-icons', () => {
    return gulp
        .src('./img/svg-icons/icons/*.svg')
        .pipe(
            svgmin(file => {
                var prefix = path.basename(file.relative, path.extname(file.relative));
                return {
                    plugins: [
                        {
                            cleanupIDs: {
                                prefix: prefix + '-',
                                minify: true,
                            },
                        },
                    ],
                };
            }),
        )
        .pipe(gulp.dest('../images/icons'))
        .pipe(svgstore({ inlineSvg: true }))
        .pipe(gulp.dest('../images/sprites'));
});

gulp.task('svg', () => {
    gulp.start('svg-global');
    gulp.start('svg-icons');
});
