import 'slick-carousel/slick/slick.scss';
import 'babel-polyfill';
import 'nodelist-foreach-polyfill';
import 'object-fit-polyfill';
// import svg4everybody from 'svg4everybody';
import '../scss/main.scss';

import './global/scroll-to';
import './global/lazysizes';
import './global/parallax';
import './global/bigpicture';
import './global/accessibility';
import './global/helpers';

import '../components/footer/footer';
import '../components/header/header';
import '../components/search-dialog/search-dialog';
import '../components/navigation/navigation';
import '../components/navigation/navigation-accessibility';
import '../components/tile-links-section/tile-links-section';
import '../components/section-accomm/section-accomm';
import '../components/tile-link/tile-link';
import '../components/blog/blog-media';
import '../components/accordion-group/accordion-group';
// import '../components/blog/blog-social-share';
import '../components/forms/select';
import '../components/favourites/favourites';
import '../components/favourites/favourites-page';
import '../components/footer-carousel/footer-carousel';
import '../components/hero-banners/hero-experience';
import '../components/listing-grid/listing-grid';
import '../components/event-calendar/event-calendar';

// fix for svg sprites in ie11
// svg4everybody();
