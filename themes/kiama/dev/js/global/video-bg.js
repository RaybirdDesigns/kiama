/*global $ */
import enquire from 'enquire.js';

// Background video
function isIE() {
	const myNav = navigator.userAgent.toLowerCase();
	return myNav.indexOf('msie') != -1 ? parseInt(myNav.split('msie')[1]) : false;
}

window.isIEOld = isIE() && isIE() < 9;
window.isiPad = navigator.userAgent.match(/iPad/i);

let poster = $('.video-bg').data('poster');
let video = $('.video-bg').data('video');
let noVideo = $('.video-bg').data('novideo');
let el = '';

enquire
	.register('(max-width: 992px)', () => {
		if ($('.video-el').length) {
			$('.video-el').remove();
		}

		el = `<div class="video-fallback" style="background-image: url('${noVideo}')"></div>`;

		$('.video-bg').prepend(el);
	})
	.register('(min-width: 993px)', () => {
		if (!isIEOld && !isiPad && video !== '') {
			if ($('.video-fallback').length) {
				$('.video-fallback').remove();
			}

			el += `<video class="video-el" autoplay muted loop poster="${poster}">`;
			el += `<source src="${video}" type="video/mp4">`;
			el += '</video>';

			$('.video-bg').prepend(el);
		}
	});
