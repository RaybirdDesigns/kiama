/*global $ */
// import 'gsap/ScrollToPlugin';

// Add data attribute with id to scroll to.
// example: data-scroll-to="event-detail"

// Scroll to element
const $scrollBtn = $('[data-scroll-to]');

$scrollBtn.on('click', function(e) {
    let $this = $(this);
    let sectionId = $('section')
        .first('section')
        .attr('id');

    $([document.documentElement, document.body]).animate(
        {
            scrollTop: $(`#${sectionId}`).offset().top,
        },
        500,
    );

    e.preventDefault();
});
