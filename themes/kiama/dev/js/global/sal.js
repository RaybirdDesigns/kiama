// Scroll animation library, written in vanilla JavaScript
// https://github.com/mciastek/sal
import 'intersection-observer';
import sal from 'sal.js';

sal();
