// https://github.com/henrygd/bigpicture
import BigPicture from 'bigpicture';

var youtubeLinks = document.querySelectorAll('[data-youtube]');
var vimeoLinks = document.querySelectorAll('[data-vimeo]');
var videoLinks = document.querySelectorAll('[data-videosrc]');

for (var i = 0; i < youtubeLinks.length; i++) {
    youtubeLinks[i].addEventListener('click', function(e) {
        e.preventDefault();
        BigPicture({
            el: this,
            ytSrc: this.dataset.youtube,
        });
    });
}

for (var i = 0; i < vimeoLinks.length; i++) {
    vimeoLinks[i].addEventListener('click', function(e) {
        e.preventDefault();
        BigPicture({
            el: this,
            vimeoSrc: this.dataset.vimeo,
        });
    });
}
for (var i = 0; i < videoLinks.length; i++) {
    videoLinks[i].addEventListener('click', function(e) {
        e.preventDefault();
        BigPicture({
            el: this,
            vidSrc: this.dataset.videosrc,
        });
    });
}
