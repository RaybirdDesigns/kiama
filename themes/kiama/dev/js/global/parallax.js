import enquire from 'enquire.js';
import throttle from 'lodash.throttle';

enquire.register('(min-width: 769px)', () => {
    const $parallax = $('[data-parallax]');
    const $img = $parallax.find('img');
    const screenHeight = $(window).height();

    function updatePosition() {
        let scrollTop = $(window).scrollTop();
        let imgPos = scrollTop / 2 + 'px';
        if (scrollTop < screenHeight) {
            $img.css('transform', 'translate3d(0,' + imgPos + ',0)');
        }
    }

    window.requestAnimationFrame(function() {
        window.addEventListener('scroll', throttle(updatePosition, 5));
    });
});
