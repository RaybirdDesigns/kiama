const path = require('path');
const webpack = require('webpack');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const PrettierPlugin = require("prettier-webpack-plugin");

module.exports = {
    entry: {
        main: './js/index.js',
        map: './js/map.js',
        googlemap: './js/googlemap.js',
        listing: './js/listing.js',
        itinerary: './js/itinerary.js',
    },
    output: {
        publicPath: 'http://localhost:8080/',
        filename: 'build/[name].js',
    },
    devtool: 'eval-cheap-module-source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            },
            {
                test: /\.(scss|css)$/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                ],
            },
            {
                test: /\.(png|jpeg|jpg|woff|woff2|svg)$/,
                loader: 'url-loader',
                exclude: /node_modules/,
            },
        ],
    },
    devServer: {
        hot: true,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
        },
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new PrettierPlugin({
			printWidth: 120,
			tabWidth: 4,
			singleQuote: true,
			trailingComma: "all",
			bracketSpacing: true,
			jsxBracketSameLine: true,
			semi: true,
			tabs: false,
		}),
        // new BundleAnalyzerPlugin(),
    ],
};
