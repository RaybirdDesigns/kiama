const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackAssetsManifest = require('webpack-assets-manifest');
const parentFolder = require('parent-folder');

const wpTemplateName = parentFolder(path.resolve(__dirname, '../index.php'), true);

module.exports = {
    entry: {
        main: './js/index.js',
        map: './js/map.js',
        googlemap: './js/googlemap.js',
        listing: './js/listing.js',
        itinerary: './js/itinerary.js',
    },
    output: {
        path: path.resolve(__dirname, '../build'),
        filename: '[name].[hash].js',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                // exclude: /node_modules/,
                exclude: /node_modules\/(?!(auto-bind)\/).*/,
                use: {
                    loader: 'babel-loader',
                },
            },
            {
                test: /\.(scss|css)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            minimize: {
                                safe: true,
                            },
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            outputStyle: 'compressed',
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            autoprefixer: {
                                browsers: ['last 2 versions'],
                            },
                        },
                    },
                ],
            },
            {
                test: /\.(png|jpeg|jpg|gif|svg)$/,
                loader: 'url-loader',
                exclude: /node_modules/,
                options: {
                    limit: 1,
                    name: 'images/[name].[ext]',
                    publicPath: `/wp-content/themes/${wpTemplateName}`,
                },
            },
            {
                test: /\.(woff|woff2|eot|ttf)$/,
                loader: 'url-loader',
                exclude: /node_modules/,
                options: {
                    limit: 1,
                    name: 'fonts/[name].[ext]',
                    publicPath: `/wp-content/themes/${wpTemplateName}`,
                },
            },
        ],
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
        }),
        new MiniCssExtractPlugin({
            path: path.resolve(__dirname, '../build'),
            filename: 'main.[hash].css',
        }),
        new WebpackAssetsManifest({
            path: path.resolve(__dirname, '../build'),
            output: 'asset-manifest.json',
        }),
    ],
};
