const $ImageContainers = document.querySelectorAll('.blog__image, .wp-caption');

if ($ImageContainers) {
    $ImageContainers.forEach((item, i) => {
        if (isEven(i + 1)) {
            item.classList.add('even');
        } else {
            item.classList.add('odd');
        }
    });
}

function isEven(n) {
    return n % 2 == 0;
}
