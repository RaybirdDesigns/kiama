/*global $ */
const $scrollBtn = $('[data-book-now]');

$scrollBtn.on('click', function(e) {
    e.preventDefault();
    const $this = $(this);
    const id = $this[0].hash;

    $([document.documentElement, document.body]).animate(
        {
            scrollTop: $(id).offset().top,
        },
        1000,
    );
});
