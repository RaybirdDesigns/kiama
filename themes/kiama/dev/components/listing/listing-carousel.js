import slick from 'slick-carousel';
    $('[data-listing-carousel][data-arrows="true"]').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        prevArrow: '<button type="button" class="slick-prev">&lt;</button>',
        nextArrow: '<button type="button" class="slick-next">&gt;</button>',
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    arrows: false
                }
            }
        ]
    });

$('[data-listing-thumbs]').slick({
    infinite: false,
    slidesToShow: 10,
    slidesToScroll: 1,
    asNavFor: '[data-listing-carousel]',
    dots: false,
    arrows: false,
    focusOnSelect: true,
});

$('[data-listing-carousel]').on('beforeChange', function(event, slick, slide, nextSlide) {
    $('[data-listing-thumbs]')
        .find('.slick-slide')
        .removeClass('slick-current')
        .eq(nextSlide)
        .addClass('slick-current');
    $('[data-listing-thumbs]').slick('slickGoTo', nextSlide);
});