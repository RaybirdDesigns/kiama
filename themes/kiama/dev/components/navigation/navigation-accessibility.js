import isIe from '../../js/global/helpers';

// Focus styles for menus when using keyboard navigation
// Properly update the ARIA states on focus (keyboard) and mouse over events
$('nav > ul').on('focus.wparia  mouseenter.wparia', '[aria-haspopup="true"]', function(e) {
    $(e.currentTarget).attr('aria-expanded', true);
});

// Properly update the ARIA states on blur (keyboard) and mouse out events
$('nav > ul').on('blur.wparia  mouseleave.wparia', '[aria-haspopup="true"]', function(e) {
    $(e.currentTarget).attr('aria-expanded', false);
});

// If not ie add the focus within to main nav
if (!isIe) {
    $('head').append(`
		<style>
            nav.main-menu ul.main-navigation > li:focus-within .sub-menu {
                opacity: 1;
			    visibility: visible;
			    transition: visibility 0s 0.16s, all 0.2s 0.16s ease-out;
            }
        </style>
	`);
}
