// Initialize collapsible (uncomment the lines below if you use the dropdown variation)
// var collapsibleElem = document.querySelector('.collapsible');
// var collapsibleInstance = M.Collapsible.init(collapsibleElem, options);

/*global $ */
/*global $ */
import enquire from 'enquire.js';
import Hammer from 'hammerjs';

/* +++++++++++++++++++++++++++++++++++++++++
	Start mobile nav functionality
++++++++++++++++++++++++++++++++++++++++++++ */
const $body = $('body');

// Mobile nav vars
const $headerWrapper = $('.header');
const $menu = $headerWrapper.find('.main-menu');
const $mainMenuOpenBtn = $headerWrapper.find('[data-mobile-nav-btn]');
const $mainMenuCloseBtn = $headerWrapper.find('[data-mobile-nav-btn-close]');

$mainMenuOpenBtn.on('click', function() {
    $body.addClass('nav-active');
    $menu.attr('aria-hidden', 'false');
});

$mainMenuCloseBtn.on('click', function() {
    $body.removeClass('nav-active');
    $menu.attr('aria-hidden', 'true');
});

enquire.register('(max-width: 769px)', () => {
    const nav = document.querySelector('.main-menu');

    const mc = new Hammer(nav);
    mc.on('swipeleft', function(ev) {
        $body.removeClass('nav-active');
        $menu.attr('aria-hidden', 'true');
    });

    $menu.attr('aria-hidden', 'true');
});

enquire.register('(min-width: 769px)', () => {
    $('.main-nav--mobile').removeAttr('style');
    $menu.attr('aria-hidden', 'true');
});
