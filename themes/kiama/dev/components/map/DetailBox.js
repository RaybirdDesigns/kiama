import React, { Component } from 'react';
import autoBind from 'auto-bind';
import includes from 'lodash.includes';

class DeatilaBox extends Component {
    constructor() {
        super();
        autoBind(this);
    }

    handleClose() {
        this.props.hideDetailMobile();
    }

    handleAddFavourite(id) {
        this.props.setFavourites(id);
    }

    render() {
        const props = this.props;
        let { category, excerpt, id, image, link, title, bookeasy } = props.listing;
        const ctaText =
            bookeasy && category !== 'destinfo' ? 'Book Now' : category === 'accomm' ? 'Read More' : 'Explore';

        return (
            <div className="map-detail">
                <button onClick={() => this.handleClose()} className="map-detail__close-btn">
                    <svg className="close-ico" role="presentation">
                        <use xmlnsXlink="http://www.w3.org/1999/xlink" xlinkHref={window.kiamaSettings['template_directory_uri'] + "/images/sprites/global-sprite.svg#cross-ico"} />
                    </svg>
                </button>
                <div className="map-detail__image" style={{ backgroundImage: `url(${image})` }} />
                <div className="map-detail__body">
                    <h2 dangerouslySetInnerHTML={{ __html: title }} />
                    <div dangerouslySetInnerHTML={{ __html: excerpt }} />
                    <a target="_blank" className={`btn btn-medium btn-primary ${category}`} href={link}>
                        {ctaText}
                    </a>
                    <button
                        aria-label={`Click to favourite ${title}`}
                        className={`fav-btn ${includes(props.favourites, id) ? 'active' : ''}`}
                        disabled={`${includes(props.favourites, id) ? 'true' : ''}`}
                        onClick={() => this.handleAddFavourite(id)}>
                        <svg className="fav-ico" role="presentation">
                            <use xmlnsXlink="http://www.w3.org/1999/xlink" xlinkHref={window.kiamaSettings['template_directory_uri'] + "/images/sprites/global-sprite.svg#fav-ico"} />
                        </svg>
                    </button>
                </div>
            </div>
        );
    }
}

export default DeatilaBox;
