import React, { Component } from 'react';
import autoBind from 'auto-bind';
import isEmpty from 'lodash/isempty';
import includes from 'lodash.includes';
import $ from 'jquery';
import GoogleMapReact from 'google-map-react';
import Slider from 'react-slick';
import enquire from 'enquire.js';
import store from 'store';
import observePlugin from 'store/plugins/observe';

store.addPlugin(observePlugin);

import LocationPin from './LocationPin';
import Navigation from './Navigation';
import DetailBox from './DetailBox';
import spinner from './spinner.svg';
import { mapStylesGrey as mapStyles } from './Style';
import { kiamaCoords } from './Regions';

function createMapOptions(maps) {
    return {
        draggable: true,
        scrollwheel: false,
        mapTypeControl: false,
        fullscreenControl: false,
        mapTypeControl: false,
        streetViewControl: false,
        styles: mapStyles,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER,
        },
    };
}

class MapApp extends Component {
    constructor(props) {
        super(props);
        autoBind(this);
        this.root = $('#map-root');
        this.baseUrl = this.root.data('base');
        this.googleApiKey = this.root.data('key');
        this.initialBounds = this.root.data('zoom');
        this.listingsEndPoint = this.baseUrl + '/wp-json/custom/v1/listings';
        this.header = document.querySelector('header');
        this.favBtn = this.header.querySelector('.header__fav-btn');

        store.observe('favourites', () => {
            // console.log('hit react');
            this.favouritesStorage = store.get('favourites') ? JSON.parse(store.get('favourites')) : [];
            // console.log(this.favouritesStorage);
        });

        this.state = {
            loading: true,
            listings: [],
            listingIndex: 0,
            favourites: this.favouritesStorage,
            detailVisibile: window.innerWidth > 768 ? true : false,
            nav: window.mapSettings,
            isDesktop: true,
            center: {
                lat: -34.6711136,
                lng: 150.8791132,
            },
            zoom: 12,
        };
    }

    getListings(postType, primaryCategory, subCategory, ids) {
        const postTypeQuery = postType ? `?post_type=${postType}` : '';
        const primaryCategoryQuery = primaryCategory ? `&primary_category=${primaryCategory}` : '';
        const subCategoryQuery = subCategory ? `&sub_category=${subCategory}` : '';
        const idsQuery = ids ? `?ids=${ids}` : '';
        // console.log(this.listingsEndPoint + idsQuery + postTypeQuery + primaryCategoryQuery + subCategoryQuery);
        this.setState(
            {
                loading: true,
            },
            () => {
                $.ajax({
                    url: this.listingsEndPoint + idsQuery + postTypeQuery + primaryCategoryQuery + subCategoryQuery,
                    dataType: 'json',
                    success: rawData => {
                        const resultCount = rawData.length;
                        this.setState(
                            {
                                listings: rawData,
                                resultCount,
                                loading: false,
                                noResults: resultCount === 0 ? true : false,
                            },
                            () => {
                                this.setState({
                                    activeListing: this.state.listings[0],
                                    listingIndex: 0,
                                });
                            },
                        );
                    },
                    error: error => {
                        alert('error:', error);
                    },
                });
            },
        );
    }

    componentDidMount() {
        const state = this.state;
        const listingIds = state.nav.bespoke.listingIds;

        if (window.innerWidth <= 768) {
            this.setState({
                isDesktop: false,
            });
        }

        if (!isEmpty(state.nav.bespoke)) {
            this.getListings(null, null, null, listingIds);
        } else if (isEmpty(state.nav.bespoke)) {
            const cat = Object.keys(state.nav.listings)[0];
            this.getListings(cat, state.nav.region, null, null);
        } else {
            this.getListings(null, state.nav.region, null, null);
        }
    }

    setFavourites(id) {
        window.addFavourite(id);
        const $header = document.querySelector('header');
        const $favBtn = $header.querySelector('.header__fav-btn');

        this.setState(
            {
                favourites: store.get('favourites') ? JSON.parse(store.get('favourites')) : [],
            },
            () => {
                this.favBtn.classList.add('active');
                this.favBtn.innerHTML =
                    this.favBtn.innerHTML + `<div class="fav-count"><span>${this.state.favourites.length}</span></div>`;
            },
        );
    }

    getFavourites() {
        const favourites = this.state.favourites;
        this.getListings(null, null, null, favourites);
    }

    getBespokeListings() {
        const listingIds = this.state.nav.bespoke.listingIds;
        this.getListings(null, null, null, listingIds);
    }

    getCategoryListings(cat) {
        this.getListings(cat, this.state.nav.region, null, null);
    }

    getSubCategoryListings(cat, subCat) {
        this.getListings(cat, this.state.nav.region, subCat, null);
    }

    setListingIndex(index) {
        this.slider.slickGoTo(index);
    }

    // Return map bounds based on list of places
    getMapBounds(map, maps, listings) {
        const bounds = new maps.LatLngBounds();
        listings.forEach(listing => {
            bounds.extend(new maps.LatLng(listing.latitude, listing.longitude));
        });
        return bounds;
    }

    // Re-center map when resizing the window
    bindResizeListener(map, maps, bounds) {
        maps.event.addDomListenerOnce(map, 'idle', () => {
            maps.event.addDomListener(window, 'resize', () => {
                enquire
                    .register('(max-width: 768px)', () => {
                        map.fitBounds(bounds, 100);
                        map.panBy(100, 30);
                    })
                    .register('(min-width: 769px)', () => {
                        map.fitBounds(bounds, 150);
                        map.panBy(200, -30);
                    });
            });
        });
    }

    // Re-center map when clicking nav the window
    bindClickListener(map, maps) {
        const categoryButton = document.querySelectorAll('.map-nav__cat li button');
        const setBounds = () => {
            const listings = this.state.listings;
            const bounds = this.getMapBounds(map, maps, listings);

            enquire
                .register('(max-width: 768px)', () => {
                    map.fitBounds(bounds);
                    const listener = google.maps.event.addListener(map, 'idle', function() {
                        if (map.getZoom() > 13) {
                            // console.log('single');
                            map.setZoom(13);
                        }
                        // map.fitBounds(bounds, 120);
                        // map.panBy(0, 0);
                        google.maps.event.removeListener(listener);
                    });
                })
                .register('(min-width: 769px)', () => {
                    if (listings.length === 1) {
                        map.fitBounds(bounds);
                        // map.setZoom(14);
                        const listener = google.maps.event.addListener(map, 'idle', function() {
                            if (map.getZoom() > 14) map.setZoom(14);
                            map.panBy(100, -30);
                            google.maps.event.removeListener(listener);
                        });
                    } else {
                        map.fitBounds(bounds, 180);
                        map.panBy(150, -50);
                    }
                });
        };

        categoryButton.forEach(button => {
            button.addEventListener('click', () => {
                this.setState({
                    loading: true,
                });

                const pollForListingsloaded = () => {
                    // Not ideal :(
                    if (!this.state.loading) {
                        setBounds();
                        return;
                    }
                    setTimeout(pollForListingsloaded, 500);
                };
                pollForListingsloaded();
            });
        });
    }

    bindZoomListener(kiamaPoly, map, maps) {
        let zoomLevel;
        maps.event.addListener(map, 'zoom_changed', () => {
            if (map.getZoom() < 14) {
                kiamaPoly.setMap(map);
            } else {
                kiamaPoly.setMap(null);
            }
        });
    }

    apiIsLoaded(map, maps, listings) {
        // Get bounds by our places
        const bounds = this.getMapBounds(map, maps, listings);

        const kiamaPoly = new google.maps.Polygon({
            paths: kiamaCoords,
            strokeColor: '#fa3e3e',
            strokeOpacity: 0.5,
            strokeWeight: 1.5,
            fillColor: '#b5d799',
            fillOpacity: 0,
        });
        kiamaPoly.setMap(map);

        // Fit map to bounds and offset
        enquire
            .register('(max-width: 768px)', () => {
                map.fitBounds(bounds, 50);
                map.panBy(0, 0);
            })
            .register('(min-width: 769px)', () => {
                map.fitBounds(bounds, this.initialBounds);
                map.panBy(150, -50);
            });

        this.bindResizeListener(map, maps, bounds);
        this.bindClickListener(map, maps);
        this.bindZoomListener(kiamaPoly, map, maps);
    }

    hideDetailMobile() {
        this.setState({
            detailVisibile: false,
        });
    }

    showDetailMobile() {
        this.setState({
            detailVisibile: true,
        });
    }

    render() {
        const state = this.state;
        const listings = state.listings;
        const slickSettings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 0,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        dots: true,
                    },
                },
            ],
            beforeChange: (current, next) => this.setState({ listingIndex: next }),
        };

        return (
            <div id="map" className={`map__container ${state.detailVisibile ? '' : 'active'}`}>
                <Navigation
                    navData={this.state.nav}
                    getBespokeListings={this.getBespokeListings}
                    getCategoryListings={this.getCategoryListings}
                    getSubCategoryListings={this.getSubCategoryListings}
                    favourites={state.favourites}
                    getFavourites={this.getFavourites}
                />
                {!isEmpty(state.listings) && (
                    <div className={`map__detail-wrapper row ${state.detailVisibile ? '' : 'active'}`}>
                        <Slider {...slickSettings} ref={slider => (this.slider = slider)}>
                            {listings.map((listing, i) => {
                                return (
                                    <DetailBox
                                        key={listing.id}
                                        listingIndex={state.listingIndex}
                                        listing={listing}
                                        hideDetailMobile={this.hideDetailMobile}
                                        setFavourites={this.setFavourites}
                                        favourites={state.favourites}
                                    />
                                );
                            })}
                        </Slider>
                    </div>
                )}
                {!isEmpty(state.listings) && (
                    <GoogleMapReact
                        bootstrapURLKeys={{ key: this.googleApiKey }}
                        defaultCenter={this.state.center}
                        defaultZoom={this.state.zoom}
                        options={createMapOptions}
                        styles={mapStyles}
                        yesIWantToUseGoogleMapApiInternals
                        onGoogleApiLoaded={({ map, maps }) => this.apiIsLoaded(map, maps, listings)}>
                        {listings.map((listing, i) => {
                            return (
                                <LocationPin
                                    key={listing.id}
                                    lat={listing.latitude}
                                    lng={listing.longitude}
                                    listing={listing}
                                    index={i}
                                    listingIndex={state.listingIndex}
                                    setListingIndex={this.setListingIndex}
                                    showDetailMobile={this.showDetailMobile}
                                    isDesktop={this.state.isDesktop}
                                />
                            );
                        })}
                    </GoogleMapReact>
                )}
                {isEmpty(state.listings) && state.loading === false && (
                    <div className="no-listings">Sorry, we don't have any listings for that category yet...</div>
                )}
                {state.loading === true && (
                    <div className="loader">
                        <img src={spinner} />
                    </div>
                )}
            </div>
        );
    }
}

export default MapApp;
