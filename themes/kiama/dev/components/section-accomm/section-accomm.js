import slick from 'slick-carousel';

$('[data-accomm-carousel]').slick({
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    prevArrow: "<div class='slick-prev-arrow'><i class='fa fa-angle-left' aria-hidden='true'></i></div>",
    nextArrow: "<div class='slick-next-arrow'><i class='fa fa-angle-right' aria-hidden='true'></i></div>",
    responsive: [
        {
            breakpoint: 900,
            settings: {
                slidesToShow: 1.5,
                slidesToScroll: 1,
                dots: true,
                arrows: false,
            },
        },
        {
            breakpoint: 640,
            settings: {
                slidesToShow: 1.5,
                slidesToScroll: 1,
                arrows: false,
                dots: true,
                infinite: false,
            },
        },
    ],
});
