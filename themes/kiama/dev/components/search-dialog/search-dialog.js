import ally from 'ally.js';

const $body = document.querySelector('body');
const $header = document.querySelector('header');
const $searchBtn = $header.querySelector('.header__search-btn');
const $searchDialog = document.querySelector('.search-dialog');
const $searchDialogCloseBtn = $searchDialog.querySelector('.search-dialog__close');

// console.log($searchDialogCloseBtn);

let disabledHandle;
let tabHandle;
let keyHandle;
let focusedElementBeforeDialogOpened;

// setTimeout(() => {
//     $searchDialog.classList.remove('hide');
// }, 500);

function closeDialogByKey(event) {
    // delay closing so that we don't
    // immediately reopen the dialog
    setTimeout(closeSearch);
}

function openSearch() {
    // $searchDialog.classList.remove('hide');
    $body.classList.add('search-active');

    $searchDialog.hidden = false;

    // Remember the focused element before we opened the dialog
    // so we can return focus to it once we close the dialog.
    focusedElementBeforeDialogOpened = document.activeElement;

    $searchDialog.addEventListener('transitionend', () => {
        // let element = ally.query.tabbable({
        //     context: '.search-dialog__close',
        //     includeContext: true,
        //     strategy: 'quick',
        // });

        $searchDialogCloseBtn.focus();
    });

    keyHandle = ally.when.key({
        escape: closeDialogByKey,
    });

    disabledHandle = ally.maintain.disabled({
        filter: $searchDialog,
    });

    tabHandle = ally.maintain.tabFocus({
        context: $searchDialog,
    });
}

function closeSearch() {
    console.log('closed');
    $body.classList.remove('search-active');

    // undo listening to keyboard
    keyHandle.disengage();
    // undo trapping Tab key focus
    tabHandle.disengage();
    // undo disabling elements outside of the dialog
    disabledHandle.disengage();
    // return focus to where it was before we opened the dialog
    focusedElementBeforeDialogOpened.focus();
    // hide or remove the dialog
    $searchDialog.hidden = true;
}

$searchBtn.addEventListener('click', openSearch);
$searchDialogCloseBtn.addEventListener('click', closeSearch);
