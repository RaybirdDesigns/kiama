import enquire from 'enquire.js';
const $tileLink = $('.tile-link');
const $tileLinksSection = $('.tile-links-section');

enquire.register('(min-width: 769px)', () => {
    $tileLink.on('mouseenter', function() {
        const $this = $(this);
        const $section = $this.closest('section');
        const $tileContentDesc = $this.find('.read-more');

        if (!$section.hasClass('alternate-layout')) {
            $tileContentDesc.slideDown(320);
        }
    });

    $tileLink.on('mouseleave', function() {
        const $this = $(this);
        const $section = $this.closest('section');
        const $tileContentDesc = $this.find('.read-more');
        if (!$section.hasClass('alternate-layout')) {
            setTimeout(() => {
                $tileContentDesc.slideUp(320);
            }, 10);
        }
    });
});
