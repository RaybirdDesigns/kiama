import slick from 'slick-carousel';

$('[data-footer-carousel]').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 1500,
    fade: true,
    cssEase: 'ease',
    rows: 0,
});
