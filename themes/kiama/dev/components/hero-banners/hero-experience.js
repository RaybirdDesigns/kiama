/*global $ */
// import 'gsap/ScrollToPlugin';

// Add data attribute with id to scroll to.
// example: data-scroll-to="event-detail"

// Scroll to element
const $scrollBtn = $('[data-scroll-id]');

$scrollBtn.on('click', function(e) {
    let $this = $(this);
    let sectionId = $this.attr('href');

    $([document.documentElement, document.body]).animate(
        {
            scrollTop: $(`${sectionId}`).offset().top,
        },
        1000,
    );

    e.preventDefault();
});
