/*global $ */
const $scrollBtn = $('[data-scroll-top]');

$scrollBtn.on('click', function(e) {
    $([document.documentElement, document.body]).animate(
        {
            scrollTop: $('main').offset().top,
        },
        1400,
    );
});
