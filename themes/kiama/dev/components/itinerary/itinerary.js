import { mapStylesColour as mapStyles } from '../map/Style';

let pathStyles;

function initMap() {
    const $sections = document.querySelectorAll('.itinerary');

    $sections.forEach((section, index) => {
        const $mapContainer = section.querySelector('.waypoint__map');
        let waypointsArr = window[`waypoints${index}`];
        let modeOfTransport = $(section).data('mode');
        let waypointsData = $(section).data('waypoints');
        let showPath = $(section).data('path');

        if (showPath) {
            pathStyles = {
                strokeColor: '#f2eda0',
                strokeWeight: 3,
                strokeOpacity: 1,
            };
        } else {
            pathStyles = {
                strokeColor: 'transparent',
                strokeWeight: 0,
                strokeOpacity: 0,
            };
        }

        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer({
            polylineOptions: pathStyles,
        });
        var map = new google.maps.Map($mapContainer, {
            draggable: true,
            scrollwheel: false,
            mapTypeControl: false,
            streetViewControl: false,
            preserveViewport: true,
            styles: mapStyles,
        });
        directionsDisplay.setMap(map);

        calculateAndDisplayRoute(directionsService, directionsDisplay, waypointsArr, modeOfTransport);
    });
}

function calculateAndDisplayRoute(directionsService, directionsDisplay, waypointsArr, modeOfTransport) {
    const endIndex = waypointsArr.length - 1;

    const sansOrigin = [...waypointsArr];
    sansOrigin.shift();
    const sansDest = [...sansOrigin];
    sansDest.pop();
    const pointsInbetween = sansDest;

    directionsService.route(
        {
            origin: waypointsArr[0].location,
            destination: waypointsArr[endIndex].location,
            waypoints: pointsInbetween,
            optimizeWaypoints: true,
            travelMode: modeOfTransport.toUpperCase(),
        },
        function(response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        },
    );
}

initMap();
