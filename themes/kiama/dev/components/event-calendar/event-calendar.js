const $eventCalendar = $('.event-calendar');
const $filterForm = $('form.filter', $eventCalendar);
const $filterListItems = $('li', $filterForm);

// Submit filter form on checkbox change
$filterListItems.each(function(index, item) {
    const $item = $(item);
    const $input = $item.find('input');

    $input.on('change', function() {
        $filterForm.submit();
    });
});
