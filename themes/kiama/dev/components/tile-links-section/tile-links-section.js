import slick from 'slick-carousel';

const $tileLinkSections = $('.tile-links-section');

$tileLinkSections.each(function(index, section) {
    const $section = $(section);
    const tileShown = $section ? $section.data('shown') : 0;
    const $carousel = $section.find('[data-articles-carousel]');

    $carousel.slick({
        infinite: false,
        slidesToShow: Number(tileShown),
        slidesToScroll: 1,
        dots: false,
        rows: 0,
        prevArrow: "<div class='slick-prev-arrow'><i class='fa fa-angle-left' aria-hidden='true'></i></div>",
        nextArrow: "<div class='slick-next-arrow'><i class='fa fa-angle-right' aria-hidden='true'></i></div>",
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    dots: false,
                },
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1.5,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true,
                    infinite: false,
                },
            },
        ],
    });
});

// setTimeout(() => {

// }, 100);
