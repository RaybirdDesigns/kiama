// CUSTOM Styled Select
class StyledSelect {
    constructor(formName) {
        this.formName = formName;
        this.styleSelect();
    }

    styleSelect() {
        const $form = $(this.formName);

        const $wrapper = $form.find('.listselect-container, .listcountry-container');

        $wrapper.each(function(i, wrapper) {
            const $wrapper = $(wrapper);
            const $select = $wrapper.find('select');

            const defaultText = $wrapper.find('option:selected').text();

            $wrapper.find('.nf-field-element > select').before(`<span class="selection-text">${defaultText}</span>`);

            $select.on('change', () => {
                let text = $select.find('option:selected').text();
                let value = $select.find('option:selected').val();

                if (value !== '') {
                    $wrapper
                        .find('span.selection-text')
                        .addClass('active')
                        .text(text);
                } else {
                    $wrapper
                        .find('span.selection-text')
                        .removeClass('active')
                        .text('Select');
                }
            });
        });
    }
}

const pollForSelect = () => {
    if ($('.listselect-container').length > 0) {
        const contactForm = new StyledSelect('.nf-form-cont');

        return;
    }
    setTimeout(pollForSelect, 100);
};
pollForSelect();
