const $listingGrid = $('.listing-grid');
const $filterForm = $('form.filter', $listingGrid);
const $filterRegionList = $('.filter__region-list', $listingGrid);
const $filterRegionListItems = $('.filter__region-list--item', $filterRegionList);

// Submit filter form on checkbox change
$filterRegionListItems.each(function(index, item) {
    const $item = $(item);
    const $input = $item.find('input');

    $input.on('change', function() {
        $filterForm.submit();
    });
});
