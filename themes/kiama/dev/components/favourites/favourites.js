import store from 'store';
import observePlugin from 'store/plugins/observe';
import includes from 'lodash.includes';

store.addPlugin(observePlugin);

const $body = document.querySelector('body');
const postIds = document.querySelectorAll('[data-postid]');
let prevFavourites = store.get('favourites') ? JSON.parse(store.get('favourites')) : [];

const favouriteCount = store.get('favourites') ? JSON.parse(store.get('favourites')).length : null;
const $favouriteEls = document.querySelectorAll('[data-favourite]');
const $header = document.querySelector('header');
const $favBtn = $header.querySelector('.header__fav-btn');
const $favouritesPageList = $('#favoutites-list');

if (favouriteCount === null) {
    store.set('favourites', JSON.stringify([]));
}

store.observe('favourites', () => {
    const favStore = JSON.parse(store.get('favourites'));

    if (favStore.length === 0) {
        $('.fav-count').remove();
        $('.header__fav-btn').removeClass('active');
    }

    if (favStore.length > 0) {
        $favBtn.classList.add('active');
        // console.log('fav hit');

        $favBtn.innerHTML =
            $favBtn.innerHTML +
            `<div class="fav-count"><span>${JSON.parse(store.get('favourites')).length}</span></div>`;

        prevFavourites = JSON.parse(store.get('favourites'));

        postIds.forEach(el => {
            const id = el.dataset.postid;
            if (includes(prevFavourites, Number(id))) {
                el.classList.add('active');
            }
        });
    }
});

window.removeFavourite = function(id) {
    store.remove();

    const newFavourites = store.get('favourites') !== null ? JSON.parse(store.get('favourites')) : [];

    const updatedFavourites = newFavourites.filter(favourite => {
        if (favourite !== id) {
            return favourite;
        }
    });

    $(`[data-id="${id}"]`).fadeOut('600', function() {
        // console.log('hit');
        setTimeout(() => {
            $(this).remove();
            const favStore = JSON.parse(store.get('favourites'));
            if (favStore.length === 0 && $('.no-favourites').length <= 0) {
                $favouritesPageList.append(`
			<div class="no-favourites text-center">
				<p><strong>You haven't favourited anything yet.</strong></p>
				<p>To add to your favourites list, look for the heart icon on any article or listing and click it!</p>
				<p>Come back to here to see your favourite list.</p>
			</div>
    	`);
            }
        }, 200);
    });

    store.set('favourites', JSON.stringify(updatedFavourites));
};

// Add favourite to storage
window.addFavourite = function(id) {
    let newFavourites = [];
    if (includes(prevFavourites, id)) {
        console.log('already added');
    } else {
        newFavourites.push(id);
        $.ajax({
            url: location.origin + '/wp-json/custom/v1/listing_title?id=' + id,
            dataType: 'json',
            success: rawData => {
                $('body').append(
                    `<div class="fav-notification notification-${id}" style="opacity: 0;"><strong>${
                        rawData[0].title
                    }</strong> added to favourites<br/><a href="/favourites">View Favourites</a></div>`,
                );

                setTimeout(() => {
                    $(`.fav-notification.notification-${id}`).addClass('active');
                }, 40);

                setTimeout(() => {
                    $(`.fav-notification.notification-${id}`).removeClass('active');
                }, 4000);

                setTimeout(() => {
                    $(`.fav-notification.notification-${id}`).remove();
                }, 4500);
            },
        });
    }

    const newAndOldFavourites = [...prevFavourites, ...newFavourites];
    store.set('favourites', JSON.stringify(newAndOldFavourites));
};
