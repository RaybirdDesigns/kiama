import dateFormat from 'dateformat';
import store from 'store';

if ($('#favourites-page').length > 0) {
    const $favouritesList = $('#favoutites-list');
    const favouritesStore = JSON.parse(store.get('favourites'));
    const favourites = favouritesStore.join(',');
    $.ajax({
        url: location.origin + '/wp-json/custom/v1/favourites?ids=' + favourites,
        dataType: 'json',
        success: rawData => {
            console.log(rawData);
            if (rawData.length > 0) {
                rawData.forEach(data => {
                    const startDate = data.start_date !== null ? dateFormat(data.start_date, 'dd mmm yyyy') : '';
                    const endDate = data.end_date !== null ? `- ${dateFormat(data.end_date, 'dd mmm yyyy')}` : '';
                    const category = data.category ? `<span class="post-type">${data.category}</span>` : '';
                    const subCat = data.sub_category ? `<span class="sub-cat">${data.sub_category}</span>` : '';
                    let dates;

                    if (startDate && endDate && data.start_date !== data.end_date) {
                        dates = `<span class="dates">${startDate} ${endDate}</span>`;
                    } else if (startDate && endDate === '') {
                        dates = `<span class="dates">${startDate}</span>`;
                    } else if (data.start_date === data.end_date) {
                        dates = `<span class="dates">${startDate}</span>`;
                    } else {
                        dates = '';
                    }

                    $favouritesList.append(
                        `<li data-id="${data.id}" class="small-12 columns">
						    <article class="standard-list__listing">
						        <div class="standard-list__image">
						            <img src="${data.image}" alt=""/>  
						        </div>
						        <div class="standard-list__body">
						            ${dates}
						            <h4><a href="${data.link}">${data.title}</a></h4>
						            ${data.excerpt}
						            ${category}
						            ${subCat}
						        </div>
						    </article>
						    <button onclick="window.removeFavourite(${
                                data.id
                            })" class="btn btn-small btn-ghost favourite-page__remove"><span>&times;</span><span class="remove-text"> Remove</span></button>
						</li>`,
                    );
                });
            } else {
                $favouritesList.append(`
					<div class="no-favourites text-center">
						<p><strong>You haven't favourited anything yet.</strong></p>
						<p>To add to your favourites list, look for the heart icon on any article or listing and click it!</p>
						<p>Come back to here to see your favourite list.</p>
					</div>
            	`);
            }
        },
    });
}
