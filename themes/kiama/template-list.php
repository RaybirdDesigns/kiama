<?php
/**
 * Template Name: List
 */

$list_listing_category = get_field('list_listing_category');
$listing_category_post_type = $list_listing_category ? $list_listing_category['value'] : array('accomm','attraction','event','restaurant','genservice','hire','tour','transport','destinfo');

$list_accomm_sub_categories = get_field('list_accomm_sub_categories');
$list_attraction_sub_categories = get_field('list_attraction_sub_categories');
$list_event_sub_categories = get_field('list_event_sub_categories');
$list_restaurant_sub_categories = get_field('list_restaurant_sub_categories');
$list_genservice_sub_categories = get_field('list_genservice_sub_categories');
$list_hire_sub_categories = get_field('list_hire_sub_categories');
$list_tour_sub_categories = get_field('list_tour_sub_categories');
$list_info_sub_categories = get_field('list_info_sub_categories');

$list_articles_categories = get_field('list_articles_categories');

// Get category terms
$categories = array();
if (!empty($list_articles_categories)) {
    foreach($list_articles_categories as $category) {
        $categories[] = $category->slug;
    }
}

$all_sub_categories = array(
    $list_accomm_sub_categories,
    $list_attraction_sub_categories,
    $list_event_sub_categories,
    $list_restaurant_sub_categories,
    $list_genservice_sub_categories,
    $list_hire_sub_categories,
    $list_tour_sub_categories,
    $list_info_sub_categories,
);
$selected_sub_categiries = array_filter($all_sub_categories);

// Get all sub category terms
if (!empty($selected_sub_categiries)) {
    $selected_sub_category = array_values($selected_sub_categiries)[0];

    $sub_categories = array();
    foreach($selected_sub_category as $sub_category) {
        $sub_categories[] = $sub_category->slug;
    }
}

$fallback_image_id = get_field('fallback_image', 'options');

$post_types = get_post_types(array('public' => true, '_builtin' => false));
$post_type_obj = array();
foreach($post_types as $post_type) {
    // check for posts
    $posts = get_posts( array('post_type' => $post_type, 'posts_per_page' => -1) );
    if ($posts) {
        $post_type_obj[] = get_post_type_object($post_type);
    }
}

$bottom_padding = $listing_category_post_type !== 'post' ? 'collapse-bottom' : '';

?>

<?php get_header(); ?>

<?php get_template_part('components/hero-banners/global-hero-short'); ?>

<section class="standard-list__heading grey-bg collapse-top <?php echo $bottom_padding; ?>">
    <div class="row">
        <div class="small-12 columns breadcrumb--dark">
            <?php get_template_part('components/breadcrumb/breadcrumb'); ?>
        </div>
        <div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center underline">
            <h1><?php the_title(); ?></h1>
            <?php
            if (have_posts()) :
                while (have_posts()) : the_post();
                    the_content();
                endwhile;
            endif;
            ?>
        </div>
    </div>
    <?php if($listing_category_post_type !== 'post'): ?>
    <div class="archive__nav">
        <ul class="archive__nav--list">
            <?php foreach($post_type_obj as $post_type): ?>
                <?php $rewrite_slug = $post_type->rewrite['slug'];?>
                <?php if ($post_type->name !== 'banner_advert' && $post_type->name !== 'video'): ?>
                    <li class="archive__nav--list---item">
                        <a href="<?php echo site_url().'/'.$rewrite_slug ?>"><?php echo $post_type->label; ?></a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php endif; ?>
</section>

<?php
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$list_args = array(
    'posts_per_page'         => 8,
    'post_type'              => $listing_category_post_type,
    'category_name'          => !empty($categories) ? implode(',',$categories) : null,
    'post_status'            => 'publish',
    'update_post_term_cache' => false,
    'paged'                  => $paged,
);

if (!empty($sub_categories)) {
    $sub_category_args = array(
        array (
            'taxonomy' => $listing_category_post_type.'_type',
            'field' => 'slug',
            'terms' => $sub_categories,
        )
    );
    $list_args['tax_query'] = $sub_category_args;
}
$wp_query = new WP_Query($list_args);
?>

<section class="standard-list">
    <ul class="row list-reset">
    <?php while ( $wp_query->have_posts() ) :
        $wp_query->the_post(); ?>
        <?php include(locate_template( 'components/list-item/list-item.php')) ?>
    <?php endwhile; ?>
    </ul>
    <div class="row">
        <div class="small-12 columns">
            <?php include(locate_template( 'components/pagination/pagination.php')) ?>
        </div>
    </div>
</section>
<?php wp_reset_postdata(); ?>


<?php get_footer(); ?>