<?php
/**
 * Template Name: Checkout
 */

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/global-hero-short'); ?>

<script type="text/javascript">
	$w(function() {
		BE.gadget.book('#book-gadget',{
			vcID:129,
			itineraryCSS: '/itinerary-pdf.css',
			confirmationURL:'/confirmation',
			overlaySettings:     {
			    useBlockout:     true,
			    overlayColour:   '#000000',
			    overlayOpacity:  0.6,
			    innerBackground: '#FFF',
			    zIndexLowest:    1000000,
			    width:           '100%',
			    height:          '100%'
			},
		});
	});
</script>

<section class="collapse-top bg--white">
	<div class="row">
		<div class="small-12 columns breadcrumb--dark breadcrumb-row bm">
            <?php get_template_part('components/breadcrumb/breadcrumb'); ?>
        </div>
		<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center underline">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
	<?php if (get_post()->post_content !== '') : ?>
	<div class="row bm">
		<div class="small-12 large-10 large-offset-1 columns">
			<?php
			while (have_posts()) : the_post();
				the_content();
			endwhile;
			?>
		</div>
	</div>
	<?php endif; ?>
	<div class="row expanded">
		<div id="book-gadget" class="small-12 columns"></div>
	</div>
</section>

<?php get_footer()?>