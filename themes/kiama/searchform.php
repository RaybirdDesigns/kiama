<?php

$post_types = get_post_types(array('public' => true, '_builtin' => false));

$post_type_obj = array();
foreach($post_types as $post_type) {
	$post_type_obj[] = get_post_type_object($post_type);
}

?>

<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<div class="search-dialog__input-wrapper">
		<div class="search-dialog__input">
			<label for="search" class="show-for-sr">Search for</label>
			<input type="search" class="search-field" id="search" placeholder="Search for anything..." value="" name="s" title="Search for:" />
		</div>
		<div class="search-dialog__filters">
			<div class="search-dialog__filters--item">
				<input type="checkbox" id="post" name="post_type" value="post">
				<label for="post">Articles</label>
			</div>
			<?php $index = 0; ?>
			<?php foreach($post_type_obj as $post_type): ?>
				<?php if ($post_type->name !== 'banner_advert' && $post_type->name !== 'video'): ?>
				<?php $index++; ?>
				<div class="search-dialog__filters--item">
					<input type="checkbox" id="<?php echo $post_type->name; ?>" name="pt[]" value="<?php echo $post_type->name; ?>">
					<label for="<?php echo $post_type->name; ?>"><?php echo $post_type->label; ?></label>
				</div>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>
		<button title="Search" class="search-dialog__btn"><span class="show-for-sr">Search Now</span>
			<svg class="icon" role="presentation">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/global-sprite.svg#search-ico"></use>
			</svg>
		</button>
	</div>
</form>

