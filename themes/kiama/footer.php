<?php

// Footer

?>
				<footer id="footer" class="footer">
					<div class="row">
						<div class="footer__top">
							<div class="footer__top--right">
								<ul class="social-list">
									<?php if( have_rows('platform_details','options') ): ?>
										    <li><h3>Connect</h3></li>
										<?php while ( have_rows('platform_details','options') ) : the_row(); ?>
										    <li>
										    	<a target="_blank" title="<?php the_sub_field('platform', 'options')['label']; ?>" href="<?php the_sub_field('platform_url', 'options'); ?>">
									    			<span class="show-for-sr"><?php the_sub_field('platform')['label']; ?></span>
											    	<svg class="icon" role="presentation">
											    		<use xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/global-sprite.svg#<?php echo get_sub_field('platform', 'options')['value']; ?>-ico"></use>
												    </svg>
												</a>
											</li>
										<?php endwhile; ?>
									<?php endif; ?>
								</ul>
							</div>
							<div class="footer__top--left">
								<button title="Scroll to top" data-scroll-top class="btn-scroll-top">
									<span class="show-for-sr">Scroll to first section</span>
									<svg class="icon" role="presentation">
									    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#down-ico"></use>
									</svg>
								</button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="footer__mid">
							<div class="sub-form">
								<h3>Subscribe</h3>
								<?php echo do_shortcode('[ninja_form id=3]'); ?>
                                
                                <?php
								$visitor_logo = get_field('visitor_information_centre_logo', 'options');
								$visitor_text = get_field('visitor_information_centre_logo_text', 'options');
								?>
								<?php if ($visitor_logo): ?>
								<div class="visitor_logo" style="display:none;">
									<img src="<?php echo $visitor_logo['url']; ?>" alt="<?php echo $visitor_logo['alt']; ?>">
									<span><?php echo $visitor_text; ?></span>
								</div>
								<?php endif; ?>
                                
							</div>
							<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns acknowledgement-statement">
							<?php the_field('acknowledgement_of_country', 'options'); ?>
						</div>
					</div>
					<div class="row expanded">		
						<div class="footer__bottom">
							<div class="footer__bottom--left">
								<div class="footer__bottom--container">
									<ul class="legal-links">
										<li>Copyright <?php echo date("Y"); ?> © Destination Kiama</li>
										<li><a href="/terms-conditions">Terms & Conditions</a></li>
										<li><a href="/privacy-policy">Privacy Policy</a></li>
									</ul>
								</div>
							</div>
							<div class="footer__bottom--right">
								<div class="footer__bottom--container">
									<?php
									$council_logo = get_field('council_logo', 'options');
									$council_url = get_field('council_url', 'options');
									$logo = get_field('logo', 'options');
									?>
									<?php if ($council_logo): ?>
										<a target="_blank" class="council-logo" href="<?php echo $council_url; ?>">
											<img src="<?php echo $council_logo['url']; ?>" alt="<?php echo $council_logo['alt']; ?>">
										</a>
									<?php endif; ?>
									<?php if ($logo): ?>
										<img class="kiama-logo" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</footer>
			</div>
		</main>
		<div tabindex="-1"
			 id="dialog-search"
			 role="dialog"
			 aria-labelledby="dialog-search"
			 hidden
			 class="search-dialog">
			 <button type="button" id="close-dialog" class="search-dialog__close">
			 	<span class="show-for-sr">Close search</span>
			 	<svg class="icon" role="presentation">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" href="<?php echo get_template_directory_uri(); ?>/images/sprites/global-sprite.svg#cross-ico"></use>
			 	</svg>
			 </button>
			<?php get_template_part( '/searchform' ); ?>
		</div>
		<?php wp_footer()?>

		<?php the_field('footer_scripts','option'); ?>
        <script>
		jQuery(document).ready(function() {
			jQuery('.kiama_visitor_info_logo').html('');
			jQuery('.visitor_logo').detach().appendTo('.kiama_visitor_info_logo');
			jQuery('.visitor_logo').show();


            window.kiamaSettings = window.kiamaSettings || {};
            window.kiamaSettings['template_directory_uri'] = '<?php echo get_template_directory_uri(); ?>';
		});
		</script>
		<style>
		.visitor_logo { margin-top: 2px; }
		.visitor_logo > img { width:32px; height:32px; border-radius:50%; vertical-align: initial; }
		.visitor_logo > span { color:#f5f5f5; font-weight:500; font-size:15px; display:inline-block; width:170px; padding-left:5px; }
		@media only screen and (max-width: 480px) {
			.visitor_logo > span { width: 125px; }
			.visitor_logo > img { vertical-align: top; margin-top: 10px; }
		}
		</style>
        
    </body>
</html>