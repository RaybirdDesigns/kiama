<?php get_header()?>

<?php get_template_part('components/hero-banners/global-hero-short'); ?>

<section>
	<div class="row">
		<div style="height:calc(100vh - 424px);" class="small-12 columns text-center">
			<h1>404</h1>
			<p>Sorry, that page doesn't exist.</p>
			<a href="/">Back to home page</a>
		</div>
	</div>
</section>

<?php get_footer()?>