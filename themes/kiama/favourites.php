<?php
/**
 * Template Name: Favourites
 */
?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/global-hero-short'); ?>

<?php if (have_posts()) : ?>
<section class="collapse-top">
	<div class="row">
		<div class="small-12 columns breadcrumb--dark">
            <?php get_template_part('components/breadcrumb/breadcrumb'); ?>
        </div>
		<div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center underline">
            <h1><?php the_title(); ?></h1>
            <?php
            if (have_posts()) :
                while (have_posts()) : the_post();
                    the_content();
                endwhile;
            endif;
            ?>
        </div>
	</div>
</section>
<?php endif; ?>

<section id="favourites-page" class="favourites-page">
	<div class="row">
		<div class="small-12 columns">
			<ul id="favoutites-list" class="favourites-page__list"></ul>
		</div>
	</div>
</section>

<?php get_footer()?>